# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 16:38:45 2017

@author: slewis
"""

import pandas as pd

import numpy as np

class Fleet_calcs():

    def __init__(self,CostModel):
        self.curr_equip = CostModel.curr_equip
        self.ma_curve = CostModel.input_data.ma_curve
        self.ma_curve_cols = self.ma_curve.columns.values.tolist()

    def ma_factor(self,cls,used_hrs):
        etype = cls.etype
        for coldata in self.ma_curve_cols:
            hrs_targ = float(coldata[1:])
            if used_hrs < hrs_targ:
               try:
                   self.ma_curve_data = self.ma_curve.loc[etype, coldata].max()
               except:
                   self.ma_curve_data = 1
               return self.ma_curve_data


    def fleetcalcs(self,fleet_data, period_name, fleet_min_nums, fleet_hrs, eqlife, eq_id,unit_req,act_tree,eq_count_based_unit,eq_count_hrs_units):
        self.fleet_data_run = fleet_data.drop('period_hours',axis = 1)
        self.enum_period_hrs = self.curr_equip.period_op_hours
        self.enum_period_hrs_raw = self.curr_equip.period_op_hours
        self.period_name = period_name
        self.act_tree = act_tree
        self.fleet_min_nums = fleet_min_nums
        self.fleet_hrs = fleet_hrs
        self.eqlife = eqlife  # this is the life used to add new fleet to opfleet
        self.eq_id = eq_id
        self.sched_periods = self.enum_period_hrs.shape[0]
        self.eq_count_based_unit = eq_count_based_unit
        self.eq_count_hrs_units = eq_count_hrs_units

        self.equip_op_period = pd.DataFrame(columns=['Eq_Id','period_name', 'sched_period', 'opnum'])
        self.equip_purchase_period = pd.DataFrame(columns=['Eq_Id','period_name', 'sched_period', 'purchase'])
        self.equip_capital_period = pd.DataFrame(columns=['Eq_Id', 'period_name', 'sched_period', 'capital'])
        self.equip_req_period = pd.DataFrame(columns=['Eq_Id','period_name', 'sched_period', 'opnum'])
        self.equip_slvg_period = pd.DataFrame(columns=['Eq_Id','period_name', 'sched_period', 'salvage'])

        if self.fleet_data_run.loc[0,'start'] == 1:
            equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': 1,
                      'period_name': period_name[1]}
            self.equip_purchase_period = self.equip_purchase_period.append([equip_data])

            equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost, 'sched_period': 1,
                      'period_name': period_name[1]}
            self.equip_capital_period = self.equip_capital_period.append([equip_data])



        for period_num in range(0,self.sched_periods):
            eq_purchase = 0
            period_sched = period_num + 1
            self.period_name = period_name[period_sched]
            equip_p_num = 0
            try:
                equip_p_num = int(equip_operating["equip_num"].max())
            except:
                pass

            try:
                eq_count_period = self.eq_count_based_unit[period_sched]
            except:
                eq_count_period = 0

            try:
                fleet_hrs_period_required = self.fleet_hrs[period_sched] #this is the number of hours required by that unit in that year
            except:
                fleet_hrs_period_required = 0

            rem_hrs = fleet_hrs_period_required

            mask = (self.fleet_data_run['active']) & (self.fleet_data_run['sched_period'] == period_num)#this populates with fleet from prior year and resets allocation
            equip_operating = self.fleet_data_run[mask].copy()
            equip_operating['sched_period'] = period_num + 1
            equip_operating['allocated_hrs'] = 0
            equip_operating['operator_cost_eb'] = 0
            equip_operating['operator_cost_hb'] = 0
            equip_operating['replaced'] = 0
            for index, rowdat in equip_operating.iterrows():
                equip_operating.loc[index,'p_hours'] = self.enum_period_hrs[period_sched]
                equip_operating.loc[index, 'p_hours_raw'] = self.enum_period_hrs[period_sched]

                try:
                    ma_factor = self.ma_factor(self.curr_equip, rowdat['used_hrs'])
                    equip_operating.loc[index, 'p_hours'] = self.enum_period_hrs[period_sched] * ma_factor

                except:
                    pass

            # TODO - carry raw period hours to check against so that the count fleet is ok.

            min_period_hrs = equip_operating['p_hours'].min()

            equip_operating['period_name'] = self.period_name
            self.enum_period_hrs[period_sched] = equip_operating["p_hours"].mean()
            self.enum_period_hrs_raw[period_sched] = equip_operating["p_hours_raw"].mean()

            try:
                eq_count_hrs_units_data = self.eq_count_hrs_units[period_sched]
            except:
                eq_count_hrs_units_data = 0

            try:
                eq_hr_based = (fleet_hrs_period_required - eq_count_hrs_units_data) / self.enum_period_hrs[period_sched]
                fract = eq_hr_based - int(eq_hr_based)
                # TODO make this an input
                if fract > 0.05:
                    min_required = np.ceil(eq_hr_based) + eq_count_period #self.enum_yr_hrs is the annual operating hours factored down for allocation
                else:
                    min_required = np.floor(eq_hr_based) + eq_count_period
            except:
                min_required = 0

            if min_required < eq_count_period: min_required = eq_count_period
            req_equip = {'Eq_Id': self.eq_id, 'opnum': min_required, 'sched_period':period_num + 1,'period_name':self.period_name}
            self.equip_req_period = self.equip_req_period.append([req_equip])

            if eq_count_period > 0:
                temp_data = equip_operating.head(int(eq_count_period)).copy()
                temp_data['p_hours'] = eq_count_hrs_units_data / eq_count_period
                temp_data.reset_index(inplace=True)
                equip_operating.reset_index(inplace=True)
                equip_operating = equip_operating.drop('index', 1)

                equip_operating['p_hours_temp'] = temp_data['p_hours']
                mask = equip_operating['p_hours_temp'] > 0
                equip_operating.loc[mask,'p_hours'] = equip_operating.loc[mask,'p_hours_temp']
                equip_operating = equip_operating.drop('p_hours_temp',1)

            maskrem = (equip_operating['rem_hrs'] < equip_operating['p_hours'])
            equip_operating.loc[maskrem,'p_hours'] = equip_operating.loc[maskrem,'rem_hrs'] #this simply reduces the annual hours to life
            equip_list_eol = equip_operating[maskrem].copy()
            equip_list_not_eol = equip_operating[~maskrem]
            equip_list_eol.sort_values(by='rem_hrs',inplace=True)
            equip_list_eol['enum_tmp'] = equip_list_eol['equip_num']

            try:
                fleet_eol_count = equip_list_eol.shape[0]
            except:
                fleet_eol_count = 0

            fleet_capacity_if_not_replaced = equip_operating['p_hours'].sum()

            replace_required = False

            if fleet_eol_count > 0:
                if fleet_capacity_if_not_replaced < fleet_hrs_period_required: replace_required = True

            try:
                min_enum = self.fleet_min_nums.iloc[0, period_num].item()
            except:
                min_enum = 0

            try:
                if min_enum < unit_req[period_sched]: min_enum = np.ceil(unit_req[period_sched])
            except:
                pass

            if min_required > 0:
                if min_enum < min_required: min_enum = np.ceil(min_required)
            else:
                min_enum = 0

            fleet_addition_required = False
            
            effective_fleet_hour_based = (equip_operating['p_hours'].sum() - eq_count_hrs_units_data)/self.enum_period_hrs[period_sched]

            # TODO make 1.05 an input
            effective_fleet = (eq_count_period + effective_fleet_hour_based) * 1.05#note make this an overfleet input variable
            if effective_fleet != effective_fleet: effective_fleet = 0
            fleet_capacity_if_replaced = (equip_operating.shape[0] - eq_count_period) * self.enum_period_hrs[period_sched] + eq_count_hrs_units_data
            if fleet_capacity_if_replaced != fleet_capacity_if_replaced: fleet_capacity_if_replaced = 0
            fleet_short = fleet_hrs_period_required - fleet_capacity_if_replaced

            if min_enum > 1:
                fleet_short_per_unit = fleet_short/(min_enum - 1)
            else:
                fleet_short_per_unit = 0

            fleet_short_ratio = fleet_short_per_unit / self.enum_period_hrs[period_sched]
            # TODO make this an input

            if (fleet_short_ratio < 0.05) and (fleet_short_ratio > 0): min_enum -= 1

            if ((effective_fleet > min_enum) & (fleet_capacity_if_not_replaced > fleet_hrs_period_required) & (replace_required == False)):
                replace_required = False
            else:
                if fleet_eol_count > 0:
                    replace_required = True
                else:
                    replace_required = False
                    if fleet_short_ratio >= 0.05: fleet_addition_required = True

            if ((fleet_capacity_if_replaced < fleet_hrs_period_required) & (fleet_short_ratio > 0.05)):
                replace_all_fleet = True
                fleet_addition_required = True
            else:
                replace_all_fleet = False

            total_fleet_count = equip_operating.shape[0]

            if ((replace_required) & (total_fleet_count == 1)):
                replace_all_fleet = True

            total_eol = equip_list_eol['p_hours'].sum()
            total_not_eol = equip_list_not_eol['p_hours'].sum()
            total_not_eol_count = equip_list_not_eol.shape[0]
            fleet_replace_count = 0
            total_fleet_capacity = total_eol + total_not_eol
            total_fleet_short = fleet_hrs_period_required - total_fleet_capacity
            if (min_enum - fleet_eol_count) != 0:
                total_fleet_short_per_unit = total_fleet_short / (min_enum - fleet_eol_count)
            else:
                total_fleet_short_per_unit = 0

            total_fleet_short_ratio = total_fleet_short_per_unit / self.enum_period_hrs[period_sched] * -1
            total_fleet_avg_period_hours = self.enum_period_hrs[period_sched]

            equip_list_eol.set_index(['equip_num'],inplace = True)

            if min_enum>0:
                equip_period_min_enum = fleet_hrs_period_required/min_enum
            else:
                equip_period_min_enum = 0

            if fleet_eol_count > 0:
                min_hrs_eol = equip_list_eol.iloc[0]['p_hours']
                if equip_period_min_enum < min_hrs_eol:
                    replace_required = False


            try:
                neweqnum = int(equip_operating["equip_num"].max()) + 1
            except:
                neweqnum = equip_p_num + 1


            if (replace_required) & (not replace_all_fleet): #this works out how many of the fleet require replacement if not all replaced and then replaces them
                while (total_fleet_capacity < fleet_hrs_period_required):
                    try:
                        fleet_pop_hours = equip_list_eol.iloc[fleet_replace_count]['p_hours']
                    except:
                        #print('fred')
                        break
                    total_eol -= fleet_pop_hours
                    total_not_eol += self.enum_period_hrs[period_sched]
                    total_not_eol_count += 1
                    total_fleet_capacity = total_eol + total_not_eol
                    total_fleet_avg_period_hours = (fleet_hrs_period_required - total_eol)/total_not_eol_count
                    fleet_replace_count += 1

                for replace_fleet in range(neweqnum, neweqnum + fleet_replace_count):
                    mask = equip_operating['active']
                    optemp = equip_operating[mask]
                    equip_oldest = optemp.nsmallest(1, 'p_hours')
                    oldest_eqnum = equip_oldest['equip_num'].min()
                    maskenum = equip_operating['equip_num'] == oldest_eqnum
                    equip_operating.loc[maskenum,'allocated_hrs'] = equip_operating.loc[maskenum,'p_hours'].item()
                    equip_operating.loc[maskenum,'finish'] = period_sched
                    equip_operating.loc[maskenum,'active'] = False
                    equip_operating.loc[maskenum,'replaced'] = 1
                    equip_data = {'Eq_Id': self.eq_id, 'salvage': 1, 'sched_period': period_sched,'period_name':self.period_name}
                    self.equip_slvg_period = self.equip_slvg_period.append([equip_data])
                    rem_hrs -= equip_operating.loc[maskenum,'p_hours'].item()
                    hours_short = total_fleet_avg_period_hours - equip_operating.loc[maskenum,'p_hours'].item()

                    equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': period_num + 1,'period_name':self.period_name}
                    self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                    eq_purchase = 1

                    equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost, 'sched_period': period_num + 1,
                                  'period_name': self.period_name}

                    self.equip_capital_period = self.equip_capital_period.append([equip_data])

                    equipop = {'Eq_Id': self.eq_id, 'equip_num': replace_fleet, 'rem_hrs': self.eqlife, 'used_hrs': 0,
                               'start': period_sched, 'finish': 9999, 'p_hours': self.enum_period_hrs[period_sched], 'active': True,
                               'allocated_hrs': hours_short, 'sched_period': period_sched,'period_name':self.period_name,'replaced':0}


                    equip_operating = equip_operating.append([equipop])
                    rem_hrs -= hours_short

                """this is the fleet that dies and is not replaced"""
                maskremhrs = (equip_operating['active']) & (equip_operating['rem_hrs'] < total_fleet_avg_period_hours)
                optemp = equip_operating[maskremhrs]
                fleet_to_eol = optemp.shape[0]

                while fleet_to_eol > 0:
                    try:
                        mask = (equip_operating['active']) & (equip_operating['replaced'] != 999)
                    except:
                        mask = equip_operating['active']
                    optemp = equip_operating[mask]
                    equip_oldest = optemp.nsmallest(1, 'p_hours')
                    oldest_eqnum = equip_oldest['equip_num'].min()
                    maskenum = equip_operating['equip_num'] == oldest_eqnum
                    if equip_operating.loc[maskenum,'p_hours'].item() > 100:
                        equip_operating.loc[maskenum,'allocated_hrs'] = equip_operating.loc[maskenum,'p_hours'].item() - 1
                        equip_operating.loc[maskenum, 'replaced'] = 999
                        rem_hrs -= equip_operating.loc[maskenum, 'p_hours'].item()
                        rem_hrs += 1
                    else:
                        equip_operating.loc[maskenum,'finish'] = period_sched
                        equip_operating.loc[maskenum,'active'] = False
                        equip_operating.loc[maskenum, 'allocated_hrs'] = equip_operating.loc[maskenum, 'p_hours'].item()
                        equip_operating.loc[maskenum, 'replaced'] = 0
                        equip_data = {'Eq_Id': self.eq_id, 'salvage': 1, 'sched_period': period_sched,'period_name':self.period_name}
                        self.equip_slvg_period = self.equip_slvg_period.append([equip_data])
                        rem_hrs -= equip_operating.loc[maskenum,'p_hours'].item()

                    maskremhrs = (equip_operating['active']) & (equip_operating['rem_hrs'] < total_fleet_avg_period_hours) & (equip_operating['replaced'] != 999)
                    optemp = equip_operating[maskremhrs]
                    try:
                        fleet_to_eol = optemp.shape[0]
                    except:
                        fleet_to_eol = 0

                mask_not_eol = ((equip_operating['active']) & (equip_operating['allocated_hrs'] == 0))
                fleet_not_eol_count = equip_operating[mask_not_eol].shape[0]
                if fleet_not_eol_count != 0:
                    equip_operating.loc[mask_not_eol,'allocated_hrs'] = rem_hrs / fleet_not_eol_count #this adds the remaining hours to the other fleet
                else:
                    equip_operating.loc[mask_not_eol, 'allocated_hrs'] = 0

                rem_hrs = 0

            if ((replace_required) & (replace_all_fleet)):  # this works out how many of the fleet require replacement if not all replaced and then replaces them
                try:
                    equip_list_eol.reset_index(inplace=True)
                except:
                    pass
                equip_operating.loc[maskrem, 'replaced'] = 1
                countme = 0
                for replace_fleet in range(neweqnum, neweqnum + fleet_eol_count):

                    repme_eqnum = equip_list_eol.loc[countme,'equip_num'].item()
                    maskenum = equip_operating['equip_num'] == repme_eqnum
                    equip_operating.loc[maskenum,'allocated_hrs'] = equip_operating.loc[maskenum,'p_hours'].item()
                    equip_operating.loc[maskenum,'finish'] = period_sched
                    equip_operating.loc[maskenum,'active'] = False
                    equip_data = {'Eq_Id': self.eq_id, 'salvage': 1, 'sched_period': period_sched,'period_name':self.period_name}
                    self.equip_slvg_period = self.equip_slvg_period.append([equip_data])
                    rem_hrs -= equip_operating.loc[maskenum,'p_hours'].item()
                    hours_short = fleet_hrs_period_required / min_enum - equip_operating.loc[maskenum,'allocated_hrs'].item()

                    equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': period_num + 1,'period_name':self.period_name}
                    self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                    eq_purchase = 1

                    equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost, 'sched_period': period_num + 1,
                                  'period_name': self.period_name}
                    self.equip_capital_period = self.equip_capital_period.append([equip_data])

                    equipop = {'Eq_Id': self.eq_id, 'equip_num': replace_fleet, 'rem_hrs': self.eqlife, 'used_hrs': 0,
                               'start': period_sched, 'finish': 9999, 'p_hours': self.enum_period_hrs[period_sched], 'active': True,
                               'allocated_hrs': hours_short, 'sched_period': period_sched,'period_name':self.period_name,'replaced':0}

                    equip_operating = equip_operating.append([equipop])
                    rem_hrs -= hours_short
                    countme += 1

                starteqnum = neweqnum + fleet_eol_count
                mask = equip_operating['active']
                add_fleet_num = int(min_enum) - equip_operating[mask].shape[0]
                for add_fleet in range(starteqnum, starteqnum + add_fleet_num):
                    hours_allocate = fleet_hrs_period_required / min_enum
                    equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': period_num + 1,'period_name':self.period_name}
                    self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                    eq_purchase = 1

                    equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost, 'sched_period': period_num + 1,
                                  'period_name': self.period_name}
                    self.equip_capital_period = self.equip_capital_period.append([equip_data])

                    equipop = {'Eq_Id': self.eq_id, 'equip_num': add_fleet, 'rem_hrs': self.eqlife, 'used_hrs': 0,
                               'start': period_sched, 'finish': 9999, 'p_hours': self.enum_period_hrs[period_sched], 'active': True,
                               'allocated_hrs': hours_allocate, 'sched_period': period_sched,'period_name':self.period_name,'replaced':0}

                    equip_operating = equip_operating.append([equipop])
                    rem_hrs -= hours_allocate

            #load up existing fleet

            if (not replace_required) & (fleet_addition_required):  # this works out how many of the fleet require replacement if not all replaced and then replaces them

                mask = (equip_operating['active']) & (equip_operating['allocated_hrs'] == 0)
                all_hrs = equip_operating.loc[mask,'p_hours'].sum()
                equip_operating.loc[mask,'allocated_hrs'] = equip_operating.loc[mask,'p_hours']
                rem_hrs -= all_hrs
                mask = equip_operating['active']
                add_fleet_num = int(min_enum) - equip_operating[mask].shape[0]
                hours_allocate = 0
                if add_fleet_num == 0: broken = 'why_broken'
                if add_fleet_num > 0: hours_allocate = rem_hrs /add_fleet_num
                for add_fleet in range(neweqnum, neweqnum + add_fleet_num):
                    equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': period_num + 1,'period_name':self.period_name}
                    self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                    eq_purchase = 1

                    equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost, 'sched_period': period_num + 1,
                                  'period_name': self.period_name}
                    self.equip_capital_period = self.equip_capital_period.append([equip_data])

                    equipop = {'Eq_Id': self.eq_id, 'equip_num': add_fleet, 'rem_hrs': self.eqlife, 'used_hrs': 0,
                               'start': period_sched, 'finish': 9999, 'p_hours': self.enum_period_hrs[period_sched], 'active': True,
                               'allocated_hrs': hours_allocate, 'sched_period': period_sched,'period_name':self.period_name,'replaced':0}

                    equip_operating = equip_operating.append([equipop])
                    rem_hrs -= hours_allocate

            maskophrs = equip_operating['active']
            try:
                op_num = equip_operating[maskophrs].shape[0]
            except:
                op_num = 0

            try:
                neweqnum = int(equip_operating["equip_num"].max()) + 1
            except:
                neweqnum = equip_p_num + 1

            if  eq_count_period > 0:
                if op_num < min_enum:  # this works out how many of the fleet require replacement if not all replaced and then replaces them
                    mask = equip_operating['active']
                    add_fleet_num = int(min_enum) - equip_operating[mask].shape[0]
                    hours_allocate = eq_count_hrs_units_data / eq_count_period
                    if hours_allocate * add_fleet_num > rem_hrs: hours_allocate = rem_hrs/add_fleet_num
                    for add_fleet in range(neweqnum, neweqnum + add_fleet_num):
                        #hours_allocate = eq_count_hrs_units_data / eq_count_period

                        equip_data = {'Eq_Id': self.eq_id, 'purchase': 1, 'sched_period': period_num + 1,'period_name':self.period_name}
                        self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                        eq_purchase = 1

                        equip_data = {'Eq_Id': self.eq_id, 'capital': self.curr_equip.capital_cost,
                                      'sched_period': period_num + 1,
                                      'period_name': self.period_name}
                        self.equip_capital_period = self.equip_capital_period.append([equip_data])

                        equipop = {'Eq_Id': self.eq_id, 'equip_num': add_fleet, 'rem_hrs': self.eqlife, 'used_hrs': 0,
                                   'start': period_sched, 'finish': 9999, 'p_hours': self.enum_period_hrs[period_sched], 'active': True,
                                   'allocated_hrs': hours_allocate, 'sched_period': period_sched,'period_name':self.period_name,'replaced':0}

                        equip_operating = equip_operating.append([equipop])
                        rem_hrs -= hours_allocate


            """get all fleet operating with no hours and give them the rest of the hours"""

            mask = equip_operating['active']
            op_fleet_num = equip_operating[mask].shape[0]
            equip_op_year_data = {'Eq_Id': self.eq_id, 'sched_period': period_sched,'opnum':op_fleet_num,'period_name':self.period_name}
            self.equip_op_period = self.equip_op_period.append([equip_op_year_data])

            maskophrs = ((equip_operating['active']) & (equip_operating['allocated_hrs'] == 0))
            try:
                rem_num = equip_operating[maskophrs].shape[0]
            except:
                rem_num = 0

            if rem_num >0:
                rem_avg = rem_hrs / rem_num
                # TODO this should check against raw p_hours not adjusted ma p hours
                #mask_active_eol = (equip_operating['active']) & (equip_operating['p_hours'] + 1 < self.enum_period_hrs_raw[period_sched]) & (equip_operating['allocated_hrs'] == 0)
                max_phours = equip_operating['p_hours'].max()
                max_phours_raw = equip_operating['p_hours_raw'].max()
                phours_check = max_phours
                if max_phours_raw > max_phours: phours_check = max_phours_raw
                mask_active_eol = (equip_operating['active']) & (
                            phours_check + 1 < min_period_hrs) & (
                                              equip_operating['allocated_hrs'] == 0)
                min_avail_hrs = equip_operating.loc[mask_active_eol,'p_hours'].min()
                while rem_avg > min_avail_hrs:
                    maskmin = (equip_operating['p_hours'] == min_avail_hrs)
                    hours_allocated = equip_operating[maskmin].shape[0] * min_avail_hrs
                    equip_operating.loc[maskmin,'allocated_hrs'] = equip_operating.loc[maskmin,'p_hours']
                    equip_operating.loc[maskmin,'active'] = False
                    rem_hrs -= hours_allocated
                    rem_num -= equip_operating[maskmin].shape[0]
                    if rem_num > 0:
                        rem_avg = rem_hrs / rem_num
                    else:
                        rem_avg = 0
                    mask_active_eol = (equip_operating['active']) & (equip_operating['p_hours'] < self.enum_period_hrs[period_sched]) & (equip_operating['allocated_hrs'] == 0)
                    min_avail_hrs = equip_operating.loc[mask_active_eol, 'p_hours'].min()
                    maskophrs = ((equip_operating['active']) & (equip_operating['allocated_hrs'] == 0))

                if rem_num > 0: equip_operating.loc[maskophrs,'allocated_hrs'] = rem_hrs / rem_num
            else:
                equip_operating.loc[maskophrs, 'allocated_hrs'] = 0

            equip_operating['rem_hrs'] -= equip_operating['allocated_hrs']

            masklife = equip_operating['rem_hrs'] <= 0
            equip_operating.loc[masklife, 'rem_hrs'] = 0
            equip_operating.loc[masklife, 'finish'] = period_num + 1
            equip_operating.loc[masklife, 'active'] = False
            masktemp = equip_operating['active'] == True
            active_eq_temp = equip_operating[masktemp].shape[0]
            max_eqnum = equip_operating['equip_num'].max()
            if active_eq_temp == 0:
                masktemp = equip_operating['equip_num'] == max_eqnum
                equip_operating.loc[masktemp,'active'] = True
                equip_operating.loc[masktemp,'rem_hrs'] = 1




            operator_cost_count_based = eq_count_period * self.enum_period_hrs[period_sched] * self.curr_equip.operator_cost
            hrs_non_count = equip_operating['allocated_hrs'].sum() - eq_count_hrs_units_data
            operator_cost_hour_based = hrs_non_count * self.curr_equip.operator_cost
            maskactive = equip_operating['active']
            active_eq = equip_operating[maskactive].shape[0]
            equip_operating.fillna(0, inplace=True)

            total_opcost_hb = operator_cost_hour_based + operator_cost_count_based
            mask_eol_not_replaced = (equip_operating['active'] == False) & (equip_operating['replaced'] == 0)
            equip_operating.loc[mask_eol_not_replaced,'operator_cost_hb'] = equip_operating.loc[mask_eol_not_replaced,'allocated_hrs'] * self.curr_equip.operator_cost
            equip_operating.loc[mask_eol_not_replaced, 'operator_cost_eb'] = equip_operating.loc[
                                                                                 mask_eol_not_replaced, 'allocated_hrs'] * self.curr_equip.operator_cost

            total_opcost_hb -= equip_operating.loc[mask_eol_not_replaced,'operator_cost_hb'].sum()

            if active_eq > 0:
                equip_operating.loc[maskactive,'operator_cost_hb'] = total_opcost_hb/active_eq
                operator_cost_active = active_eq * self.curr_equip.operator_cost * self.enum_period_hrs[period_sched]
                equip_operating.loc[maskactive,'operator_cost_eb'] = operator_cost_active / active_eq

            equip_operating['operator_number_eb'] = 0
            equip_operating['operator_number_hb'] = 0

            if self.curr_equip.operator_annual_cost > 0: equip_operating['operator_number_eb'] = equip_operating['operator_cost_eb'] / self.curr_equip.operator_annual_cost / self.curr_equip.period_factor[period_sched]
            if self.curr_equip.operator_annual_cost > 0: equip_operating['operator_number_hb'] = equip_operating['operator_cost_hb'] / self.curr_equip.operator_annual_cost / self.curr_equip.period_factor[period_sched]

            if self.curr_equip.cost_basis == 'Op_Units':
                equip_operating['operator_number'] = equip_operating['operator_number_eb']
                equip_operating['operator_cost'] = equip_operating['operator_cost_eb']
            elif self.curr_equip.cost_basis == 'Hours':
                equip_operating['operator_number'] = equip_operating['operator_number_hb']
                equip_operating['operator_cost'] = equip_operating['operator_cost_hb']
            elif self.curr_equip.cost_basis == 'Contractor_Cplus':
                equip_operating['operator_number'] = equip_operating['operator_number_hb']
                equip_operating['operator_cost'] = equip_operating['operator_cost_hb']* ( 1 +self.curr_equip.keyinputs.contractor_opfactor)
            elif self.curr_equip.cost_basis[0] == 'C':
                # TODO add contractor data
                equip_operating['operator_number'] = equip_operating['operator_number_hb']
                equip_operating['operator_cost'] = 0

            equip_operating['tradesmen_hrs'] = equip_operating[
                                                  'allocated_hrs'] * self.curr_equip.maint_ratio * self.curr_equip.keyinputs.tradesmen_factor
            equip_operating['tradesmen_cost'] = self.curr_equip.trades_cost * equip_operating['tradesmen_hrs']
            equip_operating['tradesmen_number'] = equip_operating[
                                                     'tradesmen_hrs'] / self.curr_equip.t_roster.manhour_worked / self.curr_equip.period_factor[period_sched]
            equip_operating['used_hrs'] += equip_operating['allocated_hrs']
            equip_operating['period_name'] = period_name[period_sched]
            self.fleet_data_run = self.fleet_data_run.append(equip_operating)
            self.fleet_data_run.fillna(0,inplace=True)
            if eq_purchase == 0:
                equip_data = {'Eq_Id': self.eq_id, 'purchase': 0, 'sched_period': period_num + 1,'period_name': self.period_name}
                self.equip_purchase_period = self.equip_purchase_period.append([equip_data])
                equip_data = {'Eq_Id': self.eq_id, 'capital':0, 'sched_period': period_num + 1, 'period_name': self.period_name}
                self.equip_capital_period = self.equip_capital_period.append([equip_data])

