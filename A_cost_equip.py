
#import costrun_class_period_DP as cran
from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
from PyQt5 import QtGui, QtCore
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
import cost_equip
import pnames


#costrun = cran.CostModel(1)


class Cost_equip_view(QMainWindow, cost_equip.Ui_MainWindow):
    #equip_select = QtCore.pyqtSignal(str)


    def __init__(self, costrun, equip_cost, parent=None):
        super(Cost_equip_view, self).__init__(parent)
        self.setupUi(self)
        self.equip_cost = equip_cost
        self.costrun = costrun

        self.comboBox_equip_details.clear()
        self.comboBox_equip_details.addItem(equip_cost.etype)

        for equip_count in self.costrun.all_equipment:
            equip_all = equip_count.etype
            self.comboBox_equip_details.addItem(equip_all)

        self.home()


    def home(self):

        self.listWidget_equip_name.clear()
        self.listWidget_equip_name.addItem(str(self.equip_cost.etype))

        self.listWidget_cost_basis.clear()
        self.listWidget_cost_basis.addItem(str(self.equip_cost.cost_basis))

        self.listWidget_hours_basis.clear()
        #self.listWidget_hours_basis.addItem(str(self.costrun.eq_activity.hour_basis))

        self.listWidget_cap_cost.clear()
        self.listWidget_cap_cost.addItem(str(round(self.equip_cost.capital_cost,1)))

        self.listWidget_life.clear()
        self.listWidget_life.addItem(str(self.equip_cost.life))

        self.listWidget_discount.clear()
        self.listWidget_discount.addItem(str(self.costrun.keyinputs.irate*100))

        self.listWidget_ann_op_hrs.clear()
        self.listWidget_ann_op_hrs.addItem(str(round(self.equip_cost.o_roster.annual_op_hours,1)))

        self.listWidget_contractor_cap.clear()                                      #contract component of ownership cost
        self.listWidget_contractor_cap.addItem(str("0"))

        self.listWidget_contractor_capfactor.clear()
        self.listWidget_contractor_capfactor.addItem(str(self.costrun.keyinputs.contractor_capfactor*100))

        self.listWidget_ownership.clear()
        self.listWidget_ownership.addItem(str(round(self.equip_cost.ownership_cost,1)))

        self.listWidget_operator.clear()
        self.listWidget_operator.addItem(str(self.equip_cost.operator_category))

        self.listWidget_roster.clear()
        self.listWidget_roster.addItem(str(self.equip_cost.op_roster))

        self.listWidget_op_labour_sal.clear()
        self.listWidget_op_labour_sal.addItem(str(self.equip_cost.operator_annual_cost))

        self.listWidget_op_labour.clear()
        self.listWidget_op_labour.addItem(str(round(self.equip_cost.operator_cost*self.equip_cost.annual_op_hours)))

        self.listWidget_op_manning.clear()
        self.listWidget_op_manning.addItem(str(self.equip_cost.operator_allocation*100))

        self.listWidget_op_labour_cost.clear()
        self.listWidget_op_labour_cost.addItem(str(round(self.equip_cost.operator_cost)))

        self.listWidget_roster_2.clear()
        self.listWidget_roster_2.addItem(str(self.equip_cost.t_roster.roster_name))

        self.listWidget_mtc_labour_sal.clear()
        self.listWidget_mtc_labour_sal.addItem(str(round(self.equip_cost.labour_cost("trades"))))

        self.listWidget_mtc_labour.clear()
        self.listWidget_mtc_labour.addItem(str(round(self.equip_cost.trades_cost*self.equip_cost.annual_op_hours*self.equip_cost.maint_ratio)))

        self.listWidget_mtc_ratio.clear()
        self.listWidget_mtc_ratio.addItem(str(self.equip_cost.maint_ratio))

        self.listWidget_mtc_cost_ophr.clear()
        self.listWidget_mtc_cost_ophr.addItem(str(round(self.equip_cost.trades_cost,1)))

        self.listWidget_oil_grease.clear()
        self.listWidget_oil_grease.addItem(str(self.equip_cost.oil_grease_cost))

        self.listWidget_tyres.clear()
        self.listWidget_tyres.addItem(str(round(self.equip_cost.tyre_cost,1)))

        self.listWidget_wearplates_get.clear()
        self.listWidget_wearplates_get.addItem(str(round(self.equip_cost.wear_get_cost,1)))

        self.listWidget_components.clear()
        self.listWidget_components.addItem(str(round(self.equip_cost.component_cost,1)))

        self.listWidget_minor_repair.clear()
        self.listWidget_minor_repair.addItem(str(round(self.equip_cost.minor_repair_costs,1)))

        self.listWidget_fuel_consump.clear()
        self.listWidget_fuel_consump.addItem(str(self.equip_cost.fuel_cons))

        self.listWidget_fuel_cost.clear()
        self.listWidget_fuel_cost.addItem(str(round(self.equip_cost.fuel_cost,1)))

        self.listWidget_opcost_fixed.clear()
        self.listWidget_opcost_fixed.addItem(str(round(self.equip_cost.fixed_costs,1)))

        self.listWidget_opcost_variable.clear()
        self.listWidget_opcost_variable.addItem(str(round(self.equip_cost.variable_costs,1)))

        self.listWidget_opcost_contract.clear()
        self.listWidget_opcost_contract.addItem(str(round(self.equip_cost.contract_cost_fixed+self.equip_cost.contract_cost_variable,1)))

        self.listWidget_opcost_all.clear()
        self.listWidget_opcost_all.addItem(str(round(self.equip_cost.fixed_costs+self.equip_cost.variable_costs+self.equip_cost.operator_cost+self.equip_cost.trades_cost,1)))

        #self.listWidget_contractor.clear()
        #self.listWidget_contractor.addItem(str("0"))

        #self.listWidget_contractor_opfactor.clear()
        #self.listWidget_contractor_opfactor.addItem(str(costrun.key_input_data.loc['contractor_opfactor','value']))




def main():
    costrunt = cran.CostModel(1)
    equip_cost = getattr(costrunt,"Cat_14M")
    app = QApplication(sys.argv)
    form = Cost_equip_view(costrunt,equip_cost)
    form.show()
    app.exec_()

if __name__ == '__main__':
    import sys
    main()
