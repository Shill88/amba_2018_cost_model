

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from pandas import read_hdf
from pandas import HDFStore

#Use python 2.7+ syntax to format currency
def money(x, pos):
    'The two args are the value and tick position'
    return "${:,.0f}".format(x)
formatter = FuncFormatter(money)

hdffinin = HDFStore('financial_data.h5')
findata = read_hdf(hdffinin,'report_names_npv')
findata2 = findata.copy()

mask = (findata2['report_name'] == ('Gross Value in Concentrate IMC')) | (findata2['report_name'] == ('Capital Cost Status Quo')) | \
       (findata2['report_name'] == ('Capital Cost Improvement')) | (findata2['report_name'] == ('Capital Cost Fleet')) | (findata2['report_name'] == ('Capital Salvage Income Fleet')) | \
        (findata2['report_name'] == ('Op Cost General and Admin')) | (findata2['report_name'] == ('Op Cost Mining Operations')) | (findata2['report_name'] == ('Op Cost Stockpile Reclaim')) | \
       (findata2['report_name'] == ('Op Cost Environmental')) | (findata2['report_name'] == ('Op Cost Process')) | (findata2['report_name'] == ('Total Realisation Costs IMC')) | (findata2['report_name'] == ('Royalty Paid')) | (findata2['report_name'] == ('Tax Paid')) | (findata2['report_name'] == ('Donations'))


data = findata2[mask].copy()

data.set_index(['report_name'],inplace=True)

data = data.multiply(-1)
mask = (data.index == 'Gross Value in Concentrate IMC') | (data.index == 'Capital Salvage Income Fleet')

data.loc[mask,'npv'] *= -1

GV = data.loc['Gross Value in Concentrate IMC']['npv']
CCSQ =  data.loc['Capital Cost Status Quo']['npv']
CCI =  data.loc['Capital Cost Improvement']['npv']
CCF =  data.loc['Capital Cost Fleet']['npv']
CSI =  data.loc['Capital Salvage Income Fleet']['npv']
OCG =  data.loc['Op Cost General and Admin']['npv']
OCO =  data.loc['Op Cost Mining Operations']['npv']
OCR =  data.loc['Op Cost Stockpile Reclaim']['npv']
OCE =  data.loc['Op Cost Environmental']['npv']
OCP =  data.loc['Op Cost Process']['npv']
RsC =  data.loc['Total Realisation Costs IMC']['npv']
Roy=  data.loc['Royalty Paid']['npv']
Don = data.loc['Donations']['npv']
Tax =  data.loc['Tax Paid']['npv']


#Data to plot. Do not include a total, it will be calculated
index = ['Gross Value','Cap SQ','Cap Improvement','Cap Fleet','Salvage', 'G & A', 'Mining Ops', 'Stockpile', 'Enviro', 'Process', 'Realisation', 'Donations', 'Royalty','Tax']

data = {'amount': [GV,CCSQ,CCI,CCF,CSI,OCG,OCO,OCR,OCE,OCP,RsC,Don, Roy,Tax]}

#Store data and create a blank series to use for the waterfall
trans = pd.DataFrame(data=data,index=index)
blank = trans.amount.cumsum().shift(1).fillna(0)



#Get the net total number for the final element in the waterfall
total = trans.sum().amount
trans.loc["NPV"]= total
blank.loc["NPV"] = total

#The steps graphically show the levels as well as used for label placement
step = blank.reset_index(drop=True).repeat(3).shift(-1)
step[1::3] = np.nan

#When plotting the last element, we want to show the full bar,
#Set the blank to 0
blank.loc["NPV"] = 0

#Plot and label
my_plot = trans.plot(kind='bar', stacked=True, bottom=blank,legend=None, figsize=(18, 9), title="NPV Waterfall")
my_plot.plot(step.index, step.values,'k')
my_plot.set_xlabel("Cost Centre")

#Format the axis for dollars
my_plot.yaxis.set_major_formatter(formatter)

#Get the y-axis position for the labels
y_height = trans.amount.cumsum().shift(1).fillna(0)

#Get an offset so labels don't sit right on top of the bar
max = trans.max()
neg_offset = max / 25
pos_offset = max / 50
plot_offset = int(max / 15)

#Start label loop
loop = 0
for index, row in trans.iterrows():
    # For the last item in the list, we don't want to double count
    if row['amount'] == total:
        y = y_height[loop]
    else:
        y = y_height[loop] + row['amount']
    # Determine if we want a neg or pos offset
    if row['amount'] > 0:
        y += pos_offset
    else:
        y -= neg_offset
    my_plot.annotate("{:,.0f}".format(row['amount']),(loop,y),ha="center")
    loop+=1

#Scale up the y axis so there is room for the labels
my_plot.set_ylim(0,blank.max()+int(plot_offset))
#Rotate the labels
my_plot.set_xticklabels(trans.index,rotation=0)
my_plot.get_figure().savefig("waterfall.png",dpi=400,bbox_inches='tight')