# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 23:05:37 2017

@author: slewis
"""
import pandas as pd

def grouped_weighted_average(self, values, weights, *groupby_args, **groupby_kwargs):
	"""
	:param values: column(s) to take the average of
	:param weights_col: column to weight on
	:param group_args: args to pass into groupby (e.g. the level you want to group on)
	:param group_kwargs: kwargs to pass into groupby
	:return: pandas.Series or pandas.DataFrame
	"""

	if isinstance(values, str):
	    values = [values]

	ss = []
	for value_col in values:
	    df = self.copy()
	    prod_name = 'prod_{v}_{w}'.format(v=value_col, w=weights)
	    weights_name = 'weights_{w}'.format(w=weights)

	    df[prod_name] = df[value_col] * df[weights]
	    df[weights_name] = df[weights].where(~df[prod_name].isnull())
	    df = df.groupby(*groupby_args, **groupby_kwargs).sum()
	    s = df[prod_name] / df[weights_name]
	    s.name = value_col
	    ss.append(s)
	df = pd.concat(ss, axis=1) if len(ss) > 1 else ss[0]
	return df