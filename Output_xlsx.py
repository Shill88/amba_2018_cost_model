from pandas import HDFStore
from pandas import read_hdf
#import xlrd
#import xlwt
import os.path
import openpyxl
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl import formatting, styles
import numpy as np
from numbers import Number

class Output_xlsx(object):
    def __init__(self, filename = 'cm_equipment_Coll.xlsx'):
        self.CONST_INPUT_FILE = filename

    #LOCATION TO PLACE THE FIRST YEAR COLUMN IN THE OUTPUT
    CONST_YEAR_COL = 4

    #Column to place the headings for the tables
    CONST_HEADING_COL = 2
    CONST_HEADING_FONT_STYLE = 0

    #INPUT AND OUTPUT FILE NAMES
    CONST_INPUT_FILE = 0
    CONST_OUTPUT_FILE = 'output.xlsx'

    __Current_Row = 1
    __hdftree = 0
    __hdfinput = 0
    __TableList = [['Total Cost Tree','tot_cost_tree','Year_1'], ['Capital','capital_data',1], ['Fleet Purchase','Total_Purchased_Fleet',1], ['Fuel','fuel_data','Year_1'],['Power','kw_data','Year_1'] ,['Water','water_data','Year_1'],['Reagents','reagents_data','Year_1'], ['Overheads', 'ohead_tree_cost', 'Year_1'], ['Admin Manning','admin_tree_number','Year_1'],['Manning Operators','op_equip_data','Year_1'],['Operating Fleet','Total_Operating_Fleet','Year_1'], ['Required Fleet','Total_Required_Fleet','Year_1']]

    #LIST OF COST TREE REPORTS TO RUN
    __REPORTS_TO_RUN = []

    def Get_Schedule_Information(self, current_report = 'report_flag', NameOfTab = 'Summary'):

        self.CONST_HEADING_FONT_STYLE = styles.Font(size = 11, bold = True, color = 'ffffff')

        #Read in the schedule data
        self.__hdftree = HDFStore('tree_data.h5')
        self.__hdfinput = HDFStore('input_data.h5')
        df = read_hdf(self.__hdfinput, 'sched_data_raw')
        df.values.tolist()

        #Read in the spreadsheet template info
        wb = openpyxl.load_workbook(self.CONST_INPUT_FILE,True)
        ws = wb['Report']
        wb.close
        wb2 = openpyxl.load_workbook(self.CONST_OUTPUT_FILE,False)
        #Append the tab if it doesn't exist
        if not(NameOfTab in wb2.sheetnames):
            wb2.create_sheet(NameOfTab,0)
            wb2._active_sheet_index = 0
        wo = wb2[NameOfTab]
        x=3
        for row in ws.rows:
            if(row[0].value == 'ADDTABLE'):
                self.AddTableItem(wb, wo, row[1].value, row[2].value, row[3].value, row[4].value, row[5].value, current_report)
                print("Add Table", row[1].value)
            elif((row[1].value != 'Name') and (row[1].value != 'N/A')):
                wo.cell(row=self.__Current_Row, column=self.CONST_YEAR_COL-2).value = row[1].value
                wo.cell(row=self.__Current_Row, column=self.CONST_YEAR_COL-1).value = row[2].value
                self.EmboldenFont(wo, self.__Current_Row, self.CONST_YEAR_COL-2,row[5].value)
                print("row[1].value is", row[1].value)
                if row[3].value != None:
                    #Get the divisor and make it 1 if it's not a number
                    divisor = row[4].value
                    if not(isinstance(divisor,Number)):
                        divisor = 1
                    RowSize = int(df.values[row[3].value].size)
                    #The schedule has a summation field at the start
                    j = self.CONST_YEAR_COL
                    for ColNum in range(3,RowSize):
                        if(isinstance(df.values[row[3].value-2].real[ColNum],Number)):
                            #propagate the schedule values, dividing each number by the divisor
                            wo.cell(row=self.__Current_Row, column=(j)).value = (df.values[row[3].value-2].real[ColNum])/divisor
                        else:
                            #if the values aren't numbers then don't divide
                            wo.cell(row=self.__Current_Row, column=(j)).value = df.values[row[3].value-2].real[ColNum]
                        j+=1
                self.__Current_Row+=1
        wb2.save(self.CONST_OUTPUT_FILE)
        wb.close
        wb2.close
        self.__hdftree.close
        self.__hdfinput.close
        self.__Current_Row = 1

    def EmboldenFont(self,wo,Row,Col, Bold):
            if(Bold == 'B'):
                cell = wo.cell(row=Row, column= Col)
                cell.font = cell.font.copy(bold=True)

    def is_nan(self,x):
            return (x is np.nan or x != x)


    def AddTableItem(self, wb, wo, TableName, Units, Datarow, Divisor, Bold, current_report):
        #Add the heading
        print("AddTableItem: ", TableName )


        if(TableName == 'Total Cost Tree'):
            self.__Current_Row += 1
            wo.cell(row=self.__Current_Row, column=self.CONST_HEADING_COL, value = TableName)
            self.EmboldenFont(wo,self.__Current_Row,self.CONST_HEADING_COL, Bold)
            ws = wb['cost_tree']
            #find the start of cost_tree (the first number in the tree equal to 1) 
            cost_tree_inc = 0
            col_inc = 2
            for cost_tree_item in ws.rows:
                #Get the number of reports to generate from the header row
                if(cost_tree_item[1].row == 1):
                    print("Item size is:", cost_tree_item[1].style_array.itemsize)
                    size = cost_tree_item[1].style_array.itemsize
                    if(len(self.__REPORTS_TO_RUN) == 0):
                        for kk in range(2,size+1):
                            self.__REPORTS_TO_RUN.append(cost_tree_item[kk].value)
                            col_inc += 1
                #Find the start of the cost_tree
                if(cost_tree_item[0].value == 1):
                    cost_tree_start = cost_tree_inc
                    break
                cost_tree_inc += 1


            #Get the cost tree information
            start_col = self.FindYear1(self.__hdftree,[y for y in self.__TableList if TableName in y])
            offset = self.CONST_YEAR_COL - start_col - 1
            df2 = read_hdf(self.__hdftree, 'tot_cost_tree')

            rows = dataframe_to_rows(df2)

            #Find the report we are using and select the column to use to exclude/incorporate data

            for report_flag_col, report_flag_item in enumerate(self.__REPORTS_TO_RUN):
                if report_flag_item == current_report:
                    #Add the tree and name column
                    report_flag_col += 2
                    break;

            #skip the items with report_flag not set
            found = False
            tree_inc = 1
            for r_idx, row in enumerate(rows, 1):
                if not(found):
                    #Ignore the rows until a 1 is found in the row in the 2nd column (this is the start of the tree)
                    if(row[2] == 1):
                        found = True
                        tree_inc = cost_tree_start
                for c_idx, value in enumerate(row, 1):
                    if(c_idx == 1 or c_idx ==2):     #Don't print columns 1 and 2
                        pass
                    elif(c_idx == 3 or c_idx == 4):  #Always print columns 2 & 3 (the tree item and name)
                        wo.cell(row=r_idx+ self.__Current_Row, column=c_idx+offset, value=value)
                    else:                            #Only print the rest of the columns if flagged
                        if found and (not(ws[tree_inc+1][report_flag_col].value is None)):
                            if(c_idx == 5):         #Add the units in
                                wo.cell(row=r_idx+ self.__Current_Row, column=c_idx+offset, value=Units)
                            else:                   #Output the values to the end of the period
                                wo.cell(row=r_idx+ self.__Current_Row, column=c_idx+offset, value=value)
                tree_inc += 1
            self.__Current_Row = r_idx + self.__Current_Row + 2
        elif(TableName == 'Capital'):
            wo.cell(row=self.__Current_Row, column=self.CONST_HEADING_COL, value = TableName)
            self.EmboldenFont(wo,self.__Current_Row,self.CONST_HEADING_COL, Bold)
            self.__Current_Row += 1
            start_col = self.FindYear1(self.__hdfinput,[y for y in self.__TableList if TableName in y])
            offset = self.CONST_YEAR_COL - start_col
            self.__Current_Row = self.AddTable(self.__hdfinput, wo, [y for y in self.__TableList if TableName in y], self.__Current_Row, Units, 3, False,2)
        elif(TableName == 'Admin Manning'):
            wo.cell(row=self.__Current_Row, column=self.CONST_HEADING_COL, value = TableName)
            self.EmboldenFont(wo,self.__Current_Row,self.CONST_HEADING_COL, Bold)
            self.__Current_Row += 1
            start_col = self.FindYear1(self.__hdftree,[y for y in self.__TableList if TableName in y])
            offset = self.CONST_YEAR_COL - start_col - 1
            self.__Current_Row = self.AddTableManning(self.__hdftree, wo, [y for y in self.__TableList if TableName in y], self.__Current_Row, Units, 3, True,offset)
        else:
            wo.cell(row=self.__Current_Row, column=self.CONST_HEADING_COL, value = TableName)
            self.EmboldenFont(wo,self.__Current_Row,self.CONST_HEADING_COL, Bold)
            self.__Current_Row += 1
            start_col = self.FindYear1(self.__hdftree,[y for y in self.__TableList if TableName in y])
            offset = self.CONST_YEAR_COL - start_col - 1
            self.__Current_Row = self.AddTable(self.__hdftree, wo, [y for y in self.__TableList if TableName in y], self.__Current_Row, Units, 3, True,offset)
        return 0;




    def AddTable(self, hdftree, wo, table_name, row_count, Units, start_column = 3, remove_zeros= False, col_offset = 0,):
        df = read_hdf(hdftree, table_name[0][1])
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            temp = row[-len(row)+4:]
            if not(remove_zeros):
                all_zero = False
            else: 
                all_zero = True
            for item in temp:
                if (not(self.is_nan(item) or item == 0.0) and remove_zeros):
                    all_zero = False
                    break;
            if not(all_zero):
                for c_idx, value in enumerate(row, 1):
                    if(c_idx > start_column):
                        if(c_idx + col_offset == 2):
                            wo.cell(row=row_count, column=c_idx + col_offset, value=value)
                        elif(c_idx + col_offset == 3):
                            #Add in the units but not for row 0 because this is the header row
                            if(r_idx > 1):
                                wo.cell(row=row_count, column=c_idx + col_offset, value=Units)
                            wo.cell(row=row_count, column=c_idx + col_offset+1, value=value)
                        else:
                            wo.cell(row=row_count, column=c_idx + col_offset+1, value=value)                            
                row_count+=1
        row_count+=1
        return(row_count);

    def AddTableManning(self, hdftree, wo, table_name, row_count, Units, start_column = 3, remove_zeros= False, col_offset = 0,):
        df = read_hdf(hdftree, table_name[0][1])
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            temp = row[-len(row)+4:]
            if not(remove_zeros):
                all_zero = False
            else: 
                all_zero = True
            for item in temp:
                if (not(self.is_nan(item) or item == 0.0) and remove_zeros):
                    all_zero = False
                    break;
            if not(all_zero):
                for c_idx, value in enumerate(row, 1):
                    if(c_idx > start_column -1):
                        if(c_idx + col_offset == 2):
                            wo.cell(row=row_count, column=c_idx + col_offset, value=value)
                        elif(c_idx == 4 and r_idx > 2):
                            wo.cell(row=row_count, column=1, value = value)
                        elif(c_idx + col_offset == 3):
                            #Add in the units but not for row 0 because this is the header row
                            if(r_idx > 1):
                                wo.cell(row=row_count, column=c_idx + col_offset, value=Units)
                            wo.cell(row=row_count, column=c_idx + col_offset+1, value=value)
                        else:
                            wo.cell(row=row_count, column=c_idx + col_offset+1, value=value)                            
                row_count+=1
        row_count+=1
        return(row_count);

    def FindYear1(self,hdftree,table_name):
        df = read_hdf(hdftree, table_name[0][1])
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows,1):
            for c_idx, value in enumerate(row, 1):
                if value == table_name[0][2]:
                    return c_idx;
        return 0;

def main():
    OutputData = Output_xlsx()
    OutputData.Get_Schedule_Information()
    i = 1
    NameOfTab = 'Summary'

    for val, list in enumerate(OutputData._Output_xlsx__REPORTS_TO_RUN):
        if (val == 0):
            continue
        print("Report Name is: {}",list)
        NewTab = NameOfTab + "_" + (str(i))
        print("Name of Tab is: {}", NewTab)
        OutputData.Get_Schedule_Information(list, NewTab)
        i+=1





    
if __name__ == '__main__':
    
    import sys
    main()
      