
import pandas

class XNPV:
    def xnpv_me(self, rate, cashflow):
        self.rate = rate
        cf = cashflow
        cf_xnpv_days_pvs = [cf[i]/float(1+(1+rate)**(float((cf.index[i]-cf.index[0]).days)/365.0)-1)  for i in range(1,len(cf.index))]
        cf_xnpv = cf[0]+ sum(cf_xnpv_days_pvs)
        return cf_xnpv

if __name__ == '__main__':
    xnpvcalc = XNPV()
    datesin = pandas.to_datetime(['jan-2017', 'feb-2017'])
    cfin = [-500, 600]
    cflow = pandas.Series(cfin, index=datesin)

    ratein = 0.1

    fred = xnpvcalc.xnpv_me(ratein,cflow)


