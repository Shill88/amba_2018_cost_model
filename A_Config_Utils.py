

from configparser import ConfigParser

class Read_Config:
  def read_config(self, filename, section):
      """ Read database configuration file and return a dictionary object
      :param filename: name of the configuration file
      :param section: section of database configuration
      :return: a dictionary of database parameters
      """
      self.filename=filename
      self.section=section
      # create parser and read ini configuration file
      parser = ConfigParser()
      parser.read(self.filename)
      self.fileN = {}
      if parser.has_section(self.section):
          self.items = parser.items(self.section)
          for item in self.items:
            self.fileN[item[0]] = item[1]
      else:
          raise Exception('{0} not found in the {1} file'.format(self.section, self.filename))

      return self.fileN