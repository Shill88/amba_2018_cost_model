

from PyQt5.QtCore import QAbstractItemModel, QFile, QIODevice, QModelIndex, Qt
from PyQt5.QtWidgets import QApplication, QTreeView
from pandas import HDFStore
import sys
import pandas as pd
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf

class TreeItem(object):
    def __init__(self, data, parent=None):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []


    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return len(self.itemData)

    def data(self, column):
        try:
            return self.itemData[column]
        except IndexError:
            return None

    def parent(self):
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)

        return 0

    def __iter__(self):
        return iter(self.childItems)

    def depth_first(self):
        yield self

        for c in self:
            yield from c.depth_first()


class TreeModel(QAbstractItemModel):
    def __init__(self, headers, data, parent=None):
        super(TreeModel, self).__init__(parent)

        rootData = [header for header in headers]
        self.rootItem = TreeItem(rootData)
        self.setupModelData(data, self.rootItem)

    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != Qt.DisplayRole:
            return None

        item = index.internalPointer()

        return item.data(index.column())

    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.rootItem.data(section)

        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def setupModelData(self, lines, parent):
        parents = [parent]
        indentations = [0]
        number = 0

        while number < lines.shape[0]:
            position = lines.iloc[number,0]
            lineData = str(lines.iloc[number,1])

            if lineData:
                # Read the column data from the rest of the line.
                columnData = []
                try:
                    columnData.append(str(lines.iloc[number, 1]) + ' ' + str(lines.iloc[number, 2]))
                except:
                    columnData.append(str(lines.iloc[number, 1]) + ' ' + lines.iloc[number, 2])

                for colnum in range(3,lines.shape[1]):
                    try:
                        if pd.isnull(lines.iloc[number, colnum]):
                            datacol = ' '
                        else:
                            datacol = '%.2f' % lines.iloc[number, colnum]

                        columnData.append(datacol)
                    except:
                        if pd.isnull(str(lines.iloc[number,colnum])):
                            datacol = ' '
                        else:
                            datacol = str(lines.iloc[number,colnum])

                        columnData.append(datacol)

                if position > indentations[-1]:
                    # The last child of the current parent is now the new
                    # parent unless the current parent has no children.

                    if parents[-1].childCount() > 0:
                        parents.append(parents[-1].child(parents[-1].childCount() - 1))
                        indentations.append(position)

                else:
                    while position < indentations[-1] and len(parents) > 0:
                        parents.pop()
                        indentations.pop()

                # Append a new item to the current parent's list of children.
                parents[-1].appendChild(TreeItem(columnData, parents[-1]))

            number += 1



if __name__ == '__main__':

    import sys
    import pandas as pd
    import pandas as pd
    from pandas import HDFStore
    from pandas import read_hdf

    app = QApplication(sys.argv)
    hdfin = HDFStore('data.h5')
    data_view = 'opnum_tree'
    cost_tree = read_hdf(hdfin, data_view)
    headers = list(cost_tree)
    headers.pop(0)

    model = TreeModel(headers, cost_tree)

    view = QTreeView()
    view.setModel(model)
    view.setWindowTitle(data_view)
    view.expandAll()
    view.resizeColumnToContents(0)
    view.resizeColumnToContents(1)
    #view.resizeColumnToContents(2)
    #view.resizeColumnToContents(3)
    #view.resizeColumnToContents(4)
    #view.resizeColumnToContents(5)



    view.show()
    sys.exit(app.exec_())
