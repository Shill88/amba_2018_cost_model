
import costrun_class_period_DP as cran
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
import pnames
import xnpv

xnpvcalc = xnpv.XNPV()


period_namer = pnames.period_names()



hdfout = HDFStore('data.h5')

costrun = cran.CostModel(1)

#fleetdata = costrun.Cat_745.fleet_data

hdfyears = HDFStore('year_data.h5')
hdftree = HDFStore('tree_data.h5')
hdfplot = HDFStore('plot_data.h5')

costrun.cost_tree_sort.set_index(['act_level'],inplace=True)
costrun.cost_tree_sort.reset_index(inplace=True)
cost_tree_sort_tmp = costrun.cost_tree_sort.drop('sort_order',1)

hdftree.put('cost_tree_sort',cost_tree_sort_tmp)
hdftree.flush()

caldat= costrun.calendar.T
caldat.reset_index(inplace=True)
caldat.rename(index=str, columns={'index': 'sort_order'}, inplace=True)
caldat.set_index(['Name'],inplace=True)
caldat = caldat.T
callist = list(caldat)

caldat= costrun.calendar.T
caldat.reset_index(inplace=True)
caldat.rename(index=str, columns={'index': 'sort_order'}, inplace=True)
caldat.set_index(['month_year'],inplace=True)
caldat = caldat.T
callist_my = list(caldat)

countme = 0
for fleetcount in costrun.all_equipment:
    if countme == 0:
        fleet_operating_all = fleetcount.fleet_operating
        countme+=1
    else:
        fleet_operating_all = fleet_operating_all.append(fleetcount.fleet_operating)
    print('fleet operating: ', fleetcount.etype)

countme = 0

for fleetcount in costrun.all_equipment:
    if countme == 0:
        fleet_required_all = fleetcount.fleet_required
        countme+=1
    else:
        fleet_required_all = fleet_required_all.append(fleetcount.fleet_required)
    print('fleet required: ', fleetcount.etype)

countme = 0

for fleetcount in costrun.all_equipment:
    if countme == 0:
        fleet_purchased_all = fleetcount.fleet_purchased
        countme+=1
    else:
        fleet_purchased_all = fleet_purchased_all.append(fleetcount.fleet_purchased)

countme = 0
for fleetcount in costrun.all_equipment:
    if countme == 0:
        fleet_salvaged_all = fleetcount.fleet_salvaged
        countme+=1
    else:
        fleet_salvaged_all = fleet_salvaged_all.append(fleetcount.fleet_salvaged)

countme = 0

for fleetcount in costrun.all_equipment:
    if countme == 0:
        fleet_data_all = fleetcount.fleet_data
        countme+=1
    else:
        fleet_data_all = fleet_data_all.append(fleetcount.fleet_data)

countme = 0
for admincount in costrun.all_admin:
    if countme == 0:
        admin_data_all = admincount.cost_period
        countme += 1
    else:
        admin_data_all = admin_data_all.append(admincount.cost_period)
    print('Admin: ', admincount.aname)

countme = 0
for oheadcount in costrun.all_ohead:
    if countme == 0:
        ohead_data_all = oheadcount.cost_period
        countme += 1
    else:
        ohead_data_all = ohead_data_all.append(oheadcount.cost_period)

opnum_data = pd.DataFrame()
fuel_data = pd.DataFrame()
for equip in costrun.all_equipment:
    try:
        fuel_tmp = equip.fleet_data.groupby(['sched_period'])['fuel_litres'].sum()
        fuel_qty = fuel_tmp.reset_index()
        #fuel_qty.rename(index=str, columns={'index': 'sched_period'}, inplace=True)
        fuel_qty.set_index(['sched_period'],inplace=True)
        #fuel_qty.rename(index=str, columns={5: 'fuel_litres'}, inplace=True)
        fuel_qty = fuel_qty.T
        fuel_qty['Equip'] = equip.etype
        fuel_qty.reset_index(inplace=True)
        fuel_qty.rename(index=str, columns={'index': 'Act_Id'}, inplace=True)

        fuel_qty.set_index(['Act_Id', 'Equip'], inplace=True)
        fuel_qty.reset_index(inplace=True)

        fuel_data = fuel_data.append(fuel_qty)
    except:
        pass

fuel_data['Act_Id'] = 1

fuel_data['act_level'] = 2
fuel_data.set_index(['act_level','Act_Id'],inplace=True)
fuel_data.reset_index(inplace=True)
tot_fuel_data = fuel_data.sum(axis=0)
tot_fuel_data = tot_fuel_data.reset_index()
tot_fuel_data.rename(columns={0:'total_fuel'},inplace=True)
tot_fuel_data.iloc[0:3,1] = ' '
tot_fuel_data.set_index(['sched_period'],inplace=True)
tot_fuel_data = tot_fuel_data.T
tot_fuel_data['Act_Id'] = 1
tot_fuel_data['act_level'] = 1
tot_fuel_data['Equip'] = 'Total'
#tot_fuel_data = tot_fuel_data.append(tot_fuel_data)

tot_fuel_data = tot_fuel_data.append(fuel_data)

curdat_join = period_namer.period_me(callist, tot_fuel_data,3)

hdftree.put('fuel_data',curdat_join)

fuel_data = curdat_join[1:].copy()

fuel_data.set_index(['Equip'],inplace=True)

fuel_data.drop(['act_level','Act_Id'],axis = 1,inplace=True)
fuel_data = fuel_data.T

hdfplot.put('fuel_data', fuel_data)


for equip in costrun.all_equipment:
    for acts in equip.act_tree:
        try:
            op_num = acts.operator_number.reset_index()
            op_num.rename(index=str, columns={'index': 'sched_period'}, inplace=True)
            op_num.rename(index=str, columns={0: 'operator_number'}, inplace=True)
            op_num.set_index(['sched_period'],inplace = True)
            op_num = op_num.T
            op_num.index.values[0] = acts.act_tree
            op_num['Equip'] = acts.eqtype
            op_num.reset_index(inplace=True)
            op_num.rename(index=str, columns={'index': 'Act_Id'}, inplace=True)
            op_num.set_index(['Act_Id', 'Equip'], inplace=True)
            op_num.reset_index(inplace=True)

            opnum_data = opnum_data.append(op_num)
        except:
            pass

opnum_data.set_index(['Act_Id'],inplace=True)
costrun.cost_tree_sort.set_index(['Act_Id'],inplace=True)
opnum_tree = costrun.cost_tree_sort.join(opnum_data)
opnum_tree.reset_index(inplace=True)

opnum_tree.set_index(['act_level','Act_Id'],inplace=True)
opnum_tree.sort_values('sort_order',inplace=True)
opnum_tree = opnum_tree.drop('sort_order', 1)
opnum_tree.reset_index(inplace=True)

opnum_tree = opnum_tree.round(2)

curdat_join = period_namer.period_me(callist, opnum_tree,4)

hdftree.put('opnum_tree',curdat_join)

pname = ohead_data_all[['sched_period','period_name']]
pname.set_index(['sched_period'],inplace=True)

sum_data = ['staff_number']
admin_data_sums = admin_data_all.groupby(['sched_period','admin_name'])[sum_data].sum()
admin_data_sums = admin_data_sums.reset_index()
admin_data_sched_period=pd.pivot_table(admin_data_sums,index=['admin_name'],columns=['sched_period'])

hdfout.put('admin_number',admin_data_sched_period['staff_number'])

sum_data = ['cost']

admin_data_sums = admin_data_all.groupby(['sched_period','admin_name'])[sum_data].sum()
admin_data_sums = admin_data_sums.reset_index()
admin_data_sched_period=pd.pivot_table(admin_data_sums,index=['admin_name'],columns=['sched_period'])

hdfout.put('admin_cost',admin_data_sched_period['cost'])

"""ohead"""

sum_data = ['cost']
ohead_data_sums = ohead_data_all.groupby(['sched_period','ohead_name'])[sum_data].sum()
ohead_data_sums = ohead_data_sums.reset_index()
ohead_data_sched_period = pd.pivot_table(ohead_data_sums,index=['ohead_name'],values=['cost'],columns=['sched_period'])
hdfout.put('ohead_period',ohead_data_sched_period)

fleet_data_all['total_cost'] = fleet_data_all['operator_cost'] + fleet_data_all['tradesmen_cost'] + fleet_data_all['variable_cost'] + fleet_data_all['fixed_cost']

sum_data = ['allocated_hrs','operator_cost','operator_number','tradesmen_hrs','tradesmen_cost','tradesmen_number','variable_cost','fixed_cost','total_cost']

fleet_data_all_sums = fleet_data_all.groupby(['sched_period','Eq_Id'])[sum_data].sum()
fleet_data_all_sums.reset_index(inplace=True)
fleet_data_all_temp = pd.pivot_table(fleet_data_all_sums,index=['Eq_Id'],columns=['sched_period'])
fleet_data_all_temp.reset_index(inplace=True)

fleet_data_all_temp['sort_order'] = fleet_data_all_temp.index
row_previous = 'fred'

sort_order = fleet_data_all_temp['sort_order'] * 100
fleet_ID = fleet_data_all_temp['Eq_Id']
fleet_ID_master = fleet_ID.reset_index()
fleet_ID_master = fleet_ID_master.join(sort_order)
fleet_ID_master['act_level'] = 1
fleet_ID_master = fleet_ID_master.drop('index', 1)

count = 1
for colname in sum_data:
    sort_order = sort_order + count
    vars()[colname] = fleet_data_all_temp[colname]
    vars()[colname] = vars()[colname].join(fleet_ID)
    vars()[colname] = vars()[colname].join(sort_order)
    vars()[colname]['Eq_Id'] = colname
    vars()[colname]['act_level'] = 2
    fleet_ID_master = fleet_ID_master.append(vars()[colname])
    count += 1

fleet_ID_master.set_index(['sort_order','act_level','Eq_Id'],inplace=True)
fleet_ID_master.reset_index(inplace=True)
fleet_ID_master = fleet_ID_master.sort_values('sort_order')
fleet_ID_master.set_index(['sort_order'],inplace=True)
hdfout.put('fleet_cost_details',fleet_ID_master)

fleet_ID_master['Act_Id'] = 1
fleet_ID_master.set_index(['act_level','Act_Id'],inplace=True)
fleet_ID_master.reset_index(inplace=True)

curdat_join = period_namer.period_me(callist, fleet_ID_master,3)

hdftree.put('fleet_cost_details',curdat_join)

fleet_data_all_totals = fleet_data_all.groupby(['sched_period'])[sum_data].sum()
fleet_data_all_totals = fleet_data_all_totals.T
hdfout.put('period_summary',fleet_data_all_totals)
fleet_data_all_totals.reset_index(inplace=True)
#temp = fleet_data_all_totals[-1:]
#fleet_data_all_totals = fleet_data_all_totals.append(temp)
fleet_data_all_totals['act_level'] = 2
fleet_data_all_totals['Act_Id'] = 1
mask = fleet_data_all_totals['index'] == 'total_cost'
fleet_data_all_totals.loc[mask,'act_level'] = 1

fleet_data_all_totals.set_index(['act_level','Act_Id'],inplace=True)
fleet_data_all_totals.reset_index(inplace=True)
fleet_data_all_totals.sort_values(['act_level'],inplace=True)
fleet_data_all_totals.reset_index(inplace=True)
collist = list(fleet_data_all_totals)
fleet_data_all_totals.drop(collist[0], axis=1, inplace=True)


curdat_join = period_namer.period_me(callist, fleet_data_all_totals,3)

hdftree.put('total_costs_and_manning',curdat_join)

equip_data_all = pd.pivot_table(fleet_data_all_sums,index=['Eq_Id'],columns=['sched_period'])
equip_data_all_totals = equip_data_all.append(pd.Series(equip_data_all.sum(),name='Total'))
hdfout.put('equip_consolidated_period',equip_data_all_totals)

alloc_hrs = equip_data_all_totals['allocated_hrs'].copy()
alloc_hrs['act_level'] = 2
alloc_hrs['Act_Id'] = 1
alloc_hrs.reset_index(inplace=True)
mask = alloc_hrs['Eq_Id'] == 'Total'
alloc_hrs.loc[mask,'act_level'] = 1
alloc_hrs.set_index(['act_level','Act_Id'],inplace=True)
alloc_hrs.reset_index(inplace=True)
alloc_hrs.sort_values(['act_level'],inplace=True)
alloc_hrs.reset_index(inplace=True)
collist = list(alloc_hrs)
alloc_hrs.drop(collist[0], axis=1, inplace=True)

curdat_join = period_namer.period_me(callist,alloc_hrs,3)

hdftree.put('Total_Allocated_Hours',curdat_join)

operator_data_sums = fleet_data_all.groupby(['sched_period','operator_category'])['operator_number'].sum()
operator_data_sums = operator_data_sums.reset_index()
hdfout.put('operators',operator_data_sums)

equip_operating_period = pd.pivot_table(fleet_operating_all, index=['Eq_Id'], values=['opnum'], columns=['sched_period'])
equip_purchased_period=pd.pivot_table(fleet_purchased_all, index=['Eq_Id'], values=['purchase'], columns=['sched_period'])
equip_required_period=pd.pivot_table(fleet_required_all, index=['Eq_Id'], values=['opnum'], columns=['sched_period'])
equip_required_period.reset_index(inplace=True)
hdfout.put('equipment_required_period',equip_required_period)

equip_required = equip_required_period.set_index(['Eq_Id'])
equip_required = equip_required['opnum']
equip_required.reset_index(inplace=True)
equip_required['act_level'] = 1
equip_required['Act_Id'] = 1
equip_required.set_index(['act_level','Act_Id'],inplace=True)
equip_required.reset_index(inplace=True)
equip_required.sort_values(['act_level'],inplace=True)
equip_required.reset_index(inplace=True)
collist = list(equip_required)
equip_required.drop(collist[0], axis=1, inplace=True)

curdat_join = period_namer.period_me(callist, equip_required,3)

hdftree.put('Total_Required_Fleet',curdat_join)

equip_operating = equip_operating_period['opnum'].copy()
equip_operating.reset_index(inplace=True)
equip_operating['act_level'] = 1
equip_operating['Act_Id'] = 1
equip_operating.set_index(['act_level','Act_Id'],inplace=True)
equip_operating.reset_index(inplace=True)
equip_operating.sort_values(['act_level'],inplace=True)
equip_operating.reset_index(inplace=True)
collist = list(equip_operating)
equip_operating.drop(collist[0], axis=1, inplace=True)

curdat_join = period_namer.period_me(callist, equip_operating,3)

hdftree.put('Total_Operating_Fleet',curdat_join)

equip_purchased = equip_purchased_period['purchase'].copy()
equip_purchased.reset_index(inplace=True)
equip_purchased['act_level'] = 1
equip_purchased['Act_Id'] = 1
equip_purchased.set_index(['act_level','Act_Id'],inplace=True)
equip_purchased.reset_index(inplace=True)
equip_purchased.sort_values(['act_level'],inplace=True)
equip_purchased.reset_index(inplace=True)
collist = list(equip_purchased)
equip_purchased.drop(collist[0], axis=1, inplace=True)

hdftree.put('Total_Purchased_Fleet',equip_purchased)

operator_data_sched_period=pd.pivot_table(operator_data_sums,index=['operator_category'],values = ['operator_number'],columns=['sched_period'])

ohead_total_cost = ohead_data_all.groupby(['sched_period'])['cost'].sum()
ohead_total_cost = ohead_total_cost.reset_index()
ohead_total_cost.rename(columns={'cost':'ohead'},inplace=True)
ohead_total_cost.set_index(['sched_period'],inplace=True)
hdfout.put('total_ohead_costs',ohead_total_cost)

admin_total_cost = admin_data_all.groupby(['sched_period'])['cost'].sum()
admin_total_cost = admin_total_cost.reset_index()
admin_total_cost.rename(columns={'cost':'admin'},inplace=True)
admin_total_cost.set_index(['sched_period'],inplace=True)
o_a_tot = pd.concat([admin_total_cost,ohead_total_cost],axis=1)
o_a_tot = o_a_tot.T
hdfout.put('total_admin_costs',admin_total_cost)

tot_op_cost = fleet_data_all.groupby(['sched_period'])['total_cost'].sum()
tot_op_cost = tot_op_cost.reset_index()
tot_op_cost.rename(columns={'total_cost':'total_opcost'},inplace=True)
tot_op_cost.set_index(['sched_period'],inplace=True)
tot_op_cost = tot_op_cost.T
total_costs = o_a_tot.append(tot_op_cost)
total_costs = total_costs.append(pd.Series(total_costs.sum(),name='Total'))
total_costs.reset_index(inplace=True)
hdfout.put('total_costs',total_costs)
hdfout.put('total_op_costs',tot_op_cost)

total_costs_pername = period_namer.period_me(callist_my, total_costs,1)
total_costs_pername.set_index(['index'],inplace=True)
cflow = total_costs_pername.loc['Total']
ratein = costrun.keyinputs.discount_rate
xnpv = xnpvcalc.xnpv_me(ratein, cflow)
dcf_data = pd.DataFrame([{'disc_cost': 'total_cost', 'xnpv': xnpv}])
hdfplot.put('total_cost_disc', dcf_data)
hdfplot.flush()




# TODO make this so
total_costs_tree = total_costs
#temp = total_costs_tree[-1:]
#total_costs_tree = total_costs_tree.append(temp)
total_costs_tree['act_level'] = 2
total_costs_tree['Act_Id'] = 1
mask = total_costs_tree['index'] == 'Total'
total_costs_tree.loc[mask,'act_level'] = 1
total_costs_tree.set_index(['act_level','Act_Id'],inplace=True)
total_costs_tree.reset_index(inplace=True)
total_costs_tree.sort_values(['act_level'],inplace=True)
total_costs_tree.reset_index(inplace=True)
collist = list(total_costs_tree)
total_costs_tree.drop(collist[0], axis=1, inplace=True)

curdat_join = period_namer.period_me(callist, total_costs_tree,3)
hdftree.put('Total_Cost',curdat_join)

hdfout.put('fleet_operating_all',fleet_operating_all)
hdfout.put('fleet_DATA_all',fleet_data_all)

roster_data = costrun.roster_input.copy()
roster_data.reset_index(inplace=True)
roster_data['Act_Id'] = 1
roster_data['act_level'] = 2
roster_data.set_index(['act_level','Act_Id'],inplace=True)
roster_data.reset_index(inplace=True)
roster_header = {'Act_Id': 'Roster', 'act_level': 1}
roster_data = roster_data.append([roster_header])
roster_data.sort_values(['act_level'],inplace=True)
#roster_data['Roster_Name'] = roster_data['type']
#roster_data.set_index(['act_level','Act_Id','type','Roster_Name'],inplace=True)
#roster_data.reset_index(inplace=True)

#roster_data.drop('type',1,inplace=True)

hdftree.put('Roster_Data',roster_data)




admin_number = pd.DataFrame()
for admin in costrun.all_admin:
    for acts in admin.act_tree:
        try:
            admin_num = acts.staff_number.reset_index()
            admin_num.rename(columns={'index': 'sched_period'}, inplace=True)
            admin_num = admin_num.set_index(['sched_period'])
            admin_num = admin_num.T
            #admin_num = admin_num.reset_index()
            admin_num['Act_Id'] = acts.act_tree
            #admin_num['Role'] = acts.atype
            admin_num.reset_index(inplace=True)
            admin_num.rename(columns={'index': 'Role'}, inplace=True)
            #admin_num.rename(index=str, columns={'index': 'Act_Id'}, inplace=True)
            admin_num.set_index(['Act_Id', 'Role'], inplace=True)
            admin_num.reset_index(inplace=True)

            admin_number = admin_number.append(admin_num)
        except:
            pass

admin_number.set_index(['Act_Id'],inplace=True)
admin_tree_number = costrun.cost_tree_sort.join(admin_number)
admin_tree_number.reset_index(inplace=True)
admin_tree_number.set_index(['act_level','Act_Id'],inplace=True)
admin_tree_number.sort_values('sort_order',inplace=True)
admin_tree_number = admin_tree_number.drop('sort_order', 1)
admin_tree_number.reset_index(inplace=True)

admin_tree_number = admin_tree_number.round(1)

curdat_join = period_namer.period_me(callist, admin_tree_number,4)
#TODO fix up the input to be years
hdftree.put('admin_tree_number',curdat_join)

eq_activity = costrun.eq_activity
eq_activity.rename(columns={'activity':'Act_Id'},inplace=True)
eq_activity.reset_index(inplace=True)
eq_activity.set_index(['Act_Id'],inplace=True)
eq_activity_tree = costrun.cost_tree_sort.join(eq_activity)
eq_activity_tree.reset_index(inplace=True)
eq_activity.rename(columns={'activity':'Act_Id'},inplace=True)
eq_activity_tree.set_index(['act_level','Act_Id'],inplace=True)
eq_activity_tree.sort_values('sort_order',inplace=True)
eq_activity_tree = eq_activity_tree.drop('sort_order', 1)
eq_activity_tree.reset_index(inplace=True)

eq_activity_tree = eq_activity_tree.round(2)

hdftree.put('Equipment_Activity_Inputs',eq_activity_tree)

admin_cost_tab = pd.DataFrame()

for admin in costrun.all_admin:
    for acts in admin.act_tree:
        try:
            admin_cost = acts.cost_period.reset_index()
            admin_cost = admin_cost[['sched_period','cost']]
            admin_cost = admin_cost.set_index(['sched_period'])
            admin_cost = admin_cost.T
            admin_cost['Act_Id'] = acts.act_tree
            admin_cost['Role'] = acts.atype
            admin_cost.reset_index(inplace=True)
            #admin_cost.rename(index=str, columns={'index': 'Act_Id'}, inplace=True)
            admin_cost.set_index(['Act_Id', 'Role'], inplace=True)
            admin_cost.reset_index(inplace=True)
            admin_cost.drop('index', axis=1, inplace=True)

            admin_cost_tab = admin_cost_tab.append(admin_cost)
        except:
            pass

admin_cost_tab.set_index(['Act_Id'],inplace=True)
admin_tree_cost = costrun.cost_tree_sort.join(admin_cost_tab)
admin_tree_cost.reset_index(inplace=True)
admin_tree_cost.set_index(['act_level','Act_Id'],inplace=True)
admin_tree_cost.sort_values('sort_order',inplace=True)
admin_tree_cost = admin_tree_cost.drop('sort_order', 1)
admin_tree_cost.reset_index(inplace=True)

admin_tree_cost = admin_tree_cost.round(1)

curdat_join = period_namer.period_me(callist, admin_tree_cost,4)

hdftree.put('admin_tree_cost',curdat_join)

ohead_cost_tab = pd.DataFrame()
for ohead in costrun.all_ohead:
    for acts in ohead.act_tree:
        try:
            ohead_cost = acts.cost_period.reset_index()
            ohead_cost = ohead_cost[['sched_period','cost']]
            ohead_cost = ohead_cost.set_index(['sched_period'])
            ohead_cost = ohead_cost.T
            ohead_cost['Act_Id'] = acts.act_tree
            ohead_cost['OHead'] = acts.otype
            ohead_cost.reset_index(inplace=True)
            #ohead_cost.rename(index=str, columns={'index': 'Act_Id'}, inplace=True)
            ohead_cost.set_index(['Act_Id', 'OHead'], inplace=True)
            ohead_cost.reset_index(inplace=True)
            ohead_cost.drop('index', axis=1, inplace=True)

            ohead_cost_tab = ohead_cost_tab.append(ohead_cost)
        except:
            pass

ohead_cost_tab.set_index(['Act_Id'],inplace=True)
ohead_tree_cost = costrun.cost_tree_sort.join(ohead_cost_tab)
ohead_tree_cost.reset_index(inplace=True)
ohead_tree_cost.set_index(['act_level','Act_Id'],inplace=True)
ohead_tree_cost.sort_values('sort_order',inplace=True)
ohead_tree_cost = ohead_tree_cost.drop('sort_order', 1)
ohead_tree_cost.reset_index(inplace=True)

ohead_tree_cost = ohead_tree_cost.round(1)

curdat_join = period_namer.period_me(callist, ohead_tree_cost,4)

hdftree.put('ohead_tree_cost',curdat_join)


"""this is for total data"""

vars()['tot_cost'] = pd.DataFrame()
for acts in costrun.all_activities:
    tot_data = eval('acts.tot_cost')
    tot_data = tot_data.reset_index()
    tot_data.rename(columns={'cost': 'tot_cost'}, inplace=True)
    tot_data.rename(columns={0: 'tot_cost'}, inplace=True)
    tot_data.fillna(value=0,inplace=True)
    try:
        tot_data = tot_data.set_index(['sched_period'])
    except:
        tot_data.rename(index=str, columns={'index': 'sched_period'}, inplace=True)
        tot_data = tot_data.set_index(['sched_period'])
    tot_data = tot_data.T
    tot_data['Act_Id'] = acts.act_tree
    try:
        nameact = acts.atype
    except:

        pass
    try:
        nameact = acts.otype
    except:
        pass
    try:

        nameact = acts.eqtype
    except:
        pass

    tot_data['Name'] = nameact
    tot_data.reset_index(inplace=True)
    tot_data.set_index(['Act_Id', 'Name'], inplace=True)
    tot_data.reset_index(inplace=True)
    tot_data.drop('index', axis=1, inplace=True)
    vars()['tot_cost'] = vars()['tot_cost'].append(tot_data)

temp_tree = vars()['tot_cost']
temp_tree.drop('Name', axis=1, inplace=True)
temp_tree = temp_tree.groupby(['Act_Id']).sum()
#temp_tree.set_index(['Act_Id'], inplace=True)
total_tree = temp_tree
temp_tree_cost = costrun.cost_tree_sort.join(total_tree)
temp_tree_cost.reset_index(inplace=True)
temp_tree_cost.set_index(['act_level','Act_Id'],inplace=True)
temp_tree_cost.sort_values('sort_order',inplace=True)
temp_tree_cost = temp_tree_cost.drop('sort_order', 1)
temp_tree_cost.reset_index(inplace=True)
cost_tree_sums = period_namer.sum_act(temp_tree_cost, 3)
cost_tree_sums = cost_tree_sums.round(2)
curdat_join = period_namer.period_me(callist, cost_tree_sums, 3)
hdftree.put('tot_cost_tree',curdat_join)


"""end of totals"""



for idx, rowdat in costrun.scheddiv.iterrows():
    unit_data = 'cost_per_' + idx
    vars()[unit_data] = pd.DataFrame()
    for acts in costrun.all_activities:
        calcdat = 'acts.' + unit_data
        curr_data = eval(calcdat)
        curr_data = curr_data.reset_index()
        curr_data.rename(columns={0: unit_data}, inplace=True)

        try:
            curr_data = curr_data.set_index(['sched_period'])

        except:
            curr_data.rename(index=str, columns={'index': 'sched_period'}, inplace=True)
            curr_data = curr_data.set_index(['sched_period'])

        curr_data = curr_data.T
        curr_data['Act_Id'] = acts.act_tree

        try:
            nameact = acts.atype
        except:

            pass
        try:
            nameact = acts.otype
        except:
            pass
        try:

            nameact = acts.eqtype
        except:
            pass

        curr_data['Name'] = nameact
        curr_data.reset_index(inplace=True)
        curr_data.set_index(['Act_Id', 'Name'], inplace=True)
        curr_data.reset_index(inplace=True)
        curr_data.drop('index', axis=1, inplace=True)
        vars()[unit_data] = vars()[unit_data].append(curr_data)


    temp_tree = vars()[unit_data]
    temp_tree.drop('Name', axis=1, inplace=True)
    temp_tree = temp_tree.groupby(['Act_Id']).sum()
    #temp_tree.set_index(['Act_Id'], inplace=True)
    total_tree = temp_tree
    temp_tree_cost = costrun.cost_tree_sort.join(total_tree)
    temp_tree_cost.reset_index(inplace=True)
    temp_tree_cost.set_index(['act_level','Act_Id'],inplace=True)
    temp_tree_cost.sort_values('sort_order',inplace=True)
    temp_tree_cost = temp_tree_cost.drop('sort_order', 1)
    temp_tree_cost.reset_index(inplace=True)
    cost_tree_sums = period_namer.sum_act(temp_tree_cost, 3)
    cost_tree_sums = cost_tree_sums.round(2)
    curdat_join = period_namer.period_me(callist, cost_tree_sums, 3)
    hdftree.put(unit_data,curdat_join)

    cost_graph = curdat_join
    mask = cost_graph['act_level'] == 3
    cost_graph_heads = cost_graph[mask].copy()
    cost_graph_heads.set_index(['Act_Id', 'Name'], inplace=True)
    cost_graph_heads.drop('act_level', axis=1, inplace=True)
    cost_graph_means = cost_graph_heads.mean(axis=1)
    cost_graph_means = cost_graph_means.reset_index()
    cost_graph_means.rename(columns={0: 'unit_costs'}, inplace=True)
    cost_graph_means['plot_type'] = 'unit_cost_pie'

    hdfplot.put(unit_data, cost_graph_means)



hdftree.flush()
hdftree.close()
hdfout.flush()
hdfout.close()
hdfplot.flush()
hdfplot.close()





#cost_graph_means.unit_costs.plot.pie(labels=cost_graph_means.Name, colors=['r', 'g', 'b', 'c'],autopct='%1.1f%%', shadow=True, startangle=90)






#TODO fix total requried fleet column width issue - total purchased fleet screws up column widths - make first four columns auto size


