
import pandas as pd
import A_Config_Utils
from pandas import read_hdf
from pandas import HDFStore

ConfigIni = A_Config_Utils.Read_Config()

class realise_me():

    def __init__(self,parent=None):
        self.parent = parent

    def sellme(self,sched_in):

        # read in recovery parameters
        file_in_xl = ConfigIni.read_config('bm_in.ini', 'blockmod').get('input_data')
        parameter_file = pd.ExcelFile(file_in_xl)
        constants = parameter_file.parse('constants')
        file_in_x2 = ConfigIni.read_config('bm_in.ini', 'blockmod').get('fin_data')
        fin_file = pd.ExcelFile(file_in_x2)
        fin_data = fin_file.parse('fin_data')

        prices = fin_file.parse('price_deck')

        prices.set_index(['price'], inplace=True)
        constants.set_index(['parameter'], inplace=True)
        fin_data.set_index(['fin_data'], inplace=True)

        sched_data = sched_in.copy()
        sched_data.set_index(['report_name'],inplace=True)
        sched_mult = sched_in[['report_name','report_mult']].set_index(['report_name'])

        sched_cols = list(sched_data)

        for coldat in sched_cols:
            if coldat != 1:
                sched_data.drop(coldat, axis=1, inplace=True)
            else:
                break

        price_deck_run = fin_data.loc['price_deck']['value']
        price_deck_base = price_deck_run
        price_deck_list = ['Anglo Corporate','Consensus','Status Quo']
        period_dat = self.calendar.shape[1]
        for price_deck_run in price_deck_list:

            cu_price_t = prices.loc['Copper_' + price_deck_run] * 2204.62
            ag_price_oz = prices.loc['Silver_' + price_deck_run]
            au_price_oz = prices.loc['Gold_' + price_deck_run]
            mo_price_ton = prices.loc['Molybdenum_' + price_deck_run] * 2204.62
            mo_price_lb = prices.loc['Molybdenum_' + price_deck_run]

            cu_price_t = cu_price_t[cu_price_t.index <= period_dat].copy()
            ag_price_oz = ag_price_oz[ag_price_oz.index <= period_dat].copy()
            au_price_oz = au_price_oz[au_price_oz.index <= period_dat].copy()
            mo_price_ton = mo_price_ton[mo_price_ton.index <= period_dat].copy()
            mo_price_lb = mo_price_lb[mo_price_lb.index <= period_dat].copy()

            cu_treat_charge = prices.loc['cu_tc']
            cu_refine_charge = prices.loc['cu_rc']

            conc_cu_grade = sched_data.loc['Cu_in_Conc'] * sched_mult.loc['Cu_in_Conc']['report_mult']

            cu_payable_total = sched_data.loc['CuMet_in_Conc'] * cu_price_t * sched_mult.loc['CuMet_in_Conc']['report_mult']
            cu_concentrate = sched_data.loc['Cu Concentrate'] * sched_mult.loc['Cu Concentrate']['report_mult']
            mask = conc_cu_grade < constants.loc['conc_grade_cut']['value']
            cu_payable_below = constants.loc['conc_payable_below_cut']['value'] * cu_payable_total[mask]
            cu_payable_above = constants.loc['conc_payable_above_cut']['value'] * cu_payable_total[~mask]
            cu_payable = pd.concat([cu_payable_below,cu_payable_above])
            cu_deduction = cu_payable_total - cu_payable
            cu_minimum = constants.loc['conc_minimum_deduction']['value'] * cu_concentrate * cu_price_t
            mask_cu = cu_minimum > cu_deduction
            cu_deduct_min = cu_minimum[mask_cu].copy()
            cu_deduct_formula = cu_deduction[~mask_cu].copy()
            cu_deduction = cu_deduct_formula.append(cu_deduct_min)

            cu_treat_charge = cu_treat_charge[cu_treat_charge.index <= period_dat].copy()
            cu_tc = cu_treat_charge * cu_concentrate
            cu_refine_charge = cu_refine_charge[cu_refine_charge.index <= period_dat].copy()
            cu_rc = (cu_refine_charge * cu_payable * 2204.62) / cu_price_t
            cu_freight = (constants.loc['cu_conc_freight']['value']) * (cu_concentrate/(1-constants.loc['cu_conc_humidity']['value']))

            conc_ag_grade = sched_data.loc['Ag_in_Conc']
            ag_payable_total = (sched_data.loc['Ag_in_Conc'] * ag_price_oz * cu_concentrate) / 31.1034768
            mask = conc_ag_grade > constants.loc['ag_deduction']['value']
            ag_payable_oz_above = ((conc_ag_grade[mask] - constants.loc['ag_deduction']['value']) * cu_concentrate[mask]) / 31.1034768
            ag_payable_oz_below = conc_ag_grade[~mask] * 0
            ag_payable_oz = pd.concat([ag_payable_oz_below, ag_payable_oz_above])
            ag_credit = ag_payable_oz * ag_price_oz
            ag_realisation_cost = ag_payable_total - ag_credit

            conc_au_grade = sched_data.loc['Au_in_Conc']
            au_payable_total = (sched_data.loc['Au_in_Conc'] * au_price_oz * cu_concentrate) / 31.1034768
            mask = conc_au_grade > constants.loc['au_deduction']['value']
            au_payable_oz_above = ((conc_au_grade[mask] - constants.loc['au_deduction']['value']) * cu_concentrate[mask]) / 31.1034768
            au_payable_oz_below = conc_au_grade[~mask] * 0
            au_payable_oz = pd.concat([au_payable_oz_below, au_payable_oz_above])
            au_credit = au_payable_oz * au_price_oz
            au_realisation_cost = au_payable_total - au_credit

            conc_as_grade = sched_data.loc['As_in_Conc']
            mask = (conc_as_grade < constants.loc['arsenic_penalty_floor']['value'])
            as_penalty_none = conc_as_grade[mask] * 0
            mask = (conc_as_grade >= constants.loc['arsenic_penalty_floor']['value']) & (conc_as_grade < constants.loc['arsenic_penalty_higher']['value'])
            as_penalty_floor = ((conc_as_grade[mask] / 1000) - (constants.loc['arsenic_penalty_floor']['value'] / 1000)) * constants.loc['arsenic_penalty_10bp']['value']
            mask = conc_as_grade > constants.loc['arsenic_penalty_higher']['value']
            as_penalty_higher = (((conc_as_grade[mask] / 1000) - (constants.loc['arsenic_penalty_higher']['value'] / 1000)) * constants.loc['arsenic_penalty_higher_10bp']['value']) + (constants.loc['arsenic_penalty_fixed_floor']['value'] * constants.loc['arsenic_penalty_10bp']['value'])
            as_penalty_all = pd.concat([as_penalty_floor, as_penalty_higher,as_penalty_none])
            as_penalty = as_penalty_all * cu_concentrate

            cu_cif_revenue = cu_payable_total + ag_credit + au_credit - cu_deduction - as_penalty - cu_tc - cu_rc
            cu_insurance = cu_cif_revenue * constants.loc['insurance']['value']
            cu_umpire = cu_concentrate * constants.loc['other_conc_charges']['value']
            cu_net_rev = cu_cif_revenue - cu_insurance - cu_freight - cu_umpire

            mo_concentrate = sched_data.loc['Mo Concentrate'] * sched_mult.loc['Mo Concentrate']['report_mult']
            mo_conc_grade = sched_data.loc['Mo_in_Conc']
            mo_payable_qual1 = constants.loc['mo_payable_qual1']['value']
            mo_payable_qual2 = constants.loc['mo_payable_qual2']['value']
            mo_payable_off1 = constants.loc['mo_payable_off1']['value']
            mo_payable_off2 = constants.loc['mo_payable_off2']['value']
            mo_sales_qual1 = constants.loc['mo_sales_qual1']['value']
            mo_sales_qual2 = constants.loc['mo_sales_qual2']['value']
            mo_sales_off_grade1 = constants.loc['mo_sales_off_grade1']['value']
            mo_sales_off_grade2 = constants.loc['mo_sales_off_grade2']['value']

            mo_gross_payable = mo_concentrate * mo_conc_grade / 100 * mo_price_ton

            mo_rev_q1 = mo_concentrate * mo_conc_grade / 100 * mo_price_ton * mo_sales_qual1 * mo_payable_qual1
            mo_rev_q2 = mo_concentrate * mo_conc_grade / 100 * mo_price_ton * mo_sales_qual2 * mo_payable_qual2
            mo_rev_off1 = mo_concentrate * mo_conc_grade / 100 * mo_price_ton * mo_sales_off_grade1 * mo_payable_off1
            mo_rev_off2 = mo_concentrate * mo_conc_grade / 100 * mo_price_ton * mo_sales_off_grade2 * mo_payable_off2
            mo_revenue = mo_rev_q1 + mo_rev_q2 + mo_rev_off1 + mo_rev_off2
            mo_payable_perc = mo_sales_qual1 * mo_payable_qual1 + mo_sales_qual2 * mo_payable_qual2 + mo_sales_off_grade1 * mo_payable_off1 + mo_sales_off_grade2 * mo_payable_off2
            mo_payable_tons = mo_concentrate * mo_conc_grade / 100 * mo_payable_perc
            mo_deduction = mo_gross_payable - mo_revenue

            mo_price_participation_trigger = constants.loc['mo_price_participation_trigger']['value']
            mo_price_participation_fixed = constants.loc['mo_price_participation_fixed']['value']
            mo_price_participation_percent = constants.loc['mo_price_participation_percent']['value']

            mask = (mo_price_lb > mo_price_participation_trigger)
            mo_price_participate_yes = (mo_price_participation_fixed + ((mo_price_lb[mask] - mo_price_participation_trigger) * mo_price_participation_percent)) * 2204.62
            mo_price_participate_no = mo_price_lb[~mask] * 0
            mo_price_participate = pd.concat([mo_price_participate_yes, mo_price_participate_no])
            mo_price_participation = mo_concentrate * mo_conc_grade / 100 * mo_price_participate

            mo_treatment_charge_qual1 = constants.loc['mo_treatment_charge_qual1']['value']
            mo_treatment_charge_qual2 = constants.loc['mo_treatment_charge_qual2']['value']
            mo_treatment_charge_off1 = constants.loc['mo_treatment_charge_off1']['value']
            mo_treatment_charge_off2 = constants.loc['mo_treatment_charge_off2']['value']

            treatment_charge_q1 = mo_treatment_charge_qual1 * 2204.62
            treatment_charge_q2 = mo_treatment_charge_qual2 * 2204.62
            treatment_charge_off1 = mo_treatment_charge_off1 * 2204.62
            treatment_charge_off2 = mo_treatment_charge_off2 * 2204.62

            mo_tc = mo_payable_tons * (
                        mo_sales_qual1 * treatment_charge_q1 + mo_sales_qual2 * treatment_charge_q2 + mo_sales_off_grade1 * treatment_charge_off1 + mo_sales_off_grade2 * treatment_charge_off2)

            mo_freight = mo_concentrate * constants.loc['mo_freight']['value']

            mo_cif = mo_revenue - mo_price_participation - mo_tc
            mo_insurance = mo_cif * constants.loc['insurance_mo']['value']
            mo_net_revenue = mo_cif - mo_freight - mo_insurance
            revenue_net = cu_net_rev + mo_net_revenue
            gross_revenue = cu_payable_total + ag_payable_total + au_payable_total + mo_gross_payable


            mo_realisation_cost = mo_gross_payable - mo_net_revenue
            cu_realisation_cost = cu_deduction + as_penalty + cu_rc + cu_tc + cu_freight + cu_insurance + cu_umpire + au_realisation_cost + ag_realisation_cost
            total_realisation_costs = mo_realisation_cost + cu_realisation_cost

            #cu_cif_revenue = cu_payable_total + ag_credit + au_credit - cu_deduction - as_penalty - cu_tc - cu_rc
            sched_fin = pd.concat([gross_revenue.rename('Gross Value in Concentrate IMC'),
                                   cu_payable_total.rename('Cu Value in Concentrate IMC'),
                                   ag_payable_total.rename('Ag Value in Concentrate IMC'),
                                   au_payable_total.rename('Au Value in Concentrate IMC'),
                                   mo_gross_payable.rename('Mo Value in Concentrate IMC'), cu_deduction.rename('Cu Deduction IMC'),
                                   as_penalty.rename('Arsenic Penalty IMC'), cu_tc.rename('Cu Treatment Charge IMC'),
                                   cu_rc.rename('Cu Refining Charge IMC'),cu_realisation_cost.rename('Cu Realisation Costs IMC'),mo_realisation_cost.rename('Mo Realisation Costs IMC'),total_realisation_costs.rename('Total Realisation Costs IMC')], axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin = sched_fin / rev_mult
            sched_fin.reset_index(inplace=True)
            sched_fin = sched_fin.rename(columns={'index': 'report_name'})
            sched_fin['report_mult'] = rev_mult
            sched_fin['graph'] = 0
            sched_fin.set_index(['report_name','report_mult','graph'],inplace=True)
            sched_fin.reset_index(inplace=True)
            sched_fin['sorter'] = 0
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Gross Value in Concentrate IMC','sorter'] = 205
            sched_fin.loc['Cu Value in Concentrate IMC', 'sorter'] = 201
            sched_fin.loc['Ag Value in Concentrate IMC', 'sorter'] = 202
            sched_fin.loc['Au Value in Concentrate IMC', 'sorter'] = 203
            sched_fin.loc['Mo Value in Concentrate IMC', 'sorter'] = 204
            sched_fin.loc['Cu Deduction IMC', 'sorter'] = 206
            sched_fin.loc['Arsenic Penalty IMC', 'sorter'] = 207
            sched_fin.loc['Cu Treatment Charge IMC', 'sorter'] = 208
            sched_fin.loc['Cu Refining Charge IMC', 'sorter'] = 209
            sched_fin.loc['Cu Realisation Costs IMC', 'sorter'] = 213
            sched_fin.loc['Mo Realisation Costs IMC', 'sorter'] = 230
            sched_fin.loc['Total Realisation Costs IMC', 'sorter'] = 231


            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass

            sched_fin_temp = pd.concat([cu_cif_revenue.rename('Cu CIF Revenue IMC'),cu_insurance.rename('Cu Insurance IMC'),cu_freight.rename('Cu Freight IMC'),cu_umpire.rename('Cu Umpire IMC'),ag_realisation_cost.rename('Ag Deduction IMC'),au_realisation_cost.rename('Au Deduction IMC')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_temp = sched_fin_temp / rev_mult
            sched_fin_temp.reset_index(inplace=True)
            sched_fin_temp = sched_fin_temp.rename(columns={'index': 'report_name'})
            sched_fin_temp['report_mult'] = rev_mult
            sched_fin_temp['graph'] = 0
            sched_fin = sched_fin.append(sched_fin_temp)
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Cu CIF Revenue IMC','sorter'] = 999
            sched_fin.loc['Cu Insurance IMC', 'sorter'] = 210
            sched_fin.loc['Cu Freight IMC', 'sorter'] = 211
            sched_fin.loc['Cu Umpire IMC', 'sorter'] = 212
            sched_fin.loc['Ag Deduction IMC', 'sorter'] = 213
            sched_fin.loc['Au Deduction IMC', 'sorter'] = 214

            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass


            """cu_realisation = cu_deduction + as_penalty + cu_rc + cu_tc + cu_freight + cu_insurance + cu_umpire
            sched_fin_temp = pd.concat([cu_realisation.rename('Cu Conc Realisation Cost Total IMC')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_temp = sched_fin_temp / rev_mult
            sched_fin_temp.reset_index(inplace=True)
            sched_fin_temp = sched_fin_temp.rename(columns={'index': 'report_name'})
            sched_fin_temp['report_mult'] = rev_mult
            sched_fin_temp['graph'] = 0
            sched_fin_temp['sorter'] = 213
            sched_fin = sched_fin.append(sched_fin_temp)
            """

            sched_fin_temp = pd.concat([au_credit.rename('Au Credit IMC'),ag_credit.rename('Ag Credit IMC')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_temp = sched_fin_temp / rev_mult
            sched_fin_temp.reset_index(inplace=True)
            sched_fin_temp = sched_fin_temp.rename(columns={'index': 'report_name'})
            sched_fin_temp['report_mult'] = rev_mult
            sched_fin_temp['graph'] = 0
            sched_fin = sched_fin.append(sched_fin_temp)

            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Au Credit IMC','sorter'] = 232
            sched_fin.loc['Ag Credit IMC', 'sorter'] = 233
            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass

            sched_fin_temp = pd.concat(
                [mo_revenue.rename('Mo Net Value in Conc IMC'), mo_rev_q1.rename('Mo Net Value in Conc Qual 1 IMC'),
                 mo_rev_q2.rename('Mo Net Value in Conc Qual 2 IMC'), mo_rev_off1.rename('Mo Net Value in Conc Off 1 IMC'),
                 mo_rev_off2.rename('Mo Net Value in Conc Off 2 IMC')], axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_temp = sched_fin_temp / rev_mult
            sched_fin_temp.reset_index(inplace=True)
            sched_fin_temp = sched_fin_temp.rename(columns={'index': 'report_name'})
            sched_fin_temp['report_mult'] = rev_mult
            sched_fin_temp['graph'] = 0
            sched_fin = sched_fin.append(sched_fin_temp)
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Mo Net Value in Conc IMC','sorter'] = 220
            sched_fin.loc['Mo Net Value in Conc Qual 1 IMC', 'sorter'] = 221
            sched_fin.loc['Mo Net Value in Conc Qual 2 IMC', 'sorter'] = 222
            sched_fin.loc['Mo Net Value in Conc Off 1 IMC', 'sorter'] = 223
            sched_fin.loc['Mo Net Value in Conc Off 2 IMC', 'sorter'] = 224
            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass

            sched_fin_temp = pd.concat([mo_cif.rename('Mo CIF Revenue IMC'), mo_tc.rename('Mo Treatment Charge IMC'),
                                        mo_price_participation.rename('Mo Price Participation IMC'),
                                        mo_insurance.rename('Mo Insurance IMC'), mo_freight.rename('Mo Freight IMC')],
                                       axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_temp = sched_fin_temp / rev_mult
            sched_fin_temp.reset_index(inplace=True)
            sched_fin_temp = sched_fin_temp.rename(columns={'index': 'report_name'})
            sched_fin_temp['report_mult'] = rev_mult
            sched_fin_temp['graph'] = 0
            sched_fin = sched_fin.append(sched_fin_temp)
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Mo CIF Revenue IMC','sorter'] = 999
            sched_fin.loc['Mo Treatment Charge IMC', 'sorter'] = 225
            sched_fin.loc['Mo Price Participation IMC', 'sorter'] = 226
            sched_fin.loc['Mo Insurance IMC', 'sorter'] = 227
            sched_fin.loc['Mo Freight IMC', 'sorter'] = 228
            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass

            sched_fin_data = pd.concat([cu_net_rev.rename('Cu Net Rev IMC'),mo_net_revenue.rename('Mo Net Revenue IMC')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_data = sched_fin_data / rev_mult
            sched_fin_data.reset_index(inplace=True)
            sched_fin_data = sched_fin_data.rename(columns={'index': 'report_name'})
            sched_fin_data['report_mult'] = rev_mult
            sched_fin_data['graph'] = 'f1'
            sched_fin = sched_fin.append(sched_fin_data)
            sched_fin_all = sched_in.append(sched_fin_data)
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Cu Net Rev IMC','sorter'] = 234
            sched_fin.loc['Mo Net Revenue IMC', 'sorter'] = 235
            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass

            sched_fin_data = pd.concat([revenue_net.rename('Net Rev IMC')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_data = sched_fin_data / rev_mult
            sched_fin_data.reset_index(inplace=True)
            sched_fin_data = sched_fin_data.rename(columns={'index': 'report_name'})
            sched_fin_data['report_mult'] = rev_mult
            sched_fin_data['graph'] = 0
            sched_fin_data['sorter'] = 236
            sched_fin = sched_fin.append(sched_fin_data)
            sched_fin_all = sched_fin_all.append(sched_fin_data)

            if price_deck_run == price_deck_base:
                sched_fin_base = sched_fin
                sched_fin_all_base = sched_fin_all
            elif price_deck_run == 'Consensus':
                sched_fin_cs = sched_fin
                sched_fin_all_cs = sched_fin_all
            else:
                sched_fin_sq = sched_fin
                sched_fin_all_sq = sched_fin_all


        return (sched_fin_all_base,sched_fin_base,sched_fin_all_cs,sched_fin_cs,sched_fin_all_sq,sched_fin_sq)


if __name__ == '__main__':
    seller = sellme()
