

import openpyxl
from pandas import HDFStore
from pandas import read_hdf
import pandas as pd
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font
from openpyxl.styles import colors
from openpyxl.styles import Font, Color
from openpyxl.drawing.image import Image

from openpyxl.utils.dataframe import dataframe_to_rows
import A_Config_Utils
ConfigIni = A_Config_Utils.Read_Config()


class XLWrite:
  def xlwrite(self):

    wb = openpyxl.load_workbook('aa_summary_sheet2.xlsx')
    scenario_name = ConfigIni.read_config('cost_in.ini', 'costmod').get('scenario_name')
    scenario_directory = ConfigIni.read_config('cost_in.ini', 'costmod').get('scenario_directory')
    scenario_directory = scenario_directory + scenario_name + '\\'
    mcd_output_file = scenario_directory + 'mcd_' + scenario_name + '.xlsx'
    aa_output_file = scenario_directory + 'aa_summary_' + scenario_name + '.xlsx'


    hdftree = HDFStore('financial_data.h5')


    indat = ['fintree_data_all_npc','fintree_data_npc']

    for infile in indat:
        df = read_hdf(hdftree, infile)
        treedat = df['Tree_Dat']
        tree_dat1 = treedat.str.split(":",n=1,expand=True)
        levs = treedat.str.count(':')
        tree_dat1[0] = levs
        tree_dat1.iloc[0,1] ='Total'
        df['Tree_Dat'] = tree_dat1[1]
        df['level'] = levs


        ws = wb[infile]
        ws.column_dimensions['B'].width = float(65)
        ws.column_dimensions['C'].width = float(24)
        for row in ws['A1:GX500']:
            for cell in row:
                cell.value = None

        rows = dataframe_to_rows(df)




        for r_idx, row in enumerate(rows, 1):
            name = row[1]
            ynum = len(row) - 1
            levs = row[ynum]
            alignment = Alignment(indent=0)
            #if r_idx > 1: alignment = Alignment(indent = levs)

            currentCell = ws.cell(row=r_idx + 1, column=2)
            if levs == 0:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="C6E0B4")
                currentCell.font = Font(size=14, bold=True)
            if levs == 1:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="FFE699")
                currentCell.font = Font(size=11, bold=True)
                currentCell.alignment = Alignment(indent=2)

            if levs == 2:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="F8CBAD")
                currentCell.font = Font(size=11, bold=True)
                currentCell.alignment = Alignment(indent=4)
            if levs == 3:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="BDD7EE")
                currentCell.font = Font(size=11, bold=True)
                currentCell.alignment = Alignment(indent=6)
            if levs == 4:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="EDf7B7")
                currentCell.font = Font(size=11, bold=True)
                currentCell.alignment = Alignment(indent=8)
            if levs == 5:
                currentCell.fill = PatternFill(fill_type="solid", fgColor="B7F6F7")
                currentCell.font = Font(size=11, bold=True)
                currentCell.alignment = Alignment(indent=10)

            ws.cell(row=r_idx + 1, column=2, value=name)

            for c_idx, value in enumerate(row, 1): #for c_idx, value in enumerate(row, 1):
                if c_idx > 2:
                    currentCell = ws.cell(row=r_idx + 1, column=c_idx )
                    currentCell.number_format = '#,##0'
                    currentCell.alignment = Alignment(horizontal='center')
                    if c_idx == 3: currentCell.alignment = Alignment(horizontal='left')
                    if levs == 0:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="C6E0B4")
                        currentCell.font = Font(size=11, bold=True)
                    if levs == 1:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="FFE699")
                        currentCell.font = Font(size=11, bold=True)
                    if levs == 2:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="F8CBAD")
                        currentCell.font = Font(size=11, bold=False)

                    if levs == 3:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="BDD7EE")
                        currentCell.font = Font(size=11, bold=False)

                    if levs == 4:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="EDf7B7")
                        currentCell.font = Font(size=11, bold=False)

                    if levs == 5:
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="B7F6F7")
                        currentCell.font = Font(size=11, bold=False)



                    ws.cell(row=r_idx + 1, column=c_idx , value=value)

        """
        #ws['E2'] = 'Edited'   # example
        #ws.cell(row=9, column=15).value = 'Cat_740'   # example
        #####
        """
    wb.save(aa_output_file)

    wb = openpyxl.load_workbook('mcd.xlsx')


    try:
        sumsheet = wb['Summary_Graphs']
        img = Image('waterfall.png',size = (1000,800))
        sumsheet.add_image(img,'B2')
    except:
        pass

    try:
        sumsheet = wb['Summary_Graphs']
        img = Image('npvsens.png',size = (600,600))
        sumsheet.add_image(img,'S2')
    except:
        pass

    try:
        sumsheet = wb['Summary_Graphs']
        img = Image('sched_dat.png',size = (1600,800))
        sumsheet.add_image(img,'B30')
    except:
        pass

    try:
        sumsheet = wb['Summary_Graphs']
        img = Image('fin_dat.png',size = (1600,800))
        sumsheet.add_image(img,'B70')
    except:
        pass




    hdftree = HDFStore('financial_data.h5')
    df = read_hdf(hdftree, 'mcd_data')
    #####
    # do magic with openpyxl here and save

    ws = wb['mcd']

    ws.column_dimensions['B'].width = float(50)
    ws.column_dimensions['C'].width = float(20)
    ws.column_dimensions['D'].width = float(20)

    for row in ws['A1:GX500']:
        for cell in row:
            cell.value = None

    rows = dataframe_to_rows(df)


    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1): #for c_idx, value in enumerate(row, 1):
            currentCell = ws.cell(row=r_idx, column=c_idx)
            currentCell.number_format = '#,##0.00'
            ws.cell(row=r_idx, column=c_idx , value=value)

    """
    #ws['E2'] = 'Edited'   # example
    #ws.cell(row=9, column=15).value = 'Cat_740'   # example
    #####
    """
    wb.save(mcd_output_file)

if __name__ == '__main__':

    xlme = XLWrite()
    fred = xlme.xlwrite()




