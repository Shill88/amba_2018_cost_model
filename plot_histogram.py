


import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from pandas import HDFStore
from pandas import read_hdf
from scipy.stats import norm


hdfin = HDFStore('resources.h5')
data_inore = read_hdf(hdfin,'resource_raw')

# example data

mask  = (data_inore['al2o3'] > 44) & (data_inore['al2o3'] <50)
masksi = (data_inore['sio2'] > 0) & (data_inore['sio2'] <8)
maskal = (data_inore['al_pct'] >36) &  (data_inore['al_pct'] < 45)
x = data_inore.loc[masksi,'sio2']
x2 = data_inore.loc[mask,'al2o3']
x3 = data_inore.loc[maskal,'al_pct']
#x = x2

num_bins = 50

fig = plt.figure(figsize =( 12,10))

#fig.subplots_adjust(hspace = 0.3)

grid_spec = plt.GridSpec(3, 1, height_ratios =[ 1, 1, 1], wspace = 0.1, hspace = 0.4)

ax = fig.add_subplot( grid_spec[0])
ax2 = fig.add_subplot( grid_spec[1])
ax3 = fig.add_subplot( grid_spec[2])
#ax4 = fig.add_subplot( grid_spec[3])






#ax = fig.add_subplot(111)

#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)


#fig2 = plt.figure()
#ax2 = fig.add_subplot(122)


# the histogram of the data
n, bins, patches = ax.hist(x, num_bins, normed=1)

(mu, sigma) = norm.fit(x)
# add a 'best fit' line
y = mlab.normpdf(bins, mu, sigma)
ax.plot(bins, y, '--')
ax.set_xlabel('SiO2 Grade')
ax.set_ylabel('Block Frequency')
#ax.set_title((r'$\mathrm{Histogram\ of\ SiO2:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu, sigma)))
ax.set_title(('$\mathrm{Histogram\ of\ SiO2:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu, sigma)))

# the histogram of the data
n2, bins2, patches2 = ax2.hist(x2, num_bins, normed=1)

(mu2, sigma2) = norm.fit(x2)
# add a 'best fit' line
y2 = mlab.normpdf(bins2, mu2, sigma2)
ax2.plot(bins2, y2, '--')
ax2.set_xlabel('Al2O3 Grade')
ax2.set_ylabel('Block Frequency')
#ax2.set_title((r'$\mathrm{Histogram\ of\ Al2O3:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu2, sigma2)))
ax2.set_title(('$\mathrm{Histogram\ of\ Al2O3:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu2, sigma2)))


n3, bins3, patches3 = ax3.hist(x3, num_bins, normed=1)

(mu3, sigma3) = norm.fit(x3)
# add a 'best fit' line
y3 = mlab.normpdf(bins3, mu3, sigma3)
ax3.plot(bins3, y3, '--')
ax3.set_xlabel('PCT Al (TAA)')
ax3.set_ylabel('Block Frequency')
#ax2.set_title((r'$\mathrm{Histogram\ of\ Al2O3:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu2, sigma2)))
ax3.set_title(('$\mathrm{Histogram\ of\ PCT Al:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu3, sigma3)))


#n4, bins4, patches4 = ax4.hist(x3, num_bins, normed=1, histtype='step', cumulative=-1, label='Empirical')

#(mu4, sigma4) = norm.fit(x3)
# add a 'best fit' line
#y4 = mlab.normpdf(bins4, mu4, sigma4).cumsum()
#y4 /= y4[-1]

#ax4.plot(bins4, y4, '--')
#ax4.set_xlabel('PCT Al (TAA)')
#ax4.set_ylabel('Block Frequency')
#ax2.set_title((r'$\mathrm{Histogram\ of\ Al2O4:}\ \mu=%.4f,\ \sigma=%.4f$' %(mu2, sigma2)))
#ax4.set_title(('$\mathrm{Histogram\ of\ PCT Al:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu3, sigma3)))





# Tweak spacing to prevent clipping of ylabel



# Tweak spacing to prevent clipping of ylabel


plt.show()
