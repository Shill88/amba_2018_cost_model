
import pandas as pd
import A_Config_Utils
from pandas import read_hdf
from pandas import HDFStore

ConfigIni = A_Config_Utils.Read_Config()

class realise_me():

    def __init__(self,parent=None):
        self.parent = parent

    def sellme(self,sched_in):

        # read in recovery parameters
        file_in_xl = ConfigIni.read_config('bm_in.ini', 'blockmod').get('input_data')
        parameter_file = pd.ExcelFile(file_in_xl)
        constants = parameter_file.parse('constants')
        file_in_x2 = ConfigIni.read_config('bm_in.ini', 'blockmod').get('fin_data')
        fin_file = pd.ExcelFile(file_in_x2)
        fin_data = fin_file.parse('fin_data')

        prices = fin_file.parse('price_deck')

        other_costs = fin_file.parse('dep_amortise')

        prices.set_index(['price'], inplace=True)
        constants.set_index(['parameter'], inplace=True)
        fin_data.set_index(['fin_data'], inplace=True)
        other_costs.set_index(['data'], inplace=True)

        sched_data = sched_in.copy()
        sched_data.set_index(['report_name'],inplace=True)
        sched_mult = sched_in[['report_name','report_mult']].set_index(['report_name'])
        royalty_perc = fin_data.loc['royalty']['value']

        sched_cols = list(sched_data)

        for coldat in sched_cols:
            if coldat != 1:
                sched_data.drop(coldat, axis=1, inplace=True)
            else:
                break

        price_deck_run = fin_data.loc['price_deck']['value']
        price_deck_base = price_deck_run
        price_deck_list = ['Base','Low','Hi']
        period_dat = self.calendar.shape[1]
        for price_deck_run in price_deck_list:

           # price_dt = prices.loc[price_deck_run]
            Ni = prices.loc[price_deck_run + '_Ni']*2204.62
            Co = prices.loc[price_deck_run + '_Co']*2204.62
            Byproduct = prices.loc[price_deck_run + '_By_Product']*2204.62

            Marketing = prices.loc['Market Rate']



            """Revenue"""
            Ni_rev = (sched_data.loc['Ni metal'] * Ni * sched_mult.loc['Ni metal']['report_mult'])
            Co_rev = (sched_data.loc['Co metal'] * Co * sched_mult.loc['Co metal']['report_mult'])
            metal_rev =  Ni_rev + Co_rev
            by_product_credit = sched_data.loc['Ni metal'] * Byproduct * sched_mult.loc['Ni metal']['report_mult']
            gross_rev = metal_rev + by_product_credit

            """Marketing"""
            marketing_cost = Marketing * metal_rev
            """By_Product_Costs"""
            byproduct_cost = other_costs.loc['Total_By_Product_Costs']

            """Royalty"""
            royalty = (metal_rev) * royalty_perc
            """Finance"""
            finance_cost = other_costs.loc['Net_financing_cost']


            realisation_cost = marketing_cost - byproduct_cost


            net_rev = gross_rev - realisation_cost - finance_cost

            sched_fin = pd.concat([gross_rev.rename('Gross Value'),
                                   Ni_rev.rename('Ni Gross Value'),
                                   Co_rev.rename('Co Gross Value'),
                                   net_rev.rename('Net Rev IMC'),
                                   by_product_credit.rename('By Product Gross Value'),
                                   byproduct_cost.rename('Total_By_Product_Costs'),
                                   marketing_cost.rename('Total Marketing Costs'),
                                   finance_cost.rename('Net_Financing_Cost'),
                                   royalty.rename('Royalty')], axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin = sched_fin / rev_mult
            sched_fin.reset_index(inplace=True)
            sched_fin = sched_fin.rename(columns={'index': 'report_name'})
            sched_fin['report_mult'] = rev_mult
            sched_fin['graph'] = 0
            #sched_fin = sched_fin.append(sched_fin)
            sched_fin_all = sched_in.append(sched_fin)
            sched_fin.set_index(['report_name'],inplace=True)
            sched_fin.loc['Gross Value', 'sorter'] = 200
            sched_fin.loc['Ni Gross Value', 'sorter'] = 201
            sched_fin.loc['Co Gross Value', 'sorter'] = 202
            sched_fin.loc['By Product Gross Value', 'sorter'] = 203
            sched_fin.loc['Total_By_Product_Costs', 'sorter'] = 204
            sched_fin.loc['Total Marketing Costs', 'sorter'] = 205
            sched_fin.loc['Net Rev IMC', 'sorter'] = 206
            sched_fin.loc['Net_Financing_Cost', 'sorter'] = 207
            sched_fin.loc['Royalty', 'sorter'] = 208
            sched_fin.reset_index(inplace=True)
            try:
                sched_fin.drop(['index'], axis=1, inplace=True)
            except:
                pass


            sched_fin_data = pd.concat([Ni_rev.rename('Ni Rev'),Co_rev.rename('Co Rev'),by_product_credit.rename('By Product Rev')],axis=1).T
            rev_mult = fin_data.loc['revenue_multiplier']['value']
            sched_fin_data = sched_fin_data / rev_mult
            sched_fin_data.reset_index(inplace=True)
            sched_fin_data = sched_fin_data.rename(columns={'index': 'report_name'})
            sched_fin_data['report_mult'] = rev_mult
            sched_fin_data['graph'] = 'f1'
            sched_fin_data.set_index(['report_name'], inplace=True)
            sched_fin_data.loc['Ni Rev','sorter'] = 209
            sched_fin_data.loc['Co Rev','sorter'] = 210
            sched_fin_data.loc['By Product Rev','sorter'] = 211
            sched_fin_data.reset_index(inplace=True)
            sched_fin = sched_fin.append(sched_fin_data)


            sched_fin_all = sched_fin_all.append(sched_fin_data)

            if price_deck_run == price_deck_base:
                sched_fin_base = sched_fin
                sched_fin_all_base = sched_fin_all
            elif price_deck_run == 'Hi':
                sched_fin_Hi = sched_fin
                sched_fin_all_Hi = sched_fin_all
            else:
                sched_fin_low = sched_fin
                sched_fin_all_low = sched_fin_all


        return (sched_fin_all_base,sched_fin_base,sched_fin_all_Hi,sched_fin_Hi,sched_fin_all_low,sched_fin_low)


if __name__ == '__main__':
    seller = sellme()
