

import openpyxl
from pandas import HDFStore
from pandas import read_hdf
import pandas as pd
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font
from openpyxl.styles import colors
from openpyxl.styles import Font, Color
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.drawing.image import Image
import A_Config_Utils

ConfigIni = A_Config_Utils.Read_Config()

class XLWriteaa:
  def xlwriteaa(self):



        scenario_name = ConfigIni.read_config('cost_in.ini', 'costmod').get('scenario_name')
        scenario_directory = ConfigIni.read_config('cost_in.ini', 'costmod').get('scenario_directory')
        scenario_directory = scenario_directory + scenario_name + '\\'
        aa_output_file = scenario_directory + 'aa_summary_' + scenario_name + '.xlsx'

        wb = openpyxl.load_workbook(aa_output_file)

        try:
            sumsheet = wb['Summary_Graphs']
            img = Image('waterfall.png', size=(1000, 800))
            sumsheet.add_image(img, 'B2')
        except:
            pass

        try:
            sumsheet = wb['Summary_Graphs']
            img = Image('npvsens.png', size=(600, 600))
            sumsheet.add_image(img, 'S2')
        except:
            pass

        try:
            sumsheet = wb['Summary_Graphs']
            img = Image('sched_dat.png', size=(1600, 800))
            sumsheet.add_image(img, 'B30')
        except:
            pass

        try:
            sumsheet = wb['Summary_Graphs']
            img = Image('fin_dat.png', size=(1600, 800))
            sumsheet.add_image(img, 'B70')
        except:
            pass

        hdftree = HDFStore('financial_data.h5')
        df = read_hdf(hdftree, 'sched_data_totals')
        treedat = df['looker_upper']
        tree_dat1 = treedat.str.split("_",n=1,expand=True)
        levs = tree_dat1[tree_dat1.columns[0]].astype(int)
        tree_dat1[0] = levs

        ws = wb['Summary_Schedule']
        ws.column_dimensions['B'].width = float(2)
        ws.column_dimensions['C'].width = float(24)

        for row in ws['A1:GX500']:
          for cell in row:
            cell.value = None

        rows = dataframe_to_rows(df)
        lastlevdat = -1
        for r_idx, row in enumerate(rows, 1):
            if r_idx == 1:
                levdat = 0
            else:
                levdat = int(row[1].split("_")[0])

            heading = 0
            if levdat == lastlevdat:
                if int(row[3]) / 1 > 1:
                    heading = 0
                else:
                    heading = 1
            lastlevdat = levdat

            if heading == 0:
                cfill = PatternFill(fill_type="solid", fgColor="C6E0B4")
                cfont = Font(size=11, bold=True)
                cnum_format = '#,##0'
                calignment = Alignment(indent=0)
            else:
                cfill = PatternFill(fill_type="solid", fgColor="FFE699")
                cfont = Font(size=11, bold=False)
                cnum_format = '#,##0.00'
                calignment = Alignment(indent=4)

            colcount = 1
            for c_idx, value in enumerate(row, 1): #for c_idx, value in enumerate(row, 1):

                if colcount > 2:
                    currentCell = ws.cell(row=r_idx + 1, column=colcount)
                    currentCell.font = cfont
                    currentCell.fill = cfill
                    currentCell.alignment = calignment
                    if c_idx > 3:
                        currentCell.number_format = '#,##0'
                        currentCell.alignment = Alignment(horizontal='left')
                    if c_idx > 4:
                        currentCell.number_format = cnum_format
                        currentCell.alignment = Alignment(horizontal='center')

                    if r_idx == 1:
                        currentCell.alignment = Alignment(horizontal='center')
                        currentCell.fill = PatternFill(fill_type="solid", fgColor="EDf7B7")


                    ws.cell(row=r_idx + 1, column=colcount , value=value)

                colcount += 1


        df = read_hdf(hdftree, 'fin_data_totals')


        #####
        # do magic with openpyxl here and save
        ws = wb['Summary_Financial']
        ws.column_dimensions['B'].width = float(38)
        ws.column_dimensions['C'].width = float(20)

        for row in ws['A1:GX500']:
          for cell in row:
            cell.value = None

        rows = dataframe_to_rows(df)

        for r_idx, row in enumerate(rows, 1):

            if r_idx == 1:
                cfill = PatternFill(fill_type="solid", fgColor="C6E0B4")
                cfont = Font(size=11, bold=True)
                cnum_format = '#,##0'
                calignment = Alignment(horizontal='center')
            else:
                cfill = PatternFill(fill_type="solid", fgColor="FFE699")
                cfont = Font(size=11, bold=False)
                cnum_format = '#,##0.0'
                calignment = Alignment(horizontal='center')

            for c_idx, value in enumerate(row, 1): #for c_idx, value in enumerate(row, 1):
                if c_idx > 1:
                    currentCell = ws.cell(row=r_idx + 1, column=c_idx)
                    currentCell.font = cfont
                    currentCell.fill = cfill
                    currentCell.alignment = calignment
                    currentCell.number_format = cnum_format
                    if c_idx == 2 :
                        currentCell.font = Font(size=11, bold=True)
                        currentCell.alignment = Alignment(horizontal='left')

                    ws.cell(row=r_idx + 1, column=c_idx, value=value)





        wb.save(aa_output_file)

if __name__ == '__main__':

    xlme = XLWriteaa()
    fred = xlme.xlwriteaa()



