
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
from pandas import HDFStore
from pandas import read_hdf

plt.style.use('ggplot')

class Plot_Window(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("IMC Plotting Widget")

        self.ax = self.figure.add_subplot(211)
        self.ax1 = self.figure.add_subplot(212)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")


        #label=QLabel(self)
        #label.setText("Window Title")

        #self.setStyleSheet("QWidget {background-color:   #23297A}")

        self.resize(1280,860)

        # set the layout
        layout = QVBoxLayout()
        #layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        #layout.addWidget(self.button)
        self.setLayout(layout)
        self.plotfuel()

    def plotfuel(self):
        ''' plot some random stuff '''

        self.figure.tight_layout(pad=3)
        self.setWindowTitle('Consumables')
        self.figure.figsize=(18, 14)

        hdfplot = HDFStore('plot_data.h5')

        graph_data = read_hdf(hdfplot, 'fuel_data')
        temp = list(graph_data)
        for x in range(len(temp)):
            firsty = temp[x][0]
            if firsty == '_':
                temp[x] = temp[x][1:]

        graph_data.columns = temp

        gsums = graph_data.sum()
        gtop5 = gsums.nlargest(n=5)
        gtop5 = gtop5.to_frame()
        graph_data = graph_data.T
        graph_data = gtop5.join(graph_data)
        graph_data = graph_data.drop(0, 1).T
        self.ax.set_ylabel('Fuel Litres')
        self.figure; graph_data.plot(ax=self.ax, kind='area', title='Fuel - Top 5 Users').legend(loc=9, bbox_to_anchor=(0.5, -0.1), ncol=5, fontsize=7, borderaxespad=0)

        graph_data2 = read_hdf(hdfplot, 'kw_data')
        temp = list(graph_data2)
        for x in range(len(temp)):
            firsty = temp[x][0]
            if firsty == '_':
                temp[x] = temp[x][1:]

        graph_data2.columns = temp

        gsums2 = graph_data2.sum()
        gtop52 = gsums2.nlargest(n=5)
        gtop52 = gtop52.to_frame()
        masktemp = gtop52[0] > 0
        graph_data2 = graph_data2.T
        graph_data2 = gtop52[masktemp].join(graph_data2)
        graph_data2 = graph_data2.drop(0, 1).T
        self.ax1.set_ylabel('KwHrs')
        self.figure; graph_data2.plot(ax=self.ax1, kind='area', title='KwHrs - Top 5 Users').legend(loc=9, bbox_to_anchor=(0.5, -0.1), ncol=5, fontsize=7, borderaxespad=0)


class Plot_Window_Sched(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window_Sched, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("IMC Plotting Widget")

        self.ax = self.figure.add_subplot(221)
        self.ax1 = self.figure.add_subplot(222)
        self.ax2 = self.figure.add_subplot(223)
        self.ax3 = self.figure.add_subplot(224)


        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")

        # label=QLabel(self)
        # label.setText("Window Title")

        # self.setStyleSheet("QWidget {background-color:   #23297A}")

        #self.resize(1280, 860)

        # set the layout
        layout = QVBoxLayout()
        # layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        # layout.addWidget(self.button)
        self.setLayout(layout)

        self.plotsched()

    def plotsched(self):
        ''' plot Schedule Data '''
        import pandas as pd
        self.figure.tight_layout(pad=1)
        self.setWindowTitle('Schedule Data')
        self.figure.figsize = (18, 14)

        self.hdffin = HDFStore('financial_data.h5')
        self.truckdat = read_hdf(self.hdffin,'sched_data')
        self.truckdat.set_index('report_name',inplace=True)
        self.truckdat.drop(['report_mult', 'units','total'], axis=1, inplace=True)
        ticker = pd.DataFrame(list(self.truckdat))
        ticker = ticker.T
        self.truckdat = self.truckdat.loc['Total Truck Hours'] / 1000
        maxtruck = self.truckdat.max()
        self.truckdat = self.truckdat.to_frame()
        self.truckdat.reset_index(inplace=True)
        self.truckdat.drop(['index'], axis=1, inplace=True)


        self.hdfinputs = HDFStore('input_data.h5')

        axdat = read_hdf(self.hdfinputs, 'sched_graph_stack1')
        axdat.reset_index(inplace=True)
        axdat.drop(['index'], axis=1, inplace=True)

        self.figure; axdat.plot(ax=self.ax, kind='bar',figsize=(18, 9),
                                                                 stacked=True, fontsize=6, title='Total Mined Material')
        self.ax.set(xlabel="Period",ylabel="Million Tonnes")
        self.ax.set_xticklabels(ticker.iloc[0])

        axdat = read_hdf(self.hdfinputs, 'sched_graph_stack2')
        axdat.reset_index(inplace=True)
        axdat.drop(['index'], axis=1, inplace=True)


        self.figure; axdat.plot(ax=self.ax1, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Ore Mined',xticks = ticker.iloc[0])
        self.ax1.set(xlabel="Period", ylabel="Million Tonnes")
        self.ax1.set_xticklabels(ticker.iloc[0])

        axdat = read_hdf(self.hdfinputs, 'sched_graph_stack3')
        axdat.reset_index(inplace=True)
        axdat.drop(['index'], axis=1, inplace=True)

        self.figure; axdat.plot(ax=self.ax2, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Ore Processed',xticks = ticker.iloc[0])
        self.ax2.set(xlabel="Period", ylabel="Million Tonnes")
        self.ax2.set_xticklabels(ticker.iloc[0])

        axdat = read_hdf(self.hdfinputs, 'sched_graph_stack4')
        axdat.reset_index(inplace=True)
        axdat.drop(['index'], axis=1, inplace=True)

        self.figure; axdat.plot(ax=self.ax3, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Total Material Moved')
        self.ax3.set(xlabel = 'Periods',ylabel="Million Tonnes")
        self.ax3.set_xticklabels(ticker.iloc[0])
        #self.ax31.set_ylim(0,1000)

        self.ax31 = self.ax3.twinx()


        self.figure;self.truckdat.plot(ax=self.ax31, kind='line',figsize=(18, 9), fontsize=6,grid = False,legend=False,xticks = ticker.iloc[0],ylim=(0,maxtruck))
        self.ax31.set(ylabel="Truck Hours ('000)")
        self.ax31.set_xticklabels(ticker.iloc[0])
        #self.ax3.set_ylim(0, max(self.truckdat))

        [t.set_visible(True) for t in self.ax31.get_xticklabels()]



class Plot_Window_Fin(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window_Fin, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("IMC Plotting Widget")

        self.ax = self.figure.add_subplot(221)
        self.ax1 = self.figure.add_subplot(222)
        self.ax2 = self.figure.add_subplot(223)
        self.ax3 = self.figure.add_subplot(224)
        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")

        # label=QLabel(self)
        # label.setText("Window Title")

        # self.setStyleSheet("QWidget {background-color:   #23297A}")

        # self.resize(1280, 860)

        # set the layout
        layout = QVBoxLayout()
        # layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        # layout.addWidget(self.button)
        self.setLayout(layout)

        #self.plotsched()
        self.plotfin()

    def plotfin(self):
        ''' plot Schedule Data '''
        self.setWindowTitle("IMC Financial")
        self.figure.tight_layout(pad=1)
        self.setWindowTitle('Financial Data')
        self.figure.figsize = (18, 14)

        self.hdffin = HDFStore('financial_data.h5')

        self.plot_data = read_hdf(self.hdffin, 'sched_report_fin_data')
        mask = self.plot_data['graph'] == 'f1'
        self.plot_data_f1 = self.plot_data[mask].copy()
        self.plot_data_f1.drop(['report_mult','graph'], axis=1, inplace=True)
        self.plot_data_f1.set_index(['report_name'],inplace=True)
        self.plot_data_f1 = self.plot_data_f1.T

        mask = self.plot_data['graph'] == 'f2'
        self.plot_data_f2 = self.plot_data[mask].copy()
        self.plot_data_f2.drop(['report_mult','graph'], axis=1, inplace=True)
        self.plot_data_f2.set_index(['report_name'],inplace=True)
        self.plot_data_f2 = self.plot_data_f2.T

        mask = self.plot_data['graph'] == 'f3'
        self.plot_data_f3 = self.plot_data[mask].copy()
        self.plot_data_f3.drop(['report_mult','graph'], axis=1, inplace=True)
        self.plot_data_f3.set_index(['report_name'],inplace=True)
        self.plot_data_f3 = self.plot_data_f3.T

        mask = self.plot_data['graph'] == 'f4'
        self.plot_data_f4 = self.plot_data[mask].copy()
        self.plot_data_f4.drop(['report_mult','graph'], axis=1, inplace=True)
        self.plot_data_f4.set_index(['report_name'],inplace=True)
        self.plot_data_f4 = self.plot_data_f4.T
        self.npv_data = read_hdf(self.hdffin, 'NPV')
        npv = self.npv_data.iloc[0]['value'].astype(int)
        #title4 = 'Net Value (NPV US$' + str(npv) + 'M)'
        title4 = 'Net Value (NPV US$' + ("{0:,.0f}".format(npv)) + 'M)'



        self.figure; self.plot_data_f1.plot(ax=self.ax, kind='bar',figsize=(18, 9),
                                                                 stacked=True, fontsize=6, title='Net Revenue')
        self.ax.set(xlabel="Period", ylabel="Million Dollars")
        self.figure; self.plot_data_f2.plot(ax=self.ax1, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Mining Costs')
        self.ax1.set(xlabel="Period", ylabel="Million Dollars")
        self.figure; self.plot_data_f3.plot(ax=self.ax2, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Depreciation')
        self.ax2.set(xlabel="Period", ylabel="Million Dollars")
        self.figure; self.plot_data_f4.plot(ax=self.ax3, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title=title4)
        self.ax3.set(xlabel="Period", ylabel="Million Dollars")




if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Plot_Window()
    main.show()

    sys.exit(app.exec_())