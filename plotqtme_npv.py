
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
from pandas import HDFStore
from pandas import read_hdf

plt.style.use('ggplot')
#plt.style.use('seaborn-paper')

class Plot_Window_NPV(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window_NPV, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("NPV")

        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")


        self.resize(1280,860)

        # set the layout
        layout = QVBoxLayout()
        #layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        #layout.addWidget(self.button)
        self.setLayout(layout)
        self.plotnpv()

    def plotnpv(self):
        ''' plot some random stuff '''

        self.figure.tight_layout(pad=3)
        self.setWindowTitle('NPV Sensitivity')
        self.figure.figsize=(18, 14)

        hdfplot = HDFStore('financial_data.h5')

        npv_base_data = read_hdf(hdfplot, 'NPV')
        npv_base_data_all = npv_base_data.rename(columns={'value':'NPV_Price'})
        npv_base_data_all['Discount Rate'] = npv_base_data_all['NPV_Price']
        npv_base_data_all['Operating Cost'] = npv_base_data_all['NPV_Price']
        npv_base_data_all['Capital Cost'] = npv_base_data_all['NPV_Price']
        npv_base_data_all.drop(['fin_data'], axis=1, inplace=True)
        npv_base_data_all = npv_base_data_all.T
        xtick_data = ['Price Deck - High/Low','Discount Rate +-2%','Operating Cost +-10%','Capital Cost +-10%']

        npv_base_data4 = read_hdf(hdfplot, 'NPV_4')
        npv_base_data5 = read_hdf(hdfplot, 'NPV_5')
        npv_base_data6 = read_hdf(hdfplot, 'NPV_6')
        npv_base_data7 = read_hdf(hdfplot, 'NPV_7')
        npv_base_data8 = read_hdf(hdfplot, 'NPV_8')
        npv_base_data9 = read_hdf(hdfplot, 'NPV_9')
        npv_cs = read_hdf('financial_data_cs.h5','NPV')
        npv_sq = read_hdf('financial_data_sq.h5', 'NPV')

        npv_base = npv_base_data['value'].mean()
        npv_cs =  npv_cs['value'].mean()
        npv_sq = npv_sq['value'].mean()
        npv_pricep = 0
        npv_pricem = npv_base - npv_sq
        npv_dp = npv_base - npv_base_data4['value'].mean()
        npv_dm = npv_base_data5['value'].mean() - npv_base
        npv_op = npv_base_data7['value'].mean() - npv_base
        npv_om = npv_base - npv_base_data6['value'].mean()
        npv_capp = npv_base_data9['value'].mean() - npv_base
        npv_capm = npv_base - npv_base_data8['value'].mean()

        ebar1 = [[npv_pricem,npv_dm,npv_om,npv_capm],[npv_pricep,npv_dp,npv_op,npv_capp]]
        self.figure; npv_base_data_all.plot(ax=self.ax, kind='bar', yerr = [ebar1],capsize=4,title='NPV Sensitivity ($000,000)',legend=False,  fontsize=7, color = 'lightblue')
        self.ax.set_xticklabels(xtick_data)
        self.figure.savefig("npvsens.png", dpi=600, bbox_inches='tight')






if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Plot_Window_NPV()
    main.show()

    sys.exit(app.exec_())
