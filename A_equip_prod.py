
#import costrun_class_period_rev1 as cran
from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
import equip_prod
import math
import pnames


#costrun = cran.CostModel(1)


class Equip_prod_view(QMainWindow, equip_prod.Ui_MainWindow):


    def __init__(self, costrun, equip_prod, parent=None):
        super(Equip_prod_view, self).__init__(parent)
        self.setupUi(self)
        self.equip_prod = equip_prod
        self.costrun = costrun


        self.comboBox_selected_activity.clear()
        self.comboBox_selected_equip.clear()

        alist = []
        #alist.append('0. select')
        for aid in self.costrun.all_activities:
            try:
                if aid.calc_method[0] == 'r':
                    act_id_select = aid.act_tree
                    alist.append(act_id_select)
            except:
                pass

        aset = set(alist)
        alist = list(aset)
        list.sort(alist)

        for aid in alist:
            act_id_select = aid
            self.comboBox_selected_activity.addItem(act_id_select)

        for eid in self.costrun.all_activities:
            try:
                if eid.act_tree == act_id_select:
                    equip_select = eid.eqtype
                    self.comboBox_selected_equip.addItem(equip_select)
            except:
                pass

        self.comboBox_selected_activity.currentIndexChanged.connect(self.showMe_act)
        self.pushButton.clicked.connect(lambda: self.home(self.comboBox_selected_activity.currentText(),self.comboBox_selected_equip.currentText()))

    def showMe_act(self):
        self.comboBox_selected_equip.clear()
        act_id_select = self.comboBox_selected_activity.currentText()
        for eid in self.costrun.all_activities:
            try:
                if eid.act_tree == act_id_select:
                    equip_select = eid.eqtype
                    self.comboBox_selected_equip.addItem(equip_select)
            except:
                pass

        #self.home(self.comboBox_selected_activity.currentText(), self.comboBox_selected_equip.currentText())

    def home(self,act_id_select, equip_select):

        #act_id_select = '1.4.1.2.2'
        #equip_select = 'Cat_385'

        for cid in self.costrun.all_activities:
            current_id = cid.act_tree
            current_eq = "NA"


            try:
                current_eq = cid.eqtype
                blp = cid.bucket_load_pass
            except:
                pass

            if (current_id == act_id_select) and (current_eq == equip_select):

                self.listWidget_loading.clear()
                self.listWidget_loading.addItem(str(cid.eqtype))

                self.listWidget_bucket.clear()
                try:
                    if math.isnan(cid.capacity):
                        self.listWidget_bucket.addItem(str('-'))
                    else:
                        self.listWidget_bucket.addItem(str(cid.capacity))
                except:
                    self.listWidget_bucket.addItem(str('-'))

                self.listWidget_fill.clear()
                try:
                    if math.isnan(cid.bucket_fill_factor):
                        self.listWidget_fill.addItem(str('-'))
                    else:
                        self.listWidget_fill.addItem(str(cid.bucket_fill_factor))
                except:
                    self.listWidget_fill.addItem(str('-'))

                self.listWidget_bucket_load_lcm.clear()
                try:
                    self.listWidget_bucket_load_lcm.addItem(str(cid.bucket_load_pass))
                except:
                    self.listWidget_bucket_load_lcm.addItem(str('-'))

                self.listWidget_swell.clear()
                try:
                    if math.isnan(cid.swell_into_bucket):
                        self.listWidget_swell.addItem(str('-'))
                    else:
                        self.listWidget_swell.addItem(str(cid.swell_into_bucket + 1))
                except:
                    self.listWidget_swell.addItem(str('-'))

                self.listWidget_bucket_load_bcm.clear()
                try:
                    self.listWidget_bucket_load_bcm.addItem(str(round(cid.bucket_load_pass_bcm,2)))
                except:
                    self.listWidget_bucket_load_bcm.addItem(str('-'))

                self.listWidget_wet_dens.clear()
                try:
                    self.listWidget_wet_dens.addItem(str(round(cid.insitu_wet_density,2)))
                except:
                    self.listWidget_wet_dens.addItem(str('-'))

                self.listWidget_lifting_cap.clear()
                try:
                    self.listWidget_lifting_cap.addItem('-')
                except:
                    self.listWidget_lifting_cap.addItem('-')

                self.listWidget_bucket_load_wt.clear()
                try:
                    self.listWidget_bucket_load_wt.addItem(str(round(cid.bucket_load_per_pass,2)))
                except:
                    self.listWidget_bucket_load_wt.addItem(str('-'))

                self.listWidget_hauling.clear()
                try:
                    self.listWidget_hauling.addItem(str(cid.truck_name))
                except:
                    self.listWidget_hauling.addItem(str('-'))

                self.listWidget_truck_cap_t.clear()
                try:
                    if math.isnan(cid.truck_capacity):
                        self.listWidget_truck_cap_t.addItem(str('-'))
                    else:
                        self.listWidget_truck_cap_t.addItem(str(cid.truck_capacity))
                except:
                    self.listWidget_truck_cap_t.addItem(str('-'))

                self.listWidget_truck_cap_lcm.clear()
                try:
                    self.listWidget_truck_cap_lcm.addItem(str(round(cid.truck_capacity / cid.insitu_wet_density,2)))
                except:
                    self.listWidget_truck_cap_lcm.addItem(str('-'))

                self.listWidget_passes_theor.clear()
                try:
                    self.listWidget_passes_theor.addItem(str(round(cid.number_passes,2)))
                except:
                    self.listWidget_passes_theor.addItem(str('-'))

                self.listWidget_passes_actual.clear()
                try:
                    self.listWidget_passes_actual.addItem(str(cid.number_passes_int))
                except:
                    self.listWidget_passes_actual.addItem(str('-'))

                self.listWidget_truck_load_lcm.clear()
                try:
                    self.listWidget_truck_load_lcm.addItem(str(round(cid.number_passes_int * cid.bucket_load_pass,2)))
                except:
                    self.listWidget_truck_load_lcm.addItem(str('-'))

                self.listWidget_overload_vol.clear()
                try:
                    self.listWidget_overload_vol.addItem(str(round((((cid.number_passes_int * cid.bucket_load_pass) / (cid.truck_capacity / cid.insitu_wet_density)-1))*100,2)))
                except:
                    self.listWidget_overload_vol.addItem(str('-'))

                self.listWidget_truck_load_wt_bf_carryb.clear()
                try:
                    self.listWidget_truck_load_wt_bf_carryb.addItem(str(round(cid.number_passes_int * cid.bucket_load_per_pass,2)))
                except:
                    self.listWidget_truck_load_wt_bf_carryb.addItem(str('-'))

                self.listWidget_overload_t.clear()
                try:
                    self.listWidget_overload_t.addItem(str(round(((((cid.number_passes_int * cid.bucket_load_per_pass) / cid.truck_capacity) - 1)*100),2)))
                except:
                    self.listWidget_overload_t.addItem(str('-'))

                self.listWidget_carryback.clear()
                try:
                    if math.isnan(cid.carryback):
                        self.listWidget_carryback.addItem(str('-'))
                    else:
                        self.listWidget_carryback.addItem(str(cid.carryback * 100))
                except:
                    self.listWidget_carryback.addItem(str('-'))

                self.listWidget_truck_load_bcm.clear()
                try:
                    self.listWidget_truck_load_bcm.addItem(str(round(cid.truck_load_bcm,2)))
                except:
                    self.listWidget_truck_load_bcm.addItem(str('-'))

                self.listWidget_truck_load_wt_af_carryb.clear()
                try:
                    self.listWidget_truck_load_wt_af_carryb.addItem(str(round(cid.truck_load_ton,2)))
                except:
                    self.listWidget_truck_load_wt_af_carryb.addItem(str('-'))

                self.listWidget_prop_single_side.clear()
                try:
                    if math.isnan(cid.single_side_loading):
                        self.listWidget_prop_single_side.addItem(str('-'))
                    else:
                        self.listWidget_prop_single_side.addItem(str(cid.single_side_loading * 100))
                except:
                    self.listWidget_prop_single_side.addItem(str('-'))

                self.listWidget_prop_double_side.clear()
                try:
                    self.listWidget_prop_double_side.addItem(str(cid.dbl_side_loading * 100))
                except:
                    self.listWidget_prop_double_side.addItem(str('-'))

                self.listWidget_spot.clear()
                try:
                    if math.isnan(cid.spot_time):
                        self.listWidget_spot.addItem(str('-'))
                    else:
                        self.listWidget_spot.addItem(str(cid.spot_time))
                except:
                    self.listWidget_spot.addItem(str('-'))

                self.listWidget_first_pass.clear()
                try:
                    if math.isnan(cid.first_pass):
                        self.listWidget_first_pass.addItem(str('-'))
                    else:
                        self.listWidget_first_pass.addItem(str(cid.first_pass))
                except:
                    self.listWidget_first_pass.addItem(str('-'))

                self.listWidget_other_pass.clear()
                try:
                    if math.isnan(cid.other_passes):
                        self.listWidget_other_pass.addItem(str('-'))
                    else:
                        self.listWidget_other_pass.addItem(str(cid.other_passes))
                except:
                    self.listWidget_other_pass.addItem(str('-'))

                self.listWidget_total_load_time.clear()
                try:
                    self.listWidget_total_load_time.addItem(str(round(cid.total_load_time,2)))
                except:
                    self.listWidget_total_load_time.addItem(str('-'))

                self.listWidget_loads_per_60m.clear()
                try:
                    self.listWidget_loads_per_60m.addItem(str(round(cid.loads_hour,1)))
                except:
                    self.listWidget_loads_per_60m.addItem(str('-'))

                self.listWidget_prod_per_60m_wt.clear()
                try:
                    self.listWidget_prod_per_60m_wt.addItem(str(round(cid.wet_tonnes_hour,2)))
                except:
                    self.listWidget_prod_per_60m_wt.addItem(str('-'))

                self.listWidget_prod_per_60m_bcm.clear()
                try:
                    self.listWidget_prod_per_60m_bcm.addItem(str(round(cid.bcm_hour,2)))
                except:
                    self.listWidget_prod_per_60m_bcm.addItem(str('-'))

                self.listWidget_work_eff_min.clear()
                try:
                    self.listWidget_work_eff_min.addItem(str(cid.curr_digger.o_roster.work_efficiency))
                except:
                    self.listWidget_work_eff_min.addItem(str('-'))

                self.listWidget_work_eff_perc.clear()
                try:
                    self.listWidget_work_eff_perc.addItem(str(round((cid.curr_digger.o_roster.work_efficiency/60) * 100,1)))
                except:
                    self.listWidget_work_eff_perc.addItem(str('-'))

                self.listWidget_truck_pres_perc.clear()
                try:
                    if math.isnan(cid.truck_presentation):
                        self.listWidget_truck_pres_perc.addItem(str('-'))
                    else:
                        self.listWidget_truck_pres_perc.addItem(str(cid.truck_presentation*100))
                except:
                    self.listWidget_truck_pres_perc.addItem(str('-'))

                self.listWidget_op_hours.clear()
                try:
                    self.listWidget_op_hours.addItem(str(round(cid.curr_digger.o_roster.annual_op_hours,2)))
                except:
                    self.listWidget_op_hours.addItem(str('-'))

                self.listWidget_effectiveness.clear()
                try:
                    self.listWidget_effectiveness.addItem(str(round(cid.effectiveness*100,1)))
                except:
                    self.listWidget_effectiveness.addItem(str('-'))

                self.listWidget_prod_wt_ophr.clear()
                try:
                    if cid.calc_method == 'rate_calc':
                        self.listWidget_prod_wt_ophr.addItem(str(round(cid.wet_tonnes_productivity,2)))
                    else:
                        self.listWidget_prod_wt_ophr.addItem(str(cid.prodrate_if_rate))
                except:
                    self.listWidget_prod_wt_ophr.addItem(str('-'))

                self.listWidget_prod_bcm_ophr.clear()
                try:
                    self.listWidget_prod_bcm_ophr.addItem(str(round(cid.bcm_productivity,2)))
                except:
                    self.listWidget_prod_bcm_ophr.addItem(str('-'))


def main():
    costrunt = cran.CostModel(1)
    equip_prod = getattr(costrunt,"Cat_385")
    app = QApplication(sys.argv)
    form = Equip_prod_view(costrunt, equip_prod)
    form.show()
    app.exec_()


if __name__ == '__main__':
    import sys
    main()
