
#import costrun_class_period_DP as cran
from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
import roster_equip
import pnames


#costrun = cran.CostModel(1)


class Roster_equip_view(QMainWindow, roster_equip.Ui_MainWindow):


    def __init__(self, costrun, curr_roster, parent=None):
        super(Roster_equip_view, self).__init__(parent)
        self.setupUi(self)
        self.curr_roster = curr_roster

        self.comboBox_Equip_Roster.clear()
        self.comboBox_Equip_Roster.addItem(curr_roster.roster_name)
        self.costrun = costrun

        for roster_count in self.costrun.all_rosters:
            rosters_all = roster_count.roster_name
            self.comboBox_Equip_Roster.addItem(rosters_all)


        self.home()

    def home(self):

        self.listWidget_roster_name.clear()
        self.listWidget_roster_name.addItem(str(self.curr_roster.roster_name))

        self.listWidget_calendar_days_2.clear()
        self.listWidget_calendar_days_2.addItem(str(self.curr_roster.calendar_days))

        self.listWidget_pub_hols.clear()
        self.listWidget_pub_hols.addItem(str(self.curr_roster.public_holidays))

        self.listWidget_equip_non_work.clear()
        self.listWidget_equip_non_work.addItem(str(self.curr_roster.equip_non_work))

        self.listWidget_sched_days.clear()
        self.listWidget_sched_days.addItem(str(round(self.curr_roster.scheduled_days,1)))

        self.listWidget_num_of_shifts.clear()
        self.listWidget_num_of_shifts.addItem(str(self.curr_roster.shifts_day))

        self.listWidget_shift_length.clear()
        self.listWidget_shift_length.addItem(str(self.curr_roster.hrs_shift))

        self.listWidget_sched_hrs.clear()
        self.listWidget_sched_hrs.addItem(str(round(self.curr_roster.sched_hours,1)))

        self.listWidget_planned_ons_maint.clear()
        self.listWidget_planned_ons_maint.addItem(str(self.curr_roster.planned_onshift_mtc))

        self.listWidget_planned_offs_maint.clear()
        self.listWidget_planned_offs_maint.addItem(str(self.curr_roster.planned_offshift_mtc))

        self.listWidget_rostered_maint.clear()
        self.listWidget_rostered_maint.addItem(str(self.curr_roster.rostered_onshift_mtc))

        self.listWidget_breakdown.clear()
        self.listWidget_breakdown.addItem(str(self.curr_roster.bdown))

        self.listWidget_avail_hrs.clear()
        self.listWidget_avail_hrs.addItem(str(round(self.curr_roster.available_hours,1)))

        self.listWidget_avail_days.clear()
        self.listWidget_avail_days.addItem(str(round(self.curr_roster.available_days,1)))

        self.listWidget_wet_weather_2.clear()
        self.listWidget_wet_weather_2.addItem(str(self.curr_roster.wet_weather))

        self.listWidget_de_rate_2.clear()
        self.listWidget_de_rate_2.addItem(str(self.curr_roster.de_rate_days))

        #self.listWidget_not_req.clear()
        #self.listWidget_not_req.addItem("0")               #de-rate days

        #self.listWidget_no_op.clear()
        #self.listWidget_no_op.addItem("0")                 #de-rate days

        #self.listWidget_industrial.clear()
        #self.listWidget_industrial.addItem("0")            #de-rate days

        self.listWidget_lunch.clear()
        self.listWidget_lunch.addItem(str(self.curr_roster.crib))

        self.listWidget_break.clear()
        self.listWidget_break.addItem(str(self.curr_roster.smoko))

        self.listWidget_shift_change.clear()
        self.listWidget_shift_change.addItem(str(self.curr_roster.shift_change))

        self.listWidget_daily_serv.clear()
        self.listWidget_daily_serv.addItem(str(self.curr_roster.service_lube))

        self.listWidget_op_hrs.clear()
        self.listWidget_op_hrs.addItem(str(round(self.curr_roster.annual_op_hours,1)))

        self.listWidget_op_days.clear()
        self.listWidget_op_days.addItem(str(round(self.curr_roster.op_days,1)))

        self.listWidget_awaiting_truck.clear()
        self.listWidget_awaiting_truck.addItem(str(self.curr_roster.awaiting_trucks))

        self.listWidget_awaiting_other.clear()
        self.listWidget_awaiting_other.addItem(str(self.curr_roster.awaiting_other_plant))

        self.listWidget_relocating.clear()
        self.listWidget_relocating.addItem(str(self.curr_roster.re_locating))

        self.listWidget_cleanup.clear()
        self.listWidget_cleanup.addItem(str(self.curr_roster.clean_up))

        self.listWidget_standby.clear()
        self.listWidget_standby.addItem(str(self.curr_roster.standby))

        self.listWidget_blasting.clear()
        self.listWidget_blasting.addItem(str(self.curr_roster.blasting_other))

        self.listWidget_hrly_work_eff.clear()
        self.listWidget_hrly_work_eff.addItem(str(self.curr_roster.work_efficiency))

        self.listWidget_eff_work_hrs.clear()
        self.listWidget_eff_work_hrs.addItem(str(round(self.curr_roster.effective_work_hours,1)))

        self.listWidget_cost_hrs.clear()
        self.listWidget_cost_hrs.addItem(str(round(self.curr_roster.annual_op_hours,1)))

        self.listWidget_main_avail.clear()
        self.listWidget_main_avail.addItem(str(round(self.curr_roster.maintenance_availability*100,1)))

        self.listWidget_eq_util.clear()
        self.listWidget_eq_util.addItem(str(round(self.curr_roster.operating_utilisation*100,1)))

        self.listWidget_Util.clear()
        self.listWidget_Util.addItem(str(round(self.curr_roster.annual_op_hours / self.curr_roster.sched_hours*100,1)))

        self.listWidget_eff.clear()
        self.listWidget_eff.addItem(str(round(self.curr_roster.effectiveness*100,1)))

        self.listWidget_eff_overall.clear()
        self.listWidget_eff_overall.addItem(str(round((self.curr_roster.effective_work_hours / 8760)*100,1)))


def main():
    costrunt = cran.CostModel(1)
    curr_roster = getattr(costrunt, "r1_10_dozer")
    app = QApplication(sys.argv)
    form = Roster_equip_view(costrunt,curr_roster)
    form.show()
    app.exec_()

if __name__ == '__main__':
    import sys
    main()
