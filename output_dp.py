

import openpyxl
import zipfile
from shutil import copyfile
from shutil import rmtree
import os
from pandas import HDFStore
from pandas import read_hdf

from openpyxl.utils.dataframe import dataframe_to_rows

wb = openpyxl.load_workbook('output_dp.xlsx')

### Schedule ###
hdfinput = HDFStore('input_data.h5')
df = read_hdf(hdfinput, 'sched_data_raw')

ws = wb['sched_data_raw']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
         ws.cell(row=r_idx, column=c_idx, value=value)


### Total Operating Fleet ###
hdftree = HDFStore('tree_data.h5')
df = read_hdf(hdftree, 'Total_Operating_Fleet')

ws = wb['total_operating_fleet']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
         ws.cell(row=r_idx, column=c_idx, value=value)


### Total Purchased Fleet ###

# Table only returns periods with purchased gear
#df = read_hdf(hdftree, 'Total_Purchased_Fleet')

#ws = wb['total_purchased_fleet']

#rows = dataframe_to_rows(df)

#for r_idx, row in enumerate(rows, 1):
    #for c_idx, value in enumerate(row, 1):
        #ws.cell(row=r_idx, column=c_idx, value=value)


### Total Required Fleet ###
hdftree = HDFStore('tree_data.h5')
df = read_hdf(hdftree, 'Total_Required_Fleet')

#####
# do magic with openpyxl here and save
ws = wb['total_required_fleet']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
         ws.cell(row=r_idx, column=c_idx, value=value)



### Total Fleet Manning ###
df = read_hdf(hdftree, 'opnum_tree')

#####
# do magic with openpyxl here and save
ws = wb['opnum_tree']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


### Admin Manning ###
df = read_hdf(hdftree, 'admin_tree_number')

ws = wb['admin_tree_number']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
         ws.cell(row=r_idx, column=c_idx, value=value)


### Admin Manning Cost ###
df = read_hdf(hdftree, 'admin_tree_cost')

ws = wb['admin_tree_cost']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)

### Overhead Cost ###
df = read_hdf(hdftree, 'ohead_tree_cost')

ws = wb['ohead_tree_cost']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


### Fuel ###
df = read_hdf(hdftree, 'fuel_data')

ws = wb['fuel_data']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)

### Operator Numbers ###
df = read_hdf(hdftree, 'op_equip_data')

ws = wb['op_equip_data']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


### Operating Cost Total ###
df = read_hdf(hdftree, 'tot_cost_tree')

ws = wb['tot_cost_tree']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


### Cost per dt ###
df = read_hdf(hdftree, 'cost_per_bx_dt')

ws = wb['cost_per_bx_dt']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)



### Cost per wt ###
df = read_hdf(hdftree, 'cost_per_bx_wt')

ws = wb['cost_per_bx_wt']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


### Cost per total tonne ###
df = read_hdf(hdftree, 'cost_per_tot_ton')

ws = wb['cost_per_tot_ton']

rows = dataframe_to_rows(df)

for r_idx, row in enumerate(rows, 1):
    for c_idx, value in enumerate(row, 1):
        ws.cell(row=r_idx, column=c_idx, value=value)


#ws['E2'] = 'Edited'   # example
#ws.cell(row=9, column=15).value = 'Cat_740'   # example
#####

wb.save('output_dp.xlsx')
