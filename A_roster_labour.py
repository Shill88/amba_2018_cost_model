
#import costrun_class_period_DP as cran
from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
from PyQt5 import QtGui, QtCore
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
import roster_labour
import pnames



class Roster_labour_view(QMainWindow, roster_labour.Ui_MainWindow):
    #roster_select = QtCore.pyqtSignal(str)


    def __init__(self, costrun, curr_roster, parent=None):
        super(Roster_labour_view, self).__init__(parent)
        self.setupUi(self)
        self.curr_roster = curr_roster

        self.comboBox_LabourRoster.clear()
        self.comboBox_LabourRoster.addItem(curr_roster.roster_name)
        self.costrun = costrun

        for roster_count in self.costrun.all_rosters:
            rosters_all = roster_count.roster_name
            self.comboBox_LabourRoster.addItem(rosters_all)

        #self.comboBox_LabourRoster.currentIndexChanged.connect(self.emit_Roster)

        self.home()

    def home(self):

        self.listWidget_roster_name.clear()
        self.listWidget_roster_name.addItem(str(self.curr_roster.roster_name))

        self.listWidget_hrs_per_shift.clear()
        self.listWidget_hrs_per_shift.addItem(str(self.curr_roster.hrs_shift))

        self.listWidget_shift_per_day.clear()
        self.listWidget_shift_per_day.addItem(str(self.curr_roster.shifts_day))

        self.listWidget_no_of_panels.clear()
        self.listWidget_no_of_panels.addItem(str(self.curr_roster.panels))

        self.listWidget_days_in_roster.clear()
        self.listWidget_days_in_roster.addItem(str(self.curr_roster.days_roster))

        self.listWidget_days_worked_in_roster.clear()
        self.listWidget_days_worked_in_roster.addItem(str(self.curr_roster.days_worked))

        self.listWidget_absenteeism.clear()
        self.listWidget_absenteeism.addItem(str((self.curr_roster.absent)*100))

        self.listWidget_calendar_days.clear()
        self.listWidget_calendar_days.addItem(str(self.curr_roster.calendar_days))

        self.listWidget_roster_days_off.clear()
        self.listWidget_roster_days_off.addItem(str(round(self.curr_roster.rostered_days_off,1)))

        self.listWidget_unpaid_shut.clear()
        self.listWidget_unpaid_shut.addItem(str(self.curr_roster.unpaid_days_lost))

        self.listWidget_total_paid_days.clear()
        self.listWidget_total_paid_days.addItem(str(round(self.curr_roster.total_paid_days,1)))

        self.listWidget_sick_leave.clear()
        self.listWidget_sick_leave.addItem(str(self.curr_roster.sick_leave))

        self.listWidget_pub_holidays.clear()
        self.listWidget_pub_holidays.addItem(str(self.curr_roster.public_holidays))

        self.listWidget_ann_leave.clear()
        self.listWidget_ann_leave.addItem(str(self.curr_roster.annual_leave))

        self.listWidget_total_leave.clear()
        self.listWidget_total_leave.addItem(str((self.curr_roster.annual_leave)+(self.curr_roster.sick_leave)+(self.curr_roster.public_holidays)))

        self.listWidget_actual_work_days.clear()
        self.listWidget_actual_work_days.addItem(str(round(self.curr_roster.actual_work_days,1)))


        self.listWidget_manhours_worked.clear()
        self.listWidget_manhours_worked.addItem(str(round(self.curr_roster.manhour_worked,1)))

        self.listWidget_manhours_paid.clear()
        self.listWidget_manhours_paid.addItem(str(round(self.curr_roster.manhours_paid,1)))

        self.listWidget_num_of_panels_2.clear()
        self.listWidget_num_of_panels_2.addItem(str(self.curr_roster.panels))

        self.listWidget_absenteeism_2.clear()
        self.listWidget_absenteeism_2.addItem(str((self.curr_roster.absent)*100))

        self.listWidget_leave_coverage.clear()
        self.listWidget_leave_coverage.addItem(str(round((((self.curr_roster.annual_leave)+(self.curr_roster.sick_leave))/(self.curr_roster.actual_work_days))*100,1)))

        self.listWidget_op_rate_ratio.clear()
        self.listWidget_op_rate_ratio.addItem(str(round(self.curr_roster.operator_ratio,2)))

    #def emit_Roster(self):
        # allow Qt's blocking of signals paradigm to control flow
        #print('fred')
        #if ( not self.signalsBlocked() ):
            #self.roster_select.emit(self.comboBox_LabourRoster.currentText())


def main():
    costrunt = cran.CostModel(1)
    curr_roster = getattr(costrunt, "r2_12_fel")
    app = QApplication(sys.argv)
    form = Roster_labour_view(costrunt, curr_roster)
    form.show()
    app.exec_()

if __name__ == '__main__':
    import sys
    main()
