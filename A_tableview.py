# -*- coding: utf-8 -*-
"""
Created on Mon May  1 09:34:41 2017

@author: slewis
"""


import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
from tkinter import *
from pandastable import Table, TableModel

class Table_view(Frame):
    
    def __init__(self, dbase, tname, parent=None):
        super(Table_view, self).__init__(parent)
        
        self.parent = parent
        Frame.__init__(self)
        self.main = self.master
        self.main.geometry('600x400+200+100')
        mainttitle_text = 'Table Viewer|Table Data Presented: ' + tname
        self.main.title(mainttitle_text)
        self.f = Frame(self.main)
        self.f.pack(fill=BOTH,expand=1)
        self.hdf =HDFStore(dbase)
        self.tableName=tname
        self.df=read_hdf(self.hdf,self.tableName)
        self.table = self.pt = Table(self.f, dataframe=self.df,showtoolbar=True, showstatusbar=True)
        self.pt.show()
         
        return 

#app = Amba_Table_view('resdata.h5','codes_data')
#launch the app
#app.mainloop()