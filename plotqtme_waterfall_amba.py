
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import pandas as pd
from pandas import HDFStore
from pandas import read_hdf
from matplotlib.ticker import FuncFormatter
import numpy as np

plt.style.use('ggplot')
#plt.style.use('seaborn-paper')

class Plot_Window_wfall(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window_wfall, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("NPV")

        self.ax = self.figure.add_subplot(111)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")


        self.resize(1280,860)

        # set the layout
        layout = QVBoxLayout()
        #layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        #layout.addWidget(self.button)
        self.setLayout(layout)
        self.plotwfall()

    def plotwfall(self):
        ''' plot some random stuff '''

        self.figure.tight_layout(pad=3)
        self.setWindowTitle('NPV Waterfall')
        self.figure.figsize=(18, 14)

        def money(x, pos):
            'The two args are the value and tick position'
            return "${:,.0f}".format(x)

        formatter = FuncFormatter(money)

        hdffinin = HDFStore('financial_data.h5')
        findata = read_hdf(hdffinin, 'report_names_npv')
        findata2 = findata.copy()

        mask = (findata2['report_name'] == ('Gross Value')) | (
        findata2['report_name'] == ('Capital Cost Status Quo')) | (
        findata2['report_name'] == ('Capital Cost Improvement')) | (
        findata2['report_name'] == ('Capital Cost Fleet')) | (
        findata2['report_name'] == ('Capital Salvage Income Fleet')) | (
        findata2['report_name'] == ('Op Cost General and Admin')) | (
        findata2['report_name'] == ('Op Cost Mining Operations')) | (
        findata2['report_name'] == ('Op Cost Small Fleet')) | (
        findata2['report_name'] == ('Op Cost Environmental')) | (
        findata2['report_name'] == ('Op Cost Process')) | (
        findata2['report_name'] == ('Total Marketing Costs')) | (
        findata2['report_name'] == ('Total_By_Product_Costs')) | (
        findata2['report_name'] == ('Net_Financing_Cost')) | (
        findata2['report_name'] == ('Royalty Paid')) | (
        findata2['report_name'] == ('Tax Paid')) | (
        findata2['report_name'] == ('Donations'))

        data = findata2[mask].copy()

        data.set_index(['report_name'], inplace=True)

        data = data.multiply(-1)
        mask = (data.index == 'Gross Value') | (data.index == 'Capital Salvage Income Fleet')

        data.loc[mask, 'npv'] *= -1

        GV = data.loc['Gross Value']['npv']
        CCSQ = data.loc['Capital Cost Status Quo']['npv']
        CCI = data.loc['Capital Cost Improvement']['npv']
        CCF = data.loc['Capital Cost Fleet']['npv']
        CSI = data.loc['Capital Salvage Income Fleet']['npv']
        OCG = data.loc['Op Cost General and Admin']['npv']
        OCO = data.loc['Op Cost Mining Operations']['npv']
        OCE = data.loc['Op Cost Environmental']['npv']
        OCP = data.loc['Op Cost Process']['npv']
        OSF = data.loc['Op Cost Small Fleet']['npv']
        RMk = data.loc['Total Marketing Costs']['npv']
        RBp = data.loc['Total_By_Product_Costs']['npv']
        Roy = data.loc['Royalty Paid']['npv']
        FIN = data.loc['Net_Financing_Cost']['npv']
        Don = data.loc['Donations']['npv']
        Tax = data.loc['Tax Paid']['npv']

        # Data to plot. Do not include a total, it will be calculated
        index = ['Gross Value', 'Cap SQ', 'Cap Improvement', 'Cap Fleet', 'Salvage', 'G & A', 'Mining Ops', 'Enviro', 'Small Fleet',
                  'Process',  'Marketing', 'By Product Cost', 'Financing','Donations','Royalty', 'Tax']

        data = {'amount': [GV, CCSQ, CCI, CCF, CSI, OCG, OCO,  OCE, OSF, OCP, RMk, RBp, FIN,  Don, Roy, Tax]}

        # Store data and create a blank series to use for the waterfall
        trans = pd.DataFrame(data=data, index=index)
        blank = trans.amount.cumsum().shift(1).fillna(0)

        # Get the net total number for the final element in the waterfall
        total = trans.sum().amount
        trans.loc["NPV"] = total
        blank.loc["NPV"] = total

        # The steps graphically show the levels as well as used for label placement
        step = blank.reset_index(drop=True).repeat(3).shift(-1)
        step[1::3] = np.nan

        # When plotting the last element, we want to show the full bar,
        # Set the blank to 0
        blank.loc["NPV"] = 0

        # Plot and label
        self.figure;trans.plot(ax=self.ax,kind='bar', stacked=True, bottom=blank, legend=None, figsize=(18, 9),
                             title="NPV Waterfall",fontsize=6, color = 'lightblue')
        self.ax.plot(step.index, step.values, 'k')
        self.ax.set_xlabel("Cost Centre")

        # Format the axis for dollars
        self.ax.yaxis.set_major_formatter(formatter)

        # Get the y-axis position for the labels
        y_height = trans.amount.cumsum().shift(1).fillna(0)

        # Get an offset so labels don't sit right on top of the bar
        max = trans.max()
        neg_offset = max / 25
        pos_offset = max / 50
        plot_offset = int(max / 15)

        # Start label loop
        loop = 0
        for index, row in trans.iterrows():
            # For the last item in the list, we don't want to double count
            if row['amount'] == total:
                y = y_height[loop]
            else:
                y = y_height[loop] + row['amount']
            # Determine if we want a neg or pos offset
            if row['amount'] > 0:
                y += pos_offset
            else:
                y -= neg_offset
            self.ax.annotate("{:,.0f}".format(row['amount']), (loop, y), ha="center")
            loop += 1

        # Scale up the y axis so there is room for the labels
        self.ax.set_ylim(0, blank.max() + int(plot_offset))
        # Rotate the labels
        self.ax.set_xticklabels(trans.index, rotation=0)
        self.figure.savefig("waterfall.png", dpi=600, bbox_inches='tight')


if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Plot_Window_wfall()
    main.show()

    sys.exit(app.exec_())
