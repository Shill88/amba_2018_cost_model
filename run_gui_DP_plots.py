# -*- coding: utf-8 -*-
"""
Created on Sat Apr 29 16:39:21 2017

@author: slewis
"""

from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
from pandas import HDFStore
import cost_tree_model_act_level
from pandas import read_hdf
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import tkinter
from tkinter import messagebox
import pandas as pd
import threading
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib
import costwindow_plot_tr
import costrun_class_period_DP as cran
import A_roster_labour
import A_roster_equip
import AB_roster_labour
import plotqtme
import A_cost_equip
import AB_cost_equip
import AB_roster_equip
import costrun_class_period_DP
import traceback, sys
import os
import output_cost_tables

matplotlib.style.use('ggplot')

ctm = cost_tree_model_act_level

class WorkerSignals(QObject):

    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class Cost_App(QMainWindow, costwindow_plot_tr.Ui_MainWindow):
    def __init__(self, parent=None):
        super(Cost_App, self).__init__(parent)
        self.setupUi(self)
        self.threadpool = QThreadPool()

        self.costrun_count = 0
        #self.costrun = cran.CostModel(1)
        #self.costrun = self.costrun_populate()
        self.cleared_boxes = 0

        self.comboBox_bottom.clear()
        self.comboBox_tl.clear()
        self.comboBox_middle.clear()
        self.comboBox_tr.clear()
        self.hdf = HDFStore('tree_data.h5')
        self.hdfdat = HDFStore('data.h5')
        self.listdb = self.hdf.keys()
        self.comboBox_bottom.addItems(self.listdb)
        self.comboBox_tl.addItems(self.listdb)
        self.comboBox_middle.addItems(self.listdb)
        self.hdfplot = HDFStore('plot_data.h5')
        self.listdbplot = self.hdfplot.keys()

        self.comboBox_tr.addItems(self.listdbplot)

        self.comboBox_bottom.currentIndexChanged.connect(self.selectionchosen_bottom)
        self.comboBox_tl.currentIndexChanged.connect(self.selectionchosen_tl)
        self.comboBox_middle.currentIndexChanged.connect(self.selectionchosen_middle)

        self.view_data_lt.clicked.connect(self.viewtablt)
        self.view_data_mid.clicked.connect(self.viewtabmid)
        self.view_data_bot.clicked.connect(self.viewtabbot)
        self.actionCost_Tables_Spreadsheet.triggered.connect(self.export_output)

        self.menuRe_Dock.addAction(self.dockWidget_2.toggleViewAction())
        self.menuRe_Dock.addAction(self.dockWidget_3.toggleViewAction())
        self.menuRe_Dock.addAction(self.dockWidget.toggleViewAction())
        self.menuRe_Dock.addAction(self.dockWidget_4.toggleViewAction())

        self.mplpushButton.clicked.connect(self.update_graph)
        self.comboBox_tr.currentIndexChanged.connect(self.update_graph)
        self.exitAction.triggered.connect(QtCore.QCoreApplication.quit)

        self.write_csv_tl.clicked.connect(
            lambda: self.save_csv_tl('Select Output CSV',
                                       'CSV Files (*.csv)'))

        self.write_csv_mid.clicked.connect(
            lambda: self.save_csv_mid('Select Output CSV',
                                       'CSV Files (*.csv)'))
        #self.write_csv_tr.clicked.connect(lambda: self.save_csv_tr('Select Output CSV','CSV Files (*.csv)'))
        self.write_csv_bot.clicked.connect(
            lambda: self.save_csv_bot('Select Output CSV',
                                       'CSV Files (*.csv)'))

        """Data Viewing"""
        self.actionView_Data.triggered.connect(self.viewdbase)


        try:
            index = self.comboBox_tl.findText('/cost_tree_sort', QtCore.Qt.MatchFixedString)
            self.comboBox_tl.setCurrentIndex(index)
            self.selectionchosen_tl()
            index = self.comboBox_middle.findText('/opnum_tree', QtCore.Qt.MatchFixedString)
            self.comboBox_middle.setCurrentIndex(index)
            self.selectionchosen_middle()
            self.update_graph()
        except:
            pass

        try:
            index = self.comboBox_bottom.findText('/Equipment_Activity_Inputs', QtCore.Qt.MatchFixedString)
            self.comboBox_bottom.setCurrentIndex(index)
            self.selectionchosen_bottom()
        except:
            pass

        self.actionCompile_Data.triggered.connect(self.compile_me)
        self.actionView_Roster.triggered.connect(self.showMe_rmenu)
        self.actionView_Equip.triggered.connect(self.showMe_emenu)


        self.tree_view_tl.doubleClicked.connect(lambda: self.handler_tl(1))
        self.tree_view_middle.doubleClicked.connect(lambda: self.handler_mid(1))
        self.tree_view_bottom.doubleClicked.connect(lambda: self.handler_bot(1))

        self.actionPlot_Fuel_Usage.triggered.connect(lambda: self.menu_plot('fuel_data'))



    def handler_tl(self,opt):

        if self.costrun_count == 0:
            self.costrun = cran.CostModel(1)
            self.costrun_count += 1

        if opt == 1:
            try:
                target_data = self.model_tl.itemData(self.tree_view_tl.selectedIndexes()[0])[0].split()[1]
                target_data = target_data.lower()

                self.first_target = target_data

                self.rdat = target_data[:1]

                if self.rdat == 'r':

                    curr_roster = getattr(self.costrun, target_data)
                    self.roster_equip = A_roster_equip.Roster_equip_view(self.costrun, curr_roster)
                    self.roster_me = A_roster_labour.Roster_labour_view(self.costrun, curr_roster)
                    self.roster_equip.show()
                    self.roster_equip.comboBox_Equip_Roster.currentIndexChanged.connect(self.showMe_eroster)
                    self.roster_me.show()
                    self.roster_me.comboBox_LabourRoster.currentIndexChanged.connect(self.showMe)
                else:
                    try:
                        target_data = self.model_tl.itemData(self.tree_view_tl.selectedIndexes()[0])[0].split()[1]
                        self.first_target = target_data
                        curr_equip = getattr(self.costrun, target_data)
                        self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                        self.equip_cost_me.show()
                        self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)

                    except:
                        pass

            except:
                try:
                    target_data = self.model_tl.itemData(self.tree_view_tl.selectedIndexes()[0])[0].split()[0]
                    self.first_target = target_data
                    curr_equip = getattr(self.costrun, target_data)
                    self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                    self.equip_cost_me.show()
                    self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)

                except:
                    pass

        elif opt == 2:
            target_data = self.roster_me.comboBox_LabourRoster.currentText()

            curr_roster = getattr(self.costrun, target_data)
            self.roster_me2 = AB_roster_labour.Roster_labour_view(curr_roster)
            self.roster_me2.show()

        elif opt == 10:
            target_data = self.roster_equip.comboBox_Equip_Roster.currentText()

            curr_roster = getattr(self.costrun, target_data)
            self.roster_equip2 = AB_roster_equip.Roster_equip_view(curr_roster)
            self.roster_equip2.show()


        elif opt == 20:
            target_data = self.equip_cost_me.listWidget_roster.currentItem().text().lower()
            curr_roster = getattr(self.costrun, target_data)
            self.roster_me2 = AB_roster_labour.Roster_labour_view(curr_roster)
            self.roster_me2.show()
            self.roster_equip2 = AB_roster_equip.Roster_equip_view(curr_roster)
            self.roster_equip2.show()

        elif opt == 30:
            target_data = self.costrun.all_rosters[0].roster_name
            curr_roster = getattr(self.costrun, target_data)
            self.roster_equip = A_roster_equip.Roster_equip_view(self.costrun, curr_roster)
            self.roster_me = A_roster_labour.Roster_labour_view(self.costrun, curr_roster)
            self.roster_equip.show()
            self.roster_equip.comboBox_Equip_Roster.currentIndexChanged.connect(self.showMe_eroster)
            self.roster_me.show()
            self.roster_me.comboBox_LabourRoster.currentIndexChanged.connect(self.showMe)


        elif opt == 40:
            target_data = self.costrun.all_equipment[0].etype
            curr_equip = getattr(self.costrun, target_data)
            self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)

            self.equip_cost_me.show()
            self.equip_cost_me.comboBox_equip_details.currentIndexChanged.connect(self.showMecost)
            self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)


        elif opt == 3:
            target_data = self.equip_cost_me.comboBox_equip_details.currentText()

            curr_equip = getattr(self.costrun, target_data)
            print('eqname',curr_equip.etype)
            self.equip_cost_meB = AB_cost_equip.Cost_equip_view(self.costrun,curr_equip)

            self.equip_cost_meB.show()
            self.equip_cost_meB.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)


        else:

            target_data = self.equip_cost_me.comboBox_equip_details.currentText()

            curr_equip = getattr(self.costrun, target_data)
            print('eqname',curr_equip.etype)
            self.equip_cost_meB = AB_cost_equip.Cost_equip_view(self.costrun,curr_equip)

            self.equip_cost_meB.show()

        #self.roster_me.comboBox_LabourRoster.currentIndexChanged.connect(self.showMe)
        #self.roster_equip.comboBox_Equip_Roster.currentIndexChanged.connect(self.showMe_eroster)

    def showMe(self):
        self.handler_tl(2)

    def showMe_rmenu(self):
        self.handler_tl(30)

    def showMe_emenu(self):
        self.handler_tl(40)

    def showMe_eroster(self):
        self.handler_tl(10)

    def showMecost(self):
        self.handler_tl(3)

    def showMecost_roster(self):
        self.handler_tl(20)

    def testMe(self):

        print(self.roster_me.roster_select)

    def handler_mid(self,opt):

        if self.costrun_count == 0:
            self.costrun = cran.CostModel(1)
            self.costrun_count += 1

        if opt == 1:
            try:
                target_data = self.model_middle.itemData(self.tree_view_middle.selectedIndexes()[0])[0].split()[1]
                target_data = target_data.lower()

                self.first_target = target_data

                rdat = target_data[:1]

                if rdat == 'r':

                    curr_roster = getattr(self.costrun, target_data)
                    self.roster_equip = A_roster_equip.Roster_equip_view(self.costrun, curr_roster)
                    self.roster_me = A_roster_labour.Roster_labour_view(self.costrun, curr_roster)
                    self.roster_equip.show()
                    self.roster_equip.comboBox_Equip_Roster.currentIndexChanged.connect(self.showMe_eroster)
                    self.roster_me.show()
                    self.roster_me.comboBox_LabourRoster.currentIndexChanged.connect(self.showMe)
                else:
                    try:
                        target_data = self.model_middle.itemData(self.tree_view_middle.selectedIndexes()[0])[0].split()[1]
                        self.first_target = target_data
                        curr_equip = getattr(self.costrun, target_data)
                        self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                        self.equip_cost_me.show()
                        self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)
                    except:
                        pass

            except:
                try:
                    target_data = self.model_middle.itemData(self.tree_view_middle.selectedIndexes()[0])[0].split()[0]
                    self.first_target = target_data
                    curr_equip = getattr(self.costrun, target_data)
                    self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                    self.equip_cost_me.show()
                    self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)
                except:
                    pass

    def handler_bot(self,opt):

        if self.costrun_count == 0:
            self.costrun = cran.CostModel(1)
            self.costrun_count += 1

        if opt == 1:
            try:
                target_data = self.model_bot.itemData(self.tree_view_bottom.selectedIndexes()[0])[0].split()[1]
                target_data = target_data.lower()

                self.first_target = target_data

                rdat = target_data[:1]

                if rdat == 'r':
                    curr_roster = getattr(self.costrun, target_data)
                    self.roster_equip = A_roster_equip.Roster_equip_view(self.costrun, curr_roster)
                    self.roster_me = A_roster_labour.Roster_labour_view(self.costrun, curr_roster)
                    self.roster_equip.show()
                    self.roster_equip.comboBox_Equip_Roster.currentIndexChanged.connect(self.showMe_eroster)
                    self.roster_me.show()
                    self.roster_me.comboBox_LabourRoster.currentIndexChanged.connect(self.showMe)
                else:
                    try:
                        target_data = self.model_bot.itemData(self.tree_view_bottom.selectedIndexes()[0])[0].split()[1]
                        self.first_target = target_data
                        curr_equip = getattr(self.costrun, target_data)
                        self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                        self.equip_cost_me.show()
                        self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)
                    except:
                        pass

            except:
                try:
                    target_data = self.model_bot.itemData(self.tree_view_bottom.selectedIndexes()[0])[0].split()[0]
                    self.first_target = target_data
                    curr_equip = getattr(self.costrun, target_data)
                    self.equip_cost_me = A_cost_equip.Cost_equip_view(self.costrun, curr_equip)
                    self.equip_cost_me.show()
                    self.equip_cost_me.listWidget_roster.itemDoubleClicked.connect(self.showMecost_roster)
                except:
                    pass

    def select_file(self):
        """opens a file select dialog"""
        # open the dialog and get the selected file
        file, _ = QtWidgets.QFileDialog.getOpenFileName()
        # if a file is selected
        if file:
            # update the lineEdit widget text with the selected filename
            self.mpllineEdit.setText(file)

    def update_graph(self):

        try:
            self.data_tr = self.comboBox_tr.currentText()

            self.graph_data = read_hdf(self.hdfplot, self.data_tr)

        except:
            return

        try:
            plot_type = self.graph_data['plot_type'][0]
        except:
            plot_type = 'stacked'

        self.mpl.canvas.ax.clear()

        if plot_type == 'unit_cost_pie':
            tondat = self.data_tr[10:]
            ton_data = read_hdf(self.hdfdat,'dcf_ton')
            maskton = ton_data['unit'] == tondat
            try:
                disc_ton = ton_data.loc[maskton,'xnpv'].sum()
                cost_data = read_hdf(self.hdfdat, 'total_cost_disc')
                maskcost = cost_data['disc_cost'] == 'total_cost'
                cost_disc = cost_data.loc[maskcost,'xnpv'].sum()
                tot_uc = cost_disc / disc_ton
            except:
                tot_uc = 0

            #tot_uc = self.graph_data['unit_costs'].sum()

            self.mpl.canvas.ax.set_title('Discounted ' +  self.data_tr[1:] + ' ($' + str(round(tot_uc, 2)) + ')')

            self.mpl.canvas.ax.pie(self.graph_data.unit_costs, labels=self.graph_data.Name, autopct='%1.1f%%', shadow=True, startangle=90)
            self.mpl.canvas.draw()
        else:

            plotme = plotqtme.Plot_Window()
            plotme.show()


    def menu_plot(self,data_plot):

        plotme = plotqtme.Plot_Window()
        plotme.show()

    def reDock(self):
        fred = self.tree_view_tl
        self.restoreDockWidget(fred)
        fred2 = self.tree_view_middle
        self.restoreDockWidget(fred2)
        fred3 = self.tree_view_tr
        self.restoreDockWidget(fred3)
        fred4 = self.tree_view_bottom
        self.restoreDockWidget(fred4)

    def save_csv_tl(self, text_data, file_type):
        import fileselector
        fdata = fileselector.FileApp()
        self.filedata = fdata.saveFileDialog(text_data, file_type)
        self.outfile = self.filedata
        self.cost_tree_tl.to_csv(self.outfile, index=False)

    def save_csv_mid(self, text_data, file_type):
        import fileselector
        fdata = fileselector.FileApp()
        self.filedata = fdata.saveFileDialog(text_data, file_type)
        self.outfile = self.filedata
        self.cost_tree_mid.to_csv(self.outfile, index=False)

    def save_csv_tr(self, text_data, file_type):
        import fileselector
        fdata = fileselector.FileApp()
        self.filedata = fdata.saveFileDialog(text_data, file_type)
        self.outfile = self.filedata
        self.cost_tree_tr.to_csv(self.outfile, index=False)

    def save_csv_bot(self, text_data, file_type):
        import fileselector
        fdata = fileselector.FileApp()
        self.filedata = fdata.saveFileDialog(text_data, file_type)
        self.outfile = self.filedata
        self.cost_tree_bot.to_csv(self.outfile, index=False)

    def viewtablt(self):
        import A_tableview
        dbase = 'tree_data.h5'
        tname = self.comboBox_tl.currentText()
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def viewtabmid(self):
        import A_tableview
        dbase = 'tree_data.h5'
        tname = self.comboBox_middle.currentText()
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def viewtabtr(self):
        import A_tableview
        dbase = 'tree_data.h5'
        tname = self.comboBox_tr.currentText()
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def viewtabbot(self):
        import A_tableview
        dbase = 'tree_data.h5'
        tname = self.comboBox_bottom.currentText()
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def viewtablt(self):
        import A_tableview
        dbase = 'tree_data.h5'
        tname = self.comboBox_tl.currentText()
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def selectionchosen_tl(self):
        self.data_tl = self.comboBox_tl.currentText()
        if self.cleared_boxes == 0:
            self.cost_tree = read_hdf(self.hdf,self.data_tl)
            #self.cost_tree = self.cost_tree.iloc[1:]
            self.cost_tree_tl = self.cost_tree
            self.headers = list(self.cost_tree)
            self.headers.pop(0)
            self.headers.pop(1)
            self.headers[0] = self.data_tl

            self.model_tl = ctm.TreeModel(self.headers,self.cost_tree)

            self.tree_view_tl.setModel(self.model_tl)
            self.tree_view_tl.setWindowTitle('chosen_data')
            self.tree_view_tl.expandAll()
            self.tree_view_tl.resizeColumnToContents(0)
            self.tree_view_tl.resizeColumnToContents(1)

    def selectionchosen_middle(self):
        self.data_middle = self.comboBox_middle.currentText()
        try:
            if self.cleared_boxes == 0:
                self.cost_tree = read_hdf(self.hdf,self.data_middle)
                #self.cost_tree = self.cost_tree.iloc[1:]
                self.cost_tree_mid = self.cost_tree
                self.headers = list(self.cost_tree)
                self.headers.pop(0)
                self.headers.pop(1)
                self.headers[0] = self.data_middle

                self.model_middle = ctm.TreeModel(self.headers,self.cost_tree)

                self.tree_view_middle.setModel(self.model_middle)
                self.tree_view_middle.setWindowTitle('chosen_data')
                self.tree_view_middle.expandAll()
                self.tree_view_middle.resizeColumnToContents(0)
                self.tree_view_middle.resizeColumnToContents(1)
        except:
            pass

    def selectionchosen_tr(self):
        self.data_tr = self.comboBox_tr.currentText()
        try:
            if self.cleared_boxes == 0:
                self.cost_tree = read_hdf(self.hdf,self.data_tr)
                #self.cost_tree = self.cost_tree.iloc[1:]
                self.cost_tree_tr = self.cost_tree
                self.headers = list(self.cost_tree)
                self.headers.pop(0)
                self.headers.pop(1)
                self.headers[0] = self.data_tr

                self.model_tr = ctm.TreeModel(self.headers,self.cost_tree)

                self.tree_view_tr.setModel(self.model_tr)
                self.tree_view_tr.setWindowTitle('chosen_data')
                self.tree_view_tr.expandAll()
                self.tree_view_tr.resizeColumnToContents(0)
                self.tree_view_tr.resizeColumnToContents(1)
        except:
            pass

    def selectionchosen_bottom(self):
        self.data_bottom = self.comboBox_bottom.currentText()
        try:
            if self.cleared_boxes == 0:
                self.cost_tree = read_hdf(self.hdf,self.data_bottom)
                #self.cost_tree = self.cost_tree.iloc[1:]
                self.cost_tree_bot = self.cost_tree
                self.headers = list(self.cost_tree)
                self.headers.pop(0)
                self.headers.pop(1)
                self.headers[0] = self.data_bottom

                self.model_bot = ctm.TreeModel(self.headers,self.cost_tree)

                self.tree_view_bottom.setModel(self.model_bot)
                self.tree_view_bottom.setWindowTitle('chosen_data')
                self.tree_view_bottom.expandAll()
                self.tree_view_bottom.resizeColumnToContents(0)
                self.tree_view_bottom.resizeColumnToContents(1)
        except:
            pass

    def selectionchosendb(self):
        self.viewtabs(self.dbase_chosen,self.ui.comboBox_selectTable.currentText())

    def selectionchanged(self):
        self.dbase_chosen=self.ui.comboBox_selectDbase.currentText()
        self.ui.comboBox_selectTable.clear()
        self.hdfdb=HDFStore(self.dbase_chosen)
        self.listdb=self.hdfdb.keys()
        self.ui.comboBox_selectTable.addItems(self.listdb)

    def viewdbase(self):
        import select_dbase_combo
        self.Dialog = QtWidgets.QDialog()
        self.ui = select_dbase_combo.Ui_Dialog()
        self.ui.setupUi(self.Dialog)
        self.ui.comboBox_selectDbase.addItems([' ','tree_data.h5', 'data.h5', 'input_data.h5'])
        self.ui.comboBox_selectDbase.currentIndexChanged.connect(self.selectionchanged)
        self.ui.pushButton_Ok_SelectDbase.clicked.connect(self.selectionchosendb)
        self.Dialog.show()

    def viewtabs(self, dbase, tname):
        import A_tableview
        self.table_me = A_tableview.Table_view(dbase, tname)
        self.table_me.mainloop()

    def compile_me(self):
        self.hdf.close()
        self.hdfplot.close()
        #try:
        #    os.remove("tree_data.h5")
        #    os.remove("plot_data.h5")
        #    os.remove("data.h5")
        #    os.remove("year_data.h5")
        #    os.remove("input_data.h5")
        #    os.remove("input_data.pkl")

        #except:
        #    pass

        self.cleared_boxes = 1
        self.comboBox_bottom.clear()
        self.comboBox_tl.clear()
        self.comboBox_middle.clear()
        self.comboBox_tr.clear()

        QMessageBox.about(self, "Cost Update",
                          "This Will Take about a minute - wait until the next message pops up then all finished")

        if self.costrun_count != 0:
            self.__delattr__('costrun')
            self.costrun_count = 0

        worker = Worker(self.worker_runcostmodel_DP)
        worker.signals.finished.connect(self.thread_complete)
        self.threadpool.start(worker)


    def thread_complete(self):

        print('finished thread')
        self.cleared_boxes = 1
        self.comboBox_bottom.clear()
        self.comboBox_tl.clear()
        self.comboBox_middle.clear()
        self.comboBox_tr.clear()
        self.hdf = HDFStore('tree_data.h5')
        self.listdb = self.hdf.keys()
        self.comboBox_bottom.addItems(self.listdb)
        self.comboBox_tl.addItems(self.listdb)
        self.comboBox_middle.addItems(self.listdb)
        self.hdfplot = HDFStore('plot_data.h5')
        self.listdbplot = self.hdfplot.keys()

        self.comboBox_tr.addItems(self.listdbplot)

        self.cleared_boxes = 0

        try:
            index = self.comboBox_tl.findText('/cost_tree_sort', QtCore.Qt.MatchFixedString)
            self.comboBox_tl.setCurrentIndex(index)
            self.selectionchosen_tl()
            index = self.comboBox_middle.findText('/opnum_tree', QtCore.Qt.MatchFixedString)
            self.comboBox_middle.setCurrentIndex(index)
            self.selectionchosen_middle()
            self.update_graph()
        except:
            pass

        try:
            index = self.comboBox_bottom.findText('/Equipment_Activity_Inputs', QtCore.Qt.MatchFixedString)
            self.comboBox_bottom.setCurrentIndex(index)
            self.selectionchosen_bottom()
        except:
            pass

        QMessageBox.about(self, "Cost Update", "The Cost Model has been updated with Spreadsheet Data from the spreadsheet defined in cost_in.ini file")


    def worker_runcostmodel_DP(self, progress_callback):

        costme = costrun_class_period_DP.CostModel(2)

        return "done."

    def export_output(self):

        QMessageBox.about(self, "Export Cost",
                          "This will overwrite output.xlsx, output tab names need to match table names exactly")



        self.fred = output_cost_tables.Output_tabs.runme()

        return


def main():
    app = QApplication(sys.argv)
    form = Cost_App()
    form.show()
    app.exec_()
    
if __name__ == '__main__':
    
    import sys
    main()
            