
import pandas as pd

from pandas import read_hdf
from pandas import HDFStore
import A_Config_Utils

ConfigIni = A_Config_Utils.Read_Config()

class financial_me():

    def __init__(self,parent=None):
        super(financial_me, self).__init__(parent)

    def financial(self,capital_costs,calendar_data,fleet_cap_data, tot_costs_lev3):

        file_in_x = ConfigIni.read_config('bm_in.ini', 'blockmod').get('fin_data')
        fin_file = pd.ExcelFile(file_in_x)
        fin_data = fin_file.parse('fin_data')
        dep_am = fin_file.parse('dep_amortise')
        royalty = fin_file.parse('royalty')
        real_nom = fin_file.parse('price_deck')



        dep_am.set_index(['data'], inplace=True)
        fin_data.set_index(['fin_data'], inplace=True)

        calseries = len(calendar_data)
        caldat = list(range(calseries))
        caldat = pd.Series(caldat)
        caldat.index = caldat.index + 1
        #caldat = calendar_data

        cals = pd.Series(calendar_data)
        cals180 = cals + pd.DateOffset(months=6)


        real_nom.set_index(['price'],inplace=True)
        real_nom = real_nom.loc['real_nom']
        real_nom = real_nom[real_nom.index <= calseries].copy()


        fleet_capital_all_m1 = fleet_cap_data.copy()
        masky1 = fleet_capital_all_m1['sched_period'] > 1
        fleet_capital_all_m1.loc[masky1,'sched_period'] -= 1

        total_costs_lev3 = tot_costs_lev3.rename(columns={'Name': 'report_name'})
        total_costs_lev3 = total_costs_lev3.rename(columns={'Act_Id': 'report_mult'})
        total_costs_lev3['graph'] = 0
        total_costs_lev3.set_index(['report_name','report_mult','graph'],inplace=True)
        years = len(list(total_costs_lev3))
        total_costs_lev3.columns = range(1,years + 1)
        multi = fin_data.loc['revenue_multiplier']['value']
        total_costs_lev3 = total_costs_lev3/multi
        total_sum = total_costs_lev3.sum()
        total_costs_lev3.reset_index(inplace=True)
        total_costs_lev3['report_mult'] = multi
        mask_tmp = total_costs_lev3['report_name'] == 'G_A'
        total_costs_lev3.loc[mask_tmp,'report_name'] = 'Op Cost General and Admin'
        mask_tmp = total_costs_lev3['report_name'] == 'Mining_Ops'
        total_costs_lev3.loc[mask_tmp, 'report_name'] = 'Op Cost Mining Operations'
        mask_tmp = total_costs_lev3['report_name'] == 'Stockpile_Reclaim'
        total_costs_lev3.loc[mask_tmp, 'report_name'] = 'Op Cost Stockpile Reclaim'
        mask_tmp = total_costs_lev3['report_name'] == 'Process'
        total_costs_lev3.loc[mask_tmp, 'report_name'] = 'Op Cost Process'
        mask_tmp = total_costs_lev3['report_name'] == 'Environmental'
        total_costs_lev3.loc[mask_tmp, 'report_name'] = 'Op Cost Environmental'
        total_costs_lev3['graph'] = 'f2'

        total_sum = total_sum.rename("Total Operating Cost")
        total_sum_raw = total_sum.copy()
        total_sum = total_sum.to_frame()
        total_sum = total_sum.T
        total_sum = total_sum.reset_index()
        total_sum = total_sum.rename(columns={'index': 'report_name'})
        total_sum['report_mult'] = multi
        total_sum['graph'] = 0
        total_sum['sorter'] = 133


        """Manning Totals"""

        hdffinin = HDFStore('fin_in_data.h5')
        tot_manning = read_hdf(hdffinin,'tot_manning_heads')

        tot_manning = tot_manning.rename(columns={'Name': 'report_name'})
        tot_manning = tot_manning.rename(columns={'Act_Id': 'report_mult'})
        tot_manning['graph'] = 0
        tot_manning.set_index(['report_name','report_mult','graph'],inplace=True)
        years = len(list(tot_manning))
        tot_manning.columns = range(1,years + 1)
        total_manning_sum = tot_manning.sum()
        tot_manning.reset_index(inplace=True)
        tot_manning['report_mult'] = 1
        mask_tmp = tot_manning['report_name'] == 'G_A'
        tot_manning.loc[mask_tmp,'report_name'] = 'Manning Number General and Admin'
        mask_tmp = tot_manning['report_name'] == 'Mining_Ops'
        tot_manning.loc[mask_tmp, 'report_name'] = 'Manning Number Mining Operations'
        mask_tmp = tot_manning['report_name'] == 'Stockpile_Reclaim'
        tot_manning.loc[mask_tmp, 'report_name'] = 'Manning Number Stockpile Reclaim'
        mask_tmp = tot_manning['report_name'] == 'Process'
        tot_manning.loc[mask_tmp, 'report_name'] = 'Manning Number Process'
        mask_tmp = tot_manning['report_name'] == 'Environmental'
        tot_manning.loc[mask_tmp, 'report_name'] = 'Manning Number Environmental'

        tot_manning['graph'] = 'a2'

        total_manning_sum = total_manning_sum.rename("Total Manning Number")
        total_manning_sum_raw = total_manning_sum.copy()
        total_manning_sum = total_manning_sum.to_frame()
        total_manning_sum = total_manning_sum.T
        total_manning_sum = total_manning_sum.reset_index()
        total_manning_sum = total_manning_sum.rename(columns={'index': 'report_name'})
        total_manning_sum['report_mult'] = 1
        total_manning_sum['graph'] = 0
        total_manning_sum['sorter'] = 160

        tot_fuel = read_hdf(hdffinin, 'fuel_data_raw')
        tot_fuel.set_index(['Equip'],inplace=True)
        tot_fuel = tot_fuel.loc['Total']
        ylist = list(total_manning_sum_raw.index)
        tot_fuel_yr = tot_fuel[~tot_fuel.isin(ylist)] / 1000000
        fuel_dat = financial_me.adder(self, tot_fuel_yr, 'Fuel Litres', 'a2', 1000000, 161)

        tot_kw = read_hdf(hdffinin, 'tot_kw_data_raw')
        tot_kw.set_index(['Equip'],inplace=True)
        tot_kw = tot_kw.loc['Total']
        ylist = list(total_manning_sum_raw.index)
        tot_kw_yr = tot_kw[~tot_kw.isin(ylist)] / 1000000
        kw_dat = financial_me.adder(self, tot_kw_yr, 'kW Hours', 'a2', 1000000, 162)

        mobile_capital = fleet_capital_all_m1.groupby(['sched_period'])['capital'].sum()
        mobile_capital = (caldat * 0 + 1) * mobile_capital
        mobile_capital.fillna(value=0, inplace=True)

        "run for sensitivities"

        scenario_list = ['sched_report_fin_data','sched_report_fin_data_cs','sched_report_fin_data_sq']
        hdfinputs = HDFStore('input_data.h5')
        scenario_list = ['sched_report_fin_data', 'sched_report_fin_data_cs', 'sched_report_fin_data_sq','dr2','dr_2','opcost10','opcost_10','capcost10','capcost_10']
        scenario_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        #scenario_list = [1, 2, 3]

        for srun in scenario_list:

            hdfoutput = HDFStore('financial_data.h5')
            scenario = 'sched_report_fin_data'
            df_adjust = 0
            opcost_factor = 1
            capcost_factor = 1
            if srun == 2:
                hdfoutput = HDFStore('financial_data_cs.h5')
                scenario = 'sched_report_fin_data_cs'
            if srun == 3:
                hdfoutput = HDFStore('financial_data_sq.h5')
                scenario = 'sched_report_fin_data_sq'

            if srun == 4:
                df_adjust = 0.02
            if srun == 5:
                df_adjust = -0.02
            if srun == 6:
                opcost_factor = 1.1
            if srun == 7:
                opcost_factor = 0.9
            if srun == 8:
                capcost_factor = 1.1
            if srun == 9:
                capcost_factor = 0.9


            df = read_hdf(hdfinputs, scenario)

            dep_years_mobile = fin_data.loc['mobile_fleet_dep_years']['value']
            dep_cap = pd.DataFrame(columns=['period','depreciate_mobile'])

            mobile_capital = mobile_capital * capcost_factor


            """this is for real"""

            for i, row in mobile_capital.iteritems():
                for ii in range(i,i+dep_years_mobile):
                    if ii > len(mobile_capital): break
                    dep_data = {'period': ii, 'depreciate_mobile': row / dep_years_mobile}
                    dep_cap = dep_cap.append([dep_data])

            depreciate_mobile = dep_cap.groupby(['period'])['depreciate_mobile'].sum()
            dep_mobile_raw = depreciate_mobile.copy()

            dep_mobile_raw = (caldat * 0 + 1) * dep_mobile_raw
            dep_mobile_raw.fillna(value=0, inplace=True)
            depreciate_mobile = depreciate_mobile.to_frame()
            depreciate_mobile = depreciate_mobile.T
            depreciate_mobile = depreciate_mobile.reset_index()

            depreciate_mobile = depreciate_mobile.rename(columns={'index': 'report_name'})
            depreciate_mobile['report_mult'] = multi
            depreciate_mobile['graph'] = 'f3'
            depreciate_mobile['sorter'] = 334
            depreciate_mobile.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            depreciate_mobile.reset_index(inplace=True)

            """this is for nominal"""

            dep_cap_nominal = pd.DataFrame(columns=['period','depreciate_mobile'])
            mobile_capital_nominal = mobile_capital * real_nom


            for i, row in mobile_capital_nominal.iteritems():
                for ii in range(i,i+dep_years_mobile):
                    if ii > len(mobile_capital_nominal): break
                    dep_data = {'period': ii, 'depreciate_mobile': row / dep_years_mobile}
                    dep_cap_nominal = dep_cap.append([dep_data])

            depreciate_mobile_nominal = dep_cap_nominal.groupby(['period'])['depreciate_mobile'].sum()
            dep_mobile_raw_nominal = depreciate_mobile_nominal.copy()

            dep_mobile_raw_nominal = (caldat * 0 + 1) * dep_mobile_raw_nominal
            dep_mobile_raw_nominal.fillna(value=0, inplace=True)
            depreciate_mobile_nominal = depreciate_mobile_nominal.to_frame()
            depreciate_mobile_nominal = depreciate_mobile_nominal.T
            depreciate_mobile_nominal = depreciate_mobile_nominal.reset_index()

            depreciate_mobile_nominal = depreciate_mobile_nominal.rename(columns={'index': 'report_name'})
            depreciate_mobile_nominal['report_mult'] = multi
            depreciate_mobile_nominal['graph'] = 'f3'
            depreciate_mobile_nominal['sorter'] = 335
            depreciate_mobile_nominal.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            depreciate_mobile_nominal.reset_index(inplace=True)


            listcap = ['Capital_Status_Quo','Capital_Improvement']
            cap_cost_temp = capital_costs[capital_costs.index.isin(listcap)].copy()

            """This is for Real"""

            cap_cols = list(cap_cost_temp)
            cap_multi = capital_costs.iloc[0]['multi']

            for coldat in cap_cols:
                if coldat != 1:
                    cap_cost_temp.drop(coldat, axis=1, inplace=True)
                else:
                    break

            dep_years_infra = fin_data.loc['infrastructure_dep_years']['value']
            dep_infra = pd.DataFrame(columns=['period', 'depreciate_infrastructure'])

            cap_cost_temp = cap_cost_temp.T
            cap_cost_sq = cap_cost_temp['Capital_Status_Quo']
            cap_cost_sq *= capcost_factor
            cap_cost_improve = cap_cost_temp['Capital_Improvement']
            cap_cost_improve *= capcost_factor

            cap_cost_sum = cap_cost_temp.sum(axis=1)
            cap_cost_sum *= capcost_factor

            for i, row in cap_cost_sum.iteritems():
                for ii in range(i,i+dep_years_infra):
                    if ii > len(cap_cost_sum): break
                    dep_data = {'period': ii, 'depreciate_infra': row / dep_years_infra}
                    dep_infra = dep_infra.append([dep_data])

            depreciate_infra = dep_infra.groupby(['period'])['depreciate_infra'].sum()
            dep_infra_raw = depreciate_infra.copy()
            dep_infra_raw = (caldat * 0 + 1) * dep_infra_raw
            dep_infra_raw.fillna(value=0, inplace=True)
            depreciate_infra = depreciate_infra.to_frame()
            depreciate_infra = depreciate_infra.T
            depreciate_infra = depreciate_infra.reset_index()

            depreciate_infra = depreciate_infra.rename(columns={'index': 'report_name'})
            depreciate_infra['report_mult'] = cap_multi
            depreciate_infra['graph'] = 'f3'
            depreciate_infra['sorter'] = 335
            depreciate_infra.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            depreciate_infra.reset_index(inplace=True)


            """This is for nominal"""

            cap_cost_temp_nominal = capital_costs[capital_costs.index.isin(listcap)].copy()

            cap_cols = list(cap_cost_temp_nominal)
            cap_multi = capital_costs.iloc[0]['multi']

            for coldat in cap_cols:
                if coldat != 1:
                    cap_cost_temp_nominal.drop(coldat, axis=1, inplace=True)
                else:
                    break

            dep_years_infra = fin_data.loc['infrastructure_dep_years']['value']
            dep_infra_nominal = pd.DataFrame(columns=['period', 'depreciate_infrastructure'])

            cap_cost_temp_nominal = cap_cost_temp_nominal.T
            cap_cost_sq_nominal = cap_cost_temp_nominal['Capital_Status_Quo']
            cap_cost_sq_nominal *= capcost_factor
            cap_cost_improve_nominal = cap_cost_temp_nominal['Capital_Improvement']
            cap_cost_improve_nominal *= capcost_factor

            cap_cost_sum_nominal = cap_cost_temp_nominal.sum(axis=1)
            cap_cost_sum_nominal *= capcost_factor
            cap_cost_sum_nominal *= real_nom

            for i, row in cap_cost_sum_nominal.iteritems():
                for ii in range(i,i+dep_years_infra):
                    if ii > len(cap_cost_sum_nominal): break
                    dep_data = {'period': ii, 'depreciate_infra': row / dep_years_infra}
                    dep_infra_nominal = dep_infra_nominal.append([dep_data])

            depreciate_infra_nominal = dep_infra_nominal.groupby(['period'])['depreciate_infra'].sum()
            dep_infra_raw_nominal = depreciate_infra_nominal.copy()
            dep_infra_raw_nominal = (caldat * 0 + 1) * dep_infra_raw_nominal
            dep_infra_raw_nominal.fillna(value=0, inplace=True)
            depreciate_infra_nominal = depreciate_infra_nominal.to_frame()
            depreciate_infra_nominal = depreciate_infra_nominal.T
            depreciate_infra_nominal = depreciate_infra_nominal.reset_index()

            depreciate_infra_nominal = depreciate_infra_nominal.rename(columns={'index': 'report_name'})
            depreciate_infra_nominal['report_mult'] = cap_multi
            depreciate_infra_nominal['graph'] = 'f3'
            depreciate_infra_nominal['sorter'] = 335
            depreciate_infra_nominal.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            depreciate_infra_nominal.reset_index(inplace=True)




            sched_raw = read_hdf(hdfinputs,'sched_data_raw')
            sched_raw = sched_raw.loc[sched_raw['weight_by'] == sched_raw['weight_by']].copy()


            total_costs_lev3['sorter'] = 999
            mask_tmp = total_costs_lev3['report_name'] == 'Op Cost General and Admin'
            total_costs_lev3.loc[mask_tmp, 'sorter'] = 128
            mask_tmp = total_costs_lev3['report_name'] == 'Op Cost Mining Operations'
            total_costs_lev3.loc[mask_tmp, 'sorter'] = 129
            mask_tmp = total_costs_lev3['report_name'] == 'Op Cost Stockpile Reclaim'
            total_costs_lev3.loc[mask_tmp, 'sorter'] = 130
            mask_tmp = total_costs_lev3['report_name'] == 'Op Cost Process'
            total_costs_lev3.loc[mask_tmp, 'sorter'] = 131
            mask_tmp = total_costs_lev3['report_name'] == 'Op Cost Environmental'
            total_costs_lev3.loc[mask_tmp, 'sorter'] = 132

            sched_fin_data = df.append(total_costs_lev3)


            sched_fin_data = sched_fin_data.append(total_sum)
            sched_fin_data = sched_fin_data.append(depreciate_mobile)
            sched_fin_data = sched_fin_data.append(depreciate_infra)
            sched_fin_data.fillna(value=0, inplace=True)


            tot_manning['sorter'] = 999
            mask_tmp = tot_manning['report_name'] == 'Manning Number General and Admin'
            tot_manning.loc[mask_tmp, 'sorter'] = 155
            mask_tmp = tot_manning['report_name'] == 'Manning Number Mining Operations'
            tot_manning.loc[mask_tmp, 'sorter'] = 156
            mask_tmp = tot_manning['report_name'] == 'Manning Number Stockpile Reclaim'
            tot_manning.loc[mask_tmp, 'sorter'] = 157
            mask_tmp = tot_manning['report_name'] == 'Manning Number Process'
            tot_manning.loc[mask_tmp, 'sorter'] = 158
            mask_tmp = tot_manning['report_name'] == 'Manning Number Environmental'
            tot_manning.loc[mask_tmp, 'sorter'] = 159

            sched_fin_data = sched_fin_data.append(tot_manning)


            sched_fin_data = sched_fin_data.append(total_manning_sum)
            sched_fin_data = sched_fin_data.append(fuel_dat)
            sched_fin_data = sched_fin_data.append(kw_dat)

            sched_fin_data.fillna(value=0, inplace=True)




            depreciation_existing = dep_am.loc['depreciate_existing']
            depreciation_existing = depreciation_existing[depreciation_existing.index <= calseries].copy()

            dep_exist_raw = depreciation_existing.copy()
            dep_exist_raw = dep_exist_raw / multi

            depreciation_existing = depreciation_existing.to_frame()
            depreciation_existing = depreciation_existing.T
            depreciation_existing = depreciation_existing.reset_index()

            depreciation_existing = depreciation_existing.rename(columns={'index': 'report_name'})

            depreciation_existing['report_mult'] = multi
            depreciation_existing['graph'] = 'f3'
            depreciation_existing.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            depreciation_existing = depreciation_existing/multi
            depreciation_existing.reset_index(inplace=True)
            depreciation_existing['sorter'] = 336
            sched_fin_data = sched_fin_data.append(depreciation_existing)

            amortise = dep_am.loc['amortisation']
            amortise = amortise[amortise.index <= calseries].copy()

            amortise_raw = amortise / multi
            amortise = amortise.to_frame()
            amortise = amortise.T
            amortise = amortise.reset_index()
            amortise = amortise.rename(columns={'index': 'report_name'})
            amortise['report_mult'] = multi
            amortise['graph'] = 'f3'
            amortise.set_index(['report_name', 'report_mult', 'graph'], inplace=True)
            amortise = amortise/multi
            amortise.reset_index(inplace=True)
            amortise['sorter'] = 338

            sched_fin_data = sched_fin_data.append(amortise)

            depreciate_total = dep_mobile_raw + dep_exist_raw + dep_infra_raw
            depreciate_total_raw = dep_mobile_raw + dep_exist_raw + dep_infra_raw

            depreciate_total_nominal = dep_mobile_raw_nominal + dep_exist_raw + dep_infra_raw_nominal
            depreciate_total_raw_nominal = dep_mobile_raw_nominal + dep_exist_raw + dep_infra_raw_nominal


            depdat = financial_me.adder(self,depreciate_total, 'Depreciation Total', 0, multi,337)
            sched_fin_data = sched_fin_data.append(depdat)

            depdat_nominal = financial_me.adder(self, depreciate_total_nominal, 'Depreciation Total Nom', 0, multi, 338)
            sched_fin_data = sched_fin_data.append(depdat_nominal)

            sched_report_units = read_hdf(hdfinputs, 'sched_report_units')
            sched_report_clean = read_hdf(hdfinputs, 'sched_report_clean')
            sched_report_sorter = read_hdf(hdfinputs, 'sched_report_raw')
            sched_report_sorter = sched_report_sorter[['sorter','report_name']]
            sched_report_sorter['looker_upper'] = sched_report_sorter['sorter'] + '_' + sched_report_sorter['report_name']
            sched_report_finner = read_hdf(hdfinputs, 'sched_report_clean_total')
            sched_report_finner['units'] = sched_report_units['report_units']
            sched_report_finner.drop(['graph'], axis=1, inplace=True)
            sched_report_finner['looker_upper'] = sched_report_sorter['looker_upper']
            sched_report_finner.set_index(['looker_upper','report_name', 'report_mult','units'], inplace=True)
            sched_report_finner.reset_index(inplace=True)
            sched_report_clean.set_index(['report_name'], inplace=True)
            sched_report_clean.drop(['report_mult'], axis=1,inplace=True)
            total_waste_sum = sched_report_clean.loc['Total Waste'].sum()
            total_waste = sched_report_clean.loc['Total Waste']
            total_ore = sched_report_clean.loc['Total Ore Processed']
            total_ore_sum = sched_report_clean.loc['Total Ore Processed'].sum()
            Total_ore_mined = sched_report_clean.loc['Ore Mined Ex Pit']
            Total_ore_mined_sum = sched_report_clean.loc['Ore Mined Ex Pit'].sum()
            strip_ratio = total_waste / total_ore
            strip_ratio_total = total_waste_sum / total_ore_sum


            oredat = financial_me.adder(self,total_ore, 'Ore Processed', 0, 1000000,349)
            sched_fin_data = sched_fin_data.append(oredat)

            wastedat = financial_me.adder(self,total_waste, 'Waste Mined', 0, 1000000,350)
            sched_fin_data = sched_fin_data.append(wastedat)

            strip_ratiodat = financial_me.adder(self,strip_ratio, 'Strip Ratio', 0, 1,351)
            sched_fin_data = sched_fin_data.append(strip_ratiodat)

            rev_data = sched_fin_data.copy()
            rev_data.drop(['report_mult', 'graph', 'sorter'], axis=1, inplace=True)
            rev_data.set_index(['report_name'], inplace=True)
            total_mining_cost = rev_data.loc['Op Cost Mining Operations'].sum()
            amortisation = rev_data.loc['amortisation'].sum()
            total_unit_cost = (total_mining_cost + amortisation) / (total_waste_sum + Total_ore_mined_sum)

            depreciation_existing_book = dep_am.loc['depreciate_existing_book']
            depreciation_existing_book = depreciation_existing_book[
                depreciation_existing_book.index <= calseries].copy()
            dep_exist_book = (depreciation_existing_book / 1000000) + (
                        (strip_ratio_total * Total_ore_mined) - total_waste)
            #dep_exist_book = (strip_ratio_total * rev_data.loc['Ore Processed']) - rev_data.loc['Waste Mined']
            dep_exist_value = dep_exist_book * total_unit_cost
            #mask = dep_exist_value < 0
            #dep_exist_value.loc[mask] = 0
            waste_amortisation = (Total_ore_mined / Total_ore_mined_sum) * dep_exist_value
            #waste_amortisation = dep_exist_value


            rev_data.loc['amortisation'] = rev_data.loc['amortisation'] + waste_amortisation
            net_rev = rev_data.loc['Net Rev IMC']
            net_rev_nominal = net_rev * real_nom

            #ebtr = (net_rev - rev_data.loc['Total Operating Cost']) - rev_data.loc['amortisation']
            t_op = rev_data.loc['Total Operating Cost']
            t_op *= opcost_factor
            ebtr_op_only = net_rev - t_op
            ebtr_op_only_nominal = ebtr_op_only * real_nom
            ebtr = net_rev - t_op - depreciate_total
            ebtr_nominal = ebtr_op_only_nominal - depreciate_total_nominal
            income_ratio = ebtr / net_rev
            income_ratio.fillna(value=0, inplace=True)
            royalty_list = []
            for index, row in income_ratio.iteritems():
                mask = (royalty['from'] < row) & (royalty['to'] > row)
                royalty_rate = royalty.loc[mask,'rate'].max()
                royalty_list.append(royalty_rate)
            rt = pd.Series(royalty_list)
            rt.index = rt.index + 1

            #royalty_annual =((ebtr) * 100 / (100 + (rt * 100))) * rt
            royalty_annual = (ebtr) * rt
            royalty_annual.fillna(value=0,inplace=True)
            royalty_annual_nominal = royalty_annual * real_nom
            tax_rate = dep_am.loc['corporate_tax']
            tax_rate = tax_rate[tax_rate.index <= calseries].copy()
            taxable_income = ebtr - amortise_raw - royalty_annual
            taxable_income_nominal = ebtr_nominal - amortise_raw - royalty_annual_nominal

            #taxable_income = ebtr - depreciate_total_raw - royalty_annual
            tax_paid = taxable_income * tax_rate
            tax_paid_nominal = taxable_income_nominal * tax_rate
            net_earnings = ebtr - amortise_raw - royalty_annual - tax_paid
            net_earnings_nominal = ebtr_nominal - amortise_raw - royalty_annual_nominal - tax_paid_nominal
            nom_real = 1 / real_nom
            #net_earnings = ebtr - depreciate_total_raw - royalty_annual - tax_paid
            net_earningsdat = financial_me.adder(self,net_earnings, 'Net Earnings After Dep Tax Royalty', 0, multi,347)
            net_earningsdat_nominal = financial_me.adder(self, net_earnings_nominal, 'Net Earnings After Dep Tax Royalty Nominal', 0, multi,
                                                 348)



            net_earnings = net_earnings.to_frame()
            net_earnings = net_earnings.T
            net_earnings_raw = net_earnings.copy()

            salvage_capital = read_hdf(hdffinin, 'fleet_salvaged_capital')
            salvage_capital = salvage_capital[['sched_period','salvage_value']]
            salvage_capital['sched_period'] = salvage_capital['sched_period'].astype(int)
            salvage_capital = salvage_capital.groupby(['sched_period'])['salvage_value'].sum()
            salvage_capital = (caldat * 0 + 1) * salvage_capital
            salvage_capital.fillna(value=0, inplace=True)

            disc_rate = fin_data.loc['discount_rate']['value']
            disc_rate += df_adjust
            import xnpv
            net_for_npv = net_earnings_raw + depreciate_total_raw + amortise_raw - cap_cost_sum - mobile_capital + salvage_capital
            net_for_npv_bd = net_earnings_nominal + depreciate_total_nominal

            net_for_npv_nom_real = net_for_npv_bd * nom_real + amortise_raw - cap_cost_sum - mobile_capital + salvage_capital
            net_for_npv_nom_real = net_for_npv_nom_real.to_frame().T

            """for all real"""

            net_earnings_cal = net_for_npv.T
            caldata = pd.Series(calendar_data)
            caldata.index = caldata.index + 1
            net_earnings_cal.index = caldata
            ne_series = pd.Series(net_earnings_cal[0].values)
            ne_s1 = ne_series.copy()
            mask = ne_s1.index < 1
            ne_s1 = ne_s1[mask]
            ne_s1.index = caldata[mask]
            ne_s1 = ne_s1 * 0
            ne_series.index = cals180
            ne_2018_series = ne_s1.append(ne_series)
            #ne_series.index = ne_series.index + 1
            #ne_series.index = caldata
            xnpvcalc = xnpv.XNPV()
            project_npv = xnpvcalc.xnpv_me(disc_rate, ne_2018_series)
            pnpv = pd.DataFrame(columns=['fin_data','value',])
            fin_npv_data = {'fin_data': 'XNPV', 'value': project_npv}
            pnpv = pnpv.append([fin_npv_data])
            print('Project NPV for srun ', srun, ' = ', project_npv)

            """for all nominal depreciation"""

            net_earnings_cal = net_for_npv_nom_real.T
            caldata = pd.Series(calendar_data)
            caldata.index = caldata.index + 1
            net_earnings_cal.index = caldata
            ne_series = pd.Series(net_earnings_cal[0].values)
            ne_s1 = ne_series.copy()
            mask = ne_s1.index < 1
            ne_s1 = ne_s1[mask]
            ne_s1.index = caldata[mask]
            ne_s1 = ne_s1 * 0
            ne_series.index = cals180
            ne_2018_series = ne_s1.append(ne_series)
            #ne_series.index = ne_series.index + 1
            #ne_series.index = caldata
            xnpvcalc = xnpv.XNPV()
            project_npv = xnpvcalc.xnpv_me(disc_rate, ne_2018_series)
            fin_npv_data = {'fin_data': 'XNPV_Dep_Nom', 'value': project_npv}
            pnpv = pnpv.append([fin_npv_data])
            print('Project NPV for srun ', srun, ' = ', project_npv)

            net_for_npv['report_mult'] = multi
            net_for_npv['graph'] = 'f4'
            net_for_npv['report_name'] = 'Net Earnings for NPV'
            net_for_npv['sorter'] = 348

            ebtrdat = financial_me.adder(self, ebtr, 'Royalty Income', 0, multi,339)
            sched_fin_data = sched_fin_data.append(ebtrdat)
            royaltydat = financial_me.adder(self,royalty_annual,'Royalty Paid',0,multi,340)
            sched_fin_data = sched_fin_data.append(royaltydat)
            taxincome = financial_me.adder(self, taxable_income, 'Taxable Income', 0, multi,341)
            sched_fin_data = sched_fin_data.append(taxincome)
            taxdat = financial_me.adder(self,tax_paid,'Tax Paid',0,multi,342)
            sched_fin_data = sched_fin_data.append(taxdat)

            capdat = financial_me.adder(self,cap_cost_sq,'Capital Cost Status Quo',0,multi,343)
            sched_fin_data = sched_fin_data.append(capdat)
            capdat = financial_me.adder(self,cap_cost_improve,'Capital Cost Improvement',0,multi,344)
            sched_fin_data = sched_fin_data.append(capdat)
            mobile_capdat = financial_me.adder(self,mobile_capital,'Capital Cost Fleet',0,multi,345)
            sched_fin_data = sched_fin_data.append(mobile_capdat)
            mobile_slvgdat = financial_me.adder(self, salvage_capital, 'Capital Salvage Income Fleet', 0, multi, 346)
            sched_fin_data = sched_fin_data.append(mobile_slvgdat)
            sched_fin_data = sched_fin_data.append(net_earningsdat)
            sched_fin_data = sched_fin_data.append(net_for_npv)
            sched_fin_data = sched_fin_data.sort_values(by=['sorter'])
            sched_fin_data_lup = sched_fin_data.reset_index()
            sched_fin_data_lup['sorter'] = sched_fin_data_lup['sorter'].astype(int)
            sched_fin_data_lup['looker_upper'] = sched_fin_data_lup['sorter'].astype(str) + '_' + sched_fin_data_lup[
                'report_name']
            sched_fin_data_lup = sched_fin_data_lup[['looker_upper']]
            sched_fin_data.drop(['sorter'], axis=1, inplace=True)
            sched_fin_data.reset_index(inplace=True)
            sched_fin_data.drop(['index'], axis=1, inplace=True)
            sched_fin_mcd = sched_fin_data.copy()

            fin_cols = list(sched_fin_mcd)
            sched_fin_data_temp = sched_fin_mcd.copy()
            for coldat in fin_cols:
                if coldat != 1:
                    sched_fin_data_temp.drop(coldat, axis=1, inplace=True)
                else:
                    break

            sched_fin_data_tot = sched_fin_data_temp.sum(axis=1)

            sched_fin_mcd['total'] = sched_fin_data_tot
            sched_fin_mcd.drop(['graph'], axis=1, inplace=True)
            sched_fin_mcd['looker_upper'] = sched_fin_data_lup['looker_upper']
            sched_report_mcd = sched_report_finner.append(sched_fin_mcd)
            sched_report_mcd.fillna(value='-',inplace=True)
            sched_report_mcd.set_index(['looker_upper','report_name','report_mult','units'],inplace=True)
            sched_report_mcd.reset_index(inplace=True)
            mask = sched_report_mcd['report_name'] == 'Strip Ratio'
            sched_report_mcd.loc[mask,'total'] = strip_ratio_total

            sched_fin_mcd.set_index(['looker_upper', 'report_name', 'report_mult', 'total'], inplace=True)
            sched_fin_mcd.reset_index(inplace=True)
            sched_fin_mcd.drop(['looker_upper'], axis=1, inplace=True)


            if srun == 1:

                sched_npver = sched_fin_mcd.drop(['report_mult', 'total'], axis=1)
                sched_npver.fillna(value=0,inplace=True)
                sched_npver.set_index(['report_name'], inplace=True)
                ilist = list(sched_npver.index)

                pnpc = pd.DataFrame(columns=['report_name', 'npv', ])
                for headme in ilist:
                    print(headme)
                    net_for_npv = sched_npver.loc[headme].to_frame()
                    net_earnings_cal = net_for_npv.copy()
                    caldata = pd.Series(calendar_data)
                    caldata.index = caldata.index + 1
                    net_earnings_cal.index = caldata
                    ne_series = pd.Series(net_earnings_cal[headme].values)
                    ne_s1 = ne_series.copy()
                    mask = ne_s1.index < 1
                    ne_s1 = ne_s1[mask]
                    ne_s1.index = caldata[mask]
                    ne_s1 = ne_s1 * 0
                    ne_series.index = cals180
                    ne_2018_series = ne_s1.append(ne_series)
                    #ne_series.index = ne_series.index + 1
                    #ne_series.index = caldata
                    xnpvcalc = xnpv.XNPV()
                    project_npc = xnpvcalc.xnpv_me(disc_rate, ne_2018_series)
                    fin_npv_data = {'report_name': headme, 'npv': project_npc}
                    pnpc = pnpc.append([fin_npv_data])

                hdfoutput.put('report_names_npv', pnpc)



            if srun < 4:
                hdfoutput.put('sched_report_fin_data', sched_fin_data)
                hdfoutput.put('NPV',pnpv)
                hdfoutput.put('sched_data_totals',sched_report_finner)
                hdfoutput.put('fin_data_totals', sched_fin_mcd)
                hdfoutput.put('mcd_data', sched_report_mcd)
                hdfoutput.flush()
                hdfoutput.close()
            else:
                npv_scenario = 'NPV_' + str(srun)
                hdfoutput.put(npv_scenario, pnpv)

            print('Project NPV for srun ',srun, ' = ', project_npv)

        hdfinputs.close()
        return

    def adder(self,seriesadd,reportnamefield,graphdat,multi,sorter):
        seriesadd = seriesadd.to_frame()
        seriesadd = seriesadd.T
        seriesadd = seriesadd.reset_index()
        seriesadd = seriesadd.rename(columns={'index': 'report_name'})
        seriesadd['report_mult'] = multi
        seriesadd['graph'] = graphdat
        seriesadd['report_name'] = reportnamefield
        seriesadd['sorter'] = sorter
        return seriesadd





if __name__ == '__main__':
    from pandas import read_hdf
    import pickle
    with open('cal_data.pkl', 'rb') as fin:
        callist_my = pickle.load(fin)
    capital_input = read_hdf('fin_in_data.h5','capital_input')
    fleet_capital_all = read_hdf('fin_in_data.h5', 'fleet_capital_all')
    tot_graph_heads = read_hdf('fin_in_data.h5', 'tot_graph_heads')

    finrunner = financial_me.financial(financial_me,capital_input, callist_my, fleet_capital_all, tot_graph_heads)
