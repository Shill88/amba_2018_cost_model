# -*- coding: utf-8 -*-
"""
Created on Sat May 27 09:46:51 2017

@author: slewis
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QFileDialog

class FileApp(QWidget):
 
    def __init__(self):
        super(FileApp,self).__init__()
        self.title = 'IMC File Dialog'
        self.left = 10
        self.top = 10
        self.width = 1000
        self.height = 600
        self.initUI()
        

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
 
    def openFileNameDialog(self,textdata,filetypes):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,textdata,"",filetypes, options=options)
        if fileName:

            return fileName

 
    def openFileNamesDialog(self):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self,"QFileDialog.getOpenFileNames()", "","All Files (*);;Python Files (*.py)", options=options)
        if files:
            print(files)
            
 
    def saveFileDialog(self,textdata,filetypes):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,textdata,"",filetypes, options=options)
        if fileName:
            #print(fileName)
            return fileName
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    fdata = FileApp()
    filedata = fdata.openFileNameDialog('Select MineMax CSV Export File or Type new name','HDF Files (*.h5)')
    sys.exit(app.exec_())
