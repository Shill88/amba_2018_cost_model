
#this version has the water,reagents and power added and the variable mech availability
#this is the same as rev5 but has schedule reporting in it which requires additional columns in the sched tab of the input spreadsheet.

import pandas as pd
import numpy as np
import pickle
import fleetcalcs_rev9 as fleetcalcs
import realisation_costs_rev11 as realisation_costs
import A_Config_Utils
import tkinter
from tkinter import messagebox
from pandas import HDFStore
import runcostmodel_rev11 as runcostmodel
import xnpv
import matplotlib.pyplot as plt
import grouped_weighted_average

xnpvcalc = xnpv.XNPV()

ConfigIni = A_Config_Utils.Read_Config()
pd.DataFrame.grouped_weighted_average = grouped_weighted_average

class lazyproperty:
    def __init__(self, func):
        self.func = func
    def __get__( self, instance, cls):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr( instance, self.func.__name__, value)
            return value

class CostModel():
    def __init__(self, init_data):

        self.init_data = init_data
        self.all_equipment = []
        self.all_ohead = []
        self.all_admin = []
        self.all_activities = []
        self.all_cost_objects = []
        self.all_rosters = []
        self.all_key_inputs = []

        hdfinputs = HDFStore('input_data.h5')

        if self.init_data != 0:

            file_in_xl = ConfigIni.read_config('cost_in.ini', 'costmod').get('file_in_xl')
            self.equip_file = pd.ExcelFile(file_in_xl)
            self.eqdata = self.equip_file.parse('Equip')
            self.exist_fleet_input = self.equip_file.parse('Exist_Fleet')
            self.roster_input = self.equip_file.parse('Rosters')
            self.labour_cost_input = self.equip_file.parse('Labour_Costs')
            self.sched_data = self.equip_file.parse('Sched')


            self.maintenance_curve = self.equip_file.parse('Maintenance_Curve')
            self.ma_curve = self.equip_file.parse('MA_Curve')
            self.cost_divisor = self.equip_file.parse('Grade_Tonnage')
            self.eq_activity = self.equip_file.parse('Eq_Activity')
            self.key_input_data = self.equip_file.parse('Key_Inputs')
            self.admin_input_data = self.equip_file.parse('Admin')
            self.ohead_input_data = self.equip_file.parse('OH_Charges')
            self.material_data = self.equip_file.parse('Material')
            self.learning_curve = self.equip_file.parse('Learning_Curve')
            self.cost_tree = self.equip_file.parse('cost_tree')
            self.cost_tree_finmod = self.equip_file.parse('cost_tree_finmod')
            self.capital_input = self.equip_file.parse('Capital')
            self.cost_tree = self.cost_tree[self.cost_tree.Name.notnull()]
            self.calendar = self.equip_file.parse('Calendar')
            self.calendar = self.calendar[self.calendar.Period.notnull()]
            self.calendar.set_index(['Period'],inplace=True)
            self.cost_tree_sort = self.cost_tree.reset_index()
            self.cost_tree_sort = self.cost_tree_sort.iloc[1:]
            self.cost_tree_sort = self.cost_tree_sort.reset_index()
            self.cost_tree_sort.columns.values[0] = 'sort_order'
            self.cost_tree_sort.columns.values[1] = 'Act_Id'

            self.cost_tree_finmod_sort = self.cost_tree_finmod.reset_index()
            self.cost_tree_finmod_sort = self.cost_tree_finmod_sort.iloc[1:]
            self.cost_tree_finmod_sort = self.cost_tree_finmod_sort.reset_index()
            self.cost_tree_finmod_sort.columns.values[0] = 'sort_order'
            self.cost_tree_finmod_sort.columns.values[1] = 'Act_Id'

            for index, row in self.cost_tree_sort.iterrows():
                try:
                    self.cost_tree_sort.loc[index, 'act_level'] = len(str(self.cost_tree_sort.loc[index, 'Act_Id']))
                except:
                    self.cost_tree_sort.loc[index, 'act_level'] = len(self.cost_tree_sort.loc[index, 'Act_Id'])

            hdftree = HDFStore('tree_data.h5')
            self.cost_tree_sort.set_index(['act_level'], inplace=True)
            self.cost_tree_sort.reset_index(inplace=True)
            cost_tree_sort_tmp = self.cost_tree_sort.drop('sort_order', 1)

            hdftree.put('cost_tree_sort', cost_tree_sort_tmp)
            hdftree.flush()

            for index, row in self.cost_tree_finmod_sort.iterrows():
                try:
                    self.cost_tree_finmod_sort.loc[index, 'act_level'] = len(str(self.cost_tree_finmod_sort.loc[index, 'Act_Id']))
                except:
                    self.cost_tree_finmod_sort.loc[index, 'act_level'] = len(self.cost_tree_finmod_sort.loc[index, 'Act_Id'])

            self.cost_tree_finmod_sort.set_index(['act_level'], inplace=True)
            self.cost_tree_finmod_sort.reset_index(inplace=True)
            cost_tree_finmod_sort_tmp = self.cost_tree_finmod_sort.drop('sort_order', 1)

            hdftree.put('cost_tree_finmod_sort', cost_tree_finmod_sort_tmp)
            hdftree.flush()
            hdftree.close()

            self.eq_activity.columns = [x.lower() for x in self.eq_activity.columns]
            self.cost_divisor.columns = [x.lower() for x in self.cost_divisor.columns]
            self.key_input_data.columns = [x.lower() for x in self.key_input_data.columns]
            self.eqdata.columns = [x.lower() for x in self.eqdata.columns]
            self.roster_input.columns = [x.lower() for x in self.roster_input.columns]
            self.labour_cost_input.columns = [x.lower() for x in self.labour_cost_input.columns]
            self.maintenance_curve.columns = [x.lower() for x in self.maintenance_curve.columns]
            self.ma_curve.columns = [x.lower() for x in self.ma_curve.columns]
            self.material_data.columns = [x.lower() for x in self.material_data.columns]

            self.eq_activity = self.eq_activity[self.eq_activity.activity.notnull()]
            self.key_input_data = self.key_input_data[self.key_input_data.variable.notnull()]
            self.eqdata = self.eqdata[self.eqdata.equipment.notnull()]
            self.roster_input = self.roster_input[self.roster_input.type.notnull()]
            self.labour_cost_input = self.labour_cost_input[self.labour_cost_input.type.notnull()]
            self.maintenance_curve = self.maintenance_curve[self.maintenance_curve.equipment.notnull()]
            self.ma_curve = self.ma_curve[self.ma_curve.equipment.notnull()]
            self.material_data = self.material_data[self.material_data.material.notnull()]

            import re

            for index, row in self.key_input_data.iterrows():
                self.key_input_data.loc[index, 'variable'] = re.sub('\W+', '_',
                                                                    self.key_input_data.loc[index, 'variable'])

            for index, row in self.eqdata.iterrows():
                self.eqdata.loc[index, 'equipment'] = re.sub('\W+', '_', self.eqdata.loc[index, 'equipment'])

            for index, row in self.admin_input_data.iterrows():
                self.admin_input_data.loc[index, 'staff_category'] = re.sub('\W+', '_', self.admin_input_data.loc[
                    index, 'staff_category'])

            for index, row in self.ohead_input_data.iterrows():
                self.ohead_input_data.loc[index, 'ohead_category'] = re.sub('\W+', '_', self.ohead_input_data.loc[
                    index, 'ohead_category'])

            for index, row in self.capital_input.iterrows():
                self.capital_input.loc[index, 'Item'] = re.sub('\W+', '_', self.capital_input.loc[
                    index, 'Item'])

            self.eqdata.set_index('equipment', inplace=True)

            self.labour_cost_input.set_index(['type'],inplace=True)
            self.exist_fleet_input.set_index('Equipment', inplace=True)
            self.maintenance_curve.set_index('equipment', inplace=True)
            self.ma_curve.set_index('equipment', inplace=True)
            self.key_input_data.set_index('variable',inplace=True)
            self.eq_activity.set_index('equipment',inplace=True)
            self.material_data.set_index('material',inplace=True)
            self.roster_input.set_index(['type'], inplace=True)
            self.learning_curve.set_index(['L_Curve'], inplace=True)
            self.capital_input.set_index(['Item'], inplace=True)

            self.sched_data.reset_index(inplace=True)
            self.sched_data.set_index(['row_ref'],inplace=True)
            self.sched_data.drop('index', axis=1, inplace=True)
            self.sched_data_raw = self.sched_data.copy()
            self.sched_report = self.sched_data[self.sched_data.report_material.notnull()]
            sched_cols = list(self.sched_data)

            for coldat in sched_cols:
                if coldat != 1:
                    self.sched_data.drop(coldat, axis=1, inplace=True)
                else:
                    break

            self.sched_report.reset_index(inplace=True)
            self.sched_report.set_index(['row_ref','report_material','report_name','report_units','field_mult','report_mult','weight_by','report_sort','graph'],inplace=True)
            sched_cols = list(self.sched_report)

            for coldat in sched_cols:
                if coldat != 1:
                    self.sched_report.drop(coldat, axis=1, inplace=True)
                else:
                    break

            self.sched_report.reset_index(inplace=True)
            self.sched_report = self.sched_report.fillna(0)
            mask = self.sched_report['report_sort'].str[0] == 'H'
            tempdat = self.sched_report[mask].copy()
            tempdat = tempdat[['report_material','report_sort']]
            tempdat['sorter'] = tempdat['report_sort'].str[1] + tempdat['report_sort'].str[2] + '_' + tempdat['report_material']
            tempdat.drop('report_sort', axis=1, inplace=True)
            self.sched_report.set_index(['report_material'],inplace=True)
            tempdat.set_index(['report_material'],inplace=True)
            self.sched_report['sorter'] = tempdat['sorter']
            self.sched_report.reset_index(inplace=True)
            self.sched_report.set_index(['sorter'], inplace=True)
            self.sched_report.reset_index(inplace=True)
            self.sched_report.loc[mask, 'report_sort'] = 0
            self.sched_report.sort_values(by=['sorter','report_sort'],inplace=True)
            self.sched_report_raw = self.sched_report.copy()

            sched_report_temp = self.sched_report.copy()
            sched_report_temp = sched_report_temp.reset_index().drop('index',axis=1)
            sched_cols = list(sched_report_temp)

            for coldat in sched_cols:
                if coldat != 1:
                    sched_report_temp.drop(coldat, axis=1, inplace=True)
                else:
                    break

            field_mult = self.sched_report['field_mult'].reset_index().drop('index',axis=1)
            field_mult = pd.Series(field_mult['field_mult'].values)
            sched_report_temp = sched_report_temp.multiply(field_mult, axis=0)
            report_mult = self.sched_report['report_mult'].reset_index().drop('index',axis=1)
            report_mult = pd.Series(report_mult['report_mult'].values)
            sched_report_temp = sched_report_temp.divide(report_mult, axis=0)

            self.sched_report_raw = self.sched_report_raw.reset_index().drop('index', axis=1)
            self.sched_report_clean = self.sched_report_raw[['report_name', 'report_mult','graph']]
            self.sched_report_units = self.sched_report_raw[['report_name','report_units']]
            hdfinputs.put('sched_report_units', self.sched_report_units)
            self.sched_report_raw.update(sched_report_temp)
            self.sched_report_raw['report_mult'] = self.sched_report_raw['report_mult']
            self.sched_report_clean = self.sched_report_clean.join(sched_report_temp)
            self.sched_report_clean['report_mult'] = self.sched_report_clean['report_mult']

            try:
                self.sched_report_fin_all = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[0]
                hdfinputs.put('sched_report_fin_all', self.sched_report_fin_all)
                self.sched_report_fin_data = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[1]
                hdfinputs.put('sched_report_fin_data', self.sched_report_fin_data)
                self.sched_report_fin_all_cs = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[2]
                hdfinputs.put('sched_report_fin_all_cs', self.sched_report_fin_all_cs)
                self.sched_report_fin_data_cs = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[3]
                hdfinputs.put('sched_report_fin_data_cs', self.sched_report_fin_data_cs)
                self.sched_report_fin_all_cs = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[4]
                hdfinputs.put('sched_report_fin_all_sq', self.sched_report_fin_all_cs)
                self.sched_report_fin_data_sq = realisation_costs.realise_me.sellme(self,self.sched_report_clean)[5]
                hdfinputs.put('sched_report_fin_data_sq', self.sched_report_fin_data_sq)
            except:
                pass

            try:
                mask_graph_all = self.sched_report_clean['graph'] != 0
                self.graph_data = self.sched_report_clean[mask_graph_all].copy()
                mask_graphs1 = self.graph_data['graph'].str.contains('s1')
                self.sched_graph_stack1 = self.graph_data[mask_graphs1].copy()
                self.sched_graph_stack1 = self.sched_graph_stack1.drop(['report_mult','graph'], axis=1)
                self.sched_graph_stack1 = self.sched_graph_stack1.set_index(['report_name'])
                self.sched_graph_stack1.index.rename('Material',inplace=True)
                self.sched_graph_stack1 = self.sched_graph_stack1.T
                #fig, axes = plt.subplots(nrows=2, ncols=2)
                #ax = self.sched_graph_stack1.plot(ax=axes[0,0],kind='bar',figsize = (18,9),stacked=True,fontsize = 6,title = 'Total Mined Material')
                #ax.set(xlabel = "Period", ylabel = "Million Tonnes")

                mask_graphs2 = self.graph_data['graph'].str.contains('s2')
                self.sched_graph_stack2 = self.graph_data[mask_graphs2].copy()
                self.sched_graph_stack2 = self.sched_graph_stack2.drop(['report_mult','graph'], axis=1)
                self.sched_graph_stack2 = self.sched_graph_stack2.set_index(['report_name'])
                self.sched_graph_stack2.index.rename('Material',inplace=True)
                self.sched_graph_stack2 = self.sched_graph_stack2.T
                #ax1 = self.sched_graph_stack2.plot(ax=axes[0,1],kind='bar',figsize = (18,9),stacked=True,fontsize = 6,title = 'Ore Mined')
                #ax1.set(xlabel = "Period", ylabel = "Million Tonnes")

                mask_graphs3 = self.graph_data['graph'].str.contains('s3')
                self.sched_graph_stack3 = self.graph_data[mask_graphs3].copy()
                self.sched_graph_stack3 = self.sched_graph_stack3.drop(['report_mult','graph'], axis=1)
                self.sched_graph_stack3 = self.sched_graph_stack3.set_index(['report_name'])
                self.sched_graph_stack3.index.rename('Material',inplace=True)
                self.sched_graph_stack3 = self.sched_graph_stack3.T
                #ax2 = self.sched_graph_stack3.plot(ax=axes[1,0],kind='bar',figsize = (18,9),stacked=True,fontsize = 6,title = 'Ore Processed')
                #ax2.set(xlabel = "Period", ylabel = "Million Tonnes")

                mask_graphs4 = self.graph_data['graph'].str.contains('s4')
                self.sched_graph_stack4 = self.graph_data[mask_graphs4].copy()
                self.sched_graph_stack4 = self.sched_graph_stack4.drop(['report_mult','graph'], axis=1)
                self.sched_graph_stack4 = self.sched_graph_stack4.set_index(['report_name'])
                self.sched_graph_stack4.index.rename('Material',inplace=True)
                self.sched_graph_stack4 = self.sched_graph_stack4.T
                #ax3 = self.sched_graph_stack4.plot(ax=axes[1,1],kind='bar',figsize = (18,9),stacked=True,fontsize = 6,title = 'Total Material Moved')
                #ax3.set(xlabel = "Period", ylabel = "Million Tonnes")
            except:
                pass

            try:

                self.sched_report.reset_index(inplace=True)
                self.sched_report = self.sched_report.drop(['index'], axis=1)

                self.sched_report_clean_total = self.sched_report_clean.copy()


                self.sched_report_grades = self.sched_report[self.sched_report['weight_by'] > 0].copy()
                self.sched_report_sums = self.sched_report[self.sched_report['weight_by'] == 0].copy()
                self.sched_report_mults = self.sched_report[['field_mult','report_mult']].copy()
                mults = self.sched_report_mults['field_mult'] / self.sched_report_mults['report_mult']



                sched_cols = list(self.sched_report_sums)

                for coldat in sched_cols:
                    if coldat != 1:
                        self.sched_report_sums.drop(coldat, axis=1, inplace=True)
                    else:
                        break

                #sum_field_mult = self.sched_report_sums.multiply(self.sched_report_sum_mults['field_mult'], axis=0)
                #sum_report_mult = sum_field_mult.divide(self.sched_report_sum_mults['report_mult'], axis=0)
                #sums_total = sum_report_mult.sum(axis=1)
                #sums_total = sums_total.rename("total")
                #self.sched_report = pd.concat([self.sched_report,sums_total],axis = 1)


                sums_total = self.sched_report_sums.sum(axis=1)
                sums_total = sums_total.rename("total")
                self.sched_report = pd.concat([self.sched_report,sums_total],axis = 1)


                period_dat = list(self.calendar)

                for i, row in self.sched_report_grades.iterrows():
                    tonnes = self.sched_report.loc[self.sched_report['row_ref'] == int(row.weight_by)].T
                    ydat = tonnes.index.isin(period_dat)
                    t_year = tonnes[ydat]
                    row_period = row[row.index.isin(period_dat)]
                    pmetal = t_year.multiply(row_period,axis = 0)
                    tmetal = pmetal.sum().max()
                    try:
                        gdat = tmetal / tonnes.loc['total'].sum()
                    except:
                        gdat = 0

                    mask = self.sched_report['row_ref'] == row.row_ref

                    self.sched_report.loc[mask,'total'] = gdat

                self.sched_report.fillna(value=0,inplace=True)
                self.sched_report_clean_total['total'] = self.sched_report['total'] * mults

                list_heads = ['report_name','report_mult','graph','total']
                list_heads2 = list_heads + period_dat
                self.sched_report_clean_total = self.sched_report_clean_total[list_heads2]
            except:
                pass


            self.sched_report_clean = self.sched_report_clean.drop('graph', axis=1)

            hdfinputs.put('sched_report_raw', self.sched_report_raw)
            hdfinputs.put('sched_report_clean', self.sched_report_clean)
            hdfinputs.put('sched_report_clean_total', self.sched_report_clean_total)
            hdfinputs.put('sched_graph_stack1', self.sched_graph_stack1)
            hdfinputs.put('sched_graph_stack2', self.sched_graph_stack2)
            hdfinputs.put('sched_graph_stack3', self.sched_graph_stack3)
            hdfinputs.put('sched_graph_stack4', self.sched_graph_stack4)

            hdfinputs.flush()
            hdfinputs.close()



            self.sched_data_tree = self.sched_data

            self.sched_data = self.sched_data.apply(pd.to_numeric,errors='coerce')

            self.sched_data = self.sched_data.fillna(0)

            self.mcurve_cols = self.maintenance_curve.columns.values.tolist()
            self.ma_curve_cols = self.ma_curve.columns.values.tolist()

            self.admin_input_data.set_index(['staff_category'], inplace=True)

            self.cost_tree.reset_index(inplace = True)
            self.cost_tree = self.cost_tree.dropna().set_index(['index'])
            self.admin_yr_list = []

            for value in (self.admin_input_data.columns.values.tolist()):
                try:
                    self.admin_yr_list.append(int(value))
                except ValueError:
                    continue

            self.admin_years = self.admin_input_data[self.admin_yr_list]
            self.ohead_input_data.set_index(['ohead_category'], inplace=True)
            self.ohead_yr_list = []

            for value in (self.ohead_input_data.columns.values.tolist()):
                try:
                    self.ohead_yr_list.append(int(value))
                except ValueError:
                    continue
            self.ohead_years = self.ohead_input_data[self.ohead_yr_list]

        if init_data != 0:
            self.input_data = Input_Data(self)
            input_data_file = self.input_data
        else:
            try:
                with open('input_data.pkl', 'rb') as fin:
                    self.input_data = pickle.load(fin)
            except:
                print("data not initialised - re-initialise")

        self.sched = self.input_data.sched_data
        self.sched_periods = self.input_data.calendar.shape[1]
        self.keyinputs = Key_Inputs(self)
        self.eqcols = self.input_data.eqdata.columns.values.tolist()

        self.scheddiv = pd.DataFrame()
        self.dcfdata = pd.DataFrame()
        for iddiv, divi in self.cost_divisor.iterrows():
            sched_calc_tmp = 'sched.loc[' + str(int(self.cost_divisor.loc[iddiv,'sched_row'])) + ']'
            scheddivadd = 'self.' + sched_calc_tmp
            ton_units = self.key_input_data.loc['tonnage_units_sched','value']
            div = eval(scheddivadd) * ton_units
            unit = self.cost_divisor.loc[iddiv,'name']
            period_data = self.calendar.loc['month_year']

            datesin = pd.to_datetime(period_data)
            cflow = pd.Series(div.values, index=datesin)
            ratein = self.keyinputs.discount_rate
            xnpv = xnpvcalc.xnpv_me(ratein, cflow)
            dcf_data = {'unit': unit, 'xnpv': xnpv}
            self.dcfdata = self.dcfdata.append([dcf_data])
            div['unit'] = unit
            mask = div == 0
            div[mask] = 100000000

            self.scheddiv = self.scheddiv.append(div)

        self.scheddiv.reset_index(inplace=True)
        self.scheddiv.set_index(['unit'],inplace=True)
        self.scheddiv.drop('index', axis=1, inplace=True)

        '''Write DCF Data to Data file'''
        hdf = HDFStore('data.h5')
        hdf.put('dcf_ton', self.dcfdata)
        hdf.flush()
        hdf.close()

        hdfinputs = HDFStore('input_data.h5')

        hdfinputs.put('sched_data_raw', self.sched_data_raw)
        hdfinputs.put('key_input_data', self.key_input_data)
        hdfinputs.put('sched_data', self.sched_data)
        hdfinputs.put('capital_data', self.capital_input)

        hdfinputs.flush()
        hdfinputs.close()

        # Populate Rosters

        for index, row in self.input_data.roster_input.iterrows():
            self.rname = index.lower()

            self.hrs_shift = row['hrs_shift']
            self.shifts_day = row['shifts_day']
            self.panels = row['panels']
            self.days_roster = row['days_roster']
            self.days_worked = row['days_worked']
            self.public_holidays = row['public_holidays']
            self.equip_non_work = row['equip_non_work']
            self.absent = row['absent']
            self.unpaid_days_shut = row['unpaid_days_shut']

            self.annual_leave = row['annual_leave']
            self.sick_leave = row['sick_leave']
            self.crib = row['crib']
            self.smoko = row['smoko']
            self.work_efficiency = row['work_efficiency']
            self.de_rate_days = row['de_rate_days']
            self.wet_weather = row['wet_weather']
            self.planned_onshift_mtc = row['planned_onshift_mtc']
            self.planned_offshift_mtc = row['planned_offshift_mtc']

            self.rostered_onshift_mtc = row['rostered_onshift_mtc']
            self.standby = row['standby']
            self.bdown = row['bdown']
            self.service_lube = row['service_lube']
            self.awaiting_trucks = row['awaiting_trucks']
            self.awaiting_other_plant = row['awaiting_other_plant']
            self.re_locating = row['re_locating']
            self.clean_up = row['clean_up']
            self.shift_change = row['shift_change']
            self.blasting_other = row['blasting_other']
            self.index_low = index.lower()
            vars()[self.index_low] = Roster(self, self.rname, self.hrs_shift, self.shifts_day, self.panels, self.days_roster, self.days_worked, self.unpaid_days_shut, self.public_holidays, self.equip_non_work, self.absent,
                                            self.annual_leave,
                                            self.sick_leave, self.crib, self.smoko, self.work_efficiency, self.de_rate_days, self.wet_weather, self.planned_onshift_mtc,
                                            self.planned_offshift_mtc, self.rostered_onshift_mtc, self.standby, self.bdown, self.service_lube,
                                            self.awaiting_trucks, self.awaiting_other_plant, self.re_locating, self.clean_up, self.shift_change, self.blasting_other)
            setattr(self, self.index_low, eval(self.index_low))

        #build fleet

        mask = self.input_data.eqdata['selected'] == True
        self.active_eqdata = self.input_data.eqdata[mask]
        for index, row in self.active_eqdata.iterrows():
            vars()[index] = Equip(self)
            curr_equip = eval(index)
            curr_equip.etype = index
            setattr(self, index, eval(index))
            for coldata in self.eqcols:
                if coldata == 'op_roster':
                    roster_id = row[coldata].lower()
                if coldata == 'trade_roster':
                    troster_id = row[coldata].lower()

                setattr(eval("self." + index), coldata, row[coldata])

                if coldata == 'capital_cost':
                    if curr_equip.cost_basis != 'Contractor_Cplus':
                        if curr_equip.cost_basis[0] == 'C':
                            curr_equip.capital_cost = 0

            try:
                setattr(eval(index),"o_roster", eval(roster_id))
                setattr(eval(roster_id), curr_equip.etype, eval(index))
            except:
                print("error - The Roster: ", roster_id, " has not been created yet")
            try:
                setattr(eval(index), "t_roster", eval(troster_id))

            except:
                print("error - The Roster: ", troster_id, " has not been created yet")

        self.excols = self.input_data.exist_fleet_input.columns.values.tolist()


        for index, row in self.input_data.exist_fleet_input.iterrows():
            try:
                self.curr_equip = eval(index)
                for coldata in self.excols:
                    if row[coldata] == row[coldata]:
                        self.curr_equip.exist_fleet.append(Fleet(self, row[coldata], self.curr_equip.life, 0, 9999, True, 0, 0))
            except:
                print("In Existing Fleet No Record of: ", index)

        # build admin
        for index, row in self.input_data.admin_input_data.iterrows():
            vars()[index] = Admin(self)
            curr_admin = eval(index)
            curr_admin.aname = index
            setattr(self, index, eval(index))
            curr_admin.unit_cost = row['cost']
            curr_admin.staff_number = self.input_data.admin_years.loc[index,:]

            curr_admin.act_tree.append(Admin_Act(self, index,curr_admin.staff_number,row['act_id'],row['act_name'],curr_admin.unit_cost,curr_admin.cost_period))

            curr_admin.cost = curr_admin.cost_period[['sched_period','cost']]
            curr_admin.cost.set_index(['sched_period'],inplace=True)

            for udiv, rowdiv in self.scheddiv.iterrows():
                divdat = self.scheddiv.loc[udiv]
                divdat = divdat.reset_index()
                divdat.set_index(['index'], inplace=True)
                divname = 'cost_per_' + udiv
                unit_cost = curr_admin.cost['cost'] / divdat[udiv]
                setattr(curr_admin, divname, unit_cost)
                setattr(curr_admin, 'tot_cost', curr_admin.cost['cost'])


            for admin in curr_admin.act_tree:
                admin.cost = admin.cost_period[['sched_period', 'cost']]
                admin.cost.set_index(['sched_period'], inplace=True)
                for udiv, rowdiv in self.scheddiv.iterrows():
                    divdat = self.scheddiv.loc[udiv]
                    divdat = divdat.reset_index()
                    divdat.set_index(['index'], inplace=True)
                    divname = 'cost_per_' + udiv
                    unit_cost = admin.cost['cost'] / divdat[udiv]
                    setattr(admin, divname, unit_cost)
                    setattr(admin, 'tot_cost', admin.cost['cost'])

        # build ohead
        for index, row in self.input_data.ohead_input_data.iterrows():
            print(index)
            vars()[index] = Ohead(self)
            curr_ohead = eval(index)
            curr_ohead.ohead_name = index
            setattr(self, index, eval(index))
            curr_ohead.an_cost = self.input_data.ohead_years.loc[index,:]
            curr_ohead.act_tree.append(Ohead_Act(self,index,row['act_id'],row['act_name'],curr_ohead.cost_period))

            curr_ohead.cost = curr_ohead.cost_period[['sched_period','cost']]
            curr_ohead.cost.set_index(['sched_period'],inplace=True)

            for udiv, rowdiv in self.scheddiv.iterrows():
                divdat = self.scheddiv.loc[udiv]
                divdat = divdat.reset_index()
                divdat.set_index(['index'], inplace=True)
                divname = 'cost_per_' + udiv
                unit_cost = curr_ohead.cost['cost'] / divdat[udiv]
                setattr(curr_ohead, divname, unit_cost)
                setattr(curr_ohead, 'tot_cost', curr_ohead.cost['cost'])


            for ohead in curr_ohead.act_tree:
                ohead.cost = ohead.cost_period[['sched_period', 'cost']]
                ohead.cost.set_index(['sched_period'], inplace=True)
                for udiv, rowdiv in self.scheddiv.iterrows():
                    divdat = self.scheddiv.loc[udiv]
                    divdat = divdat.reset_index()
                    divdat.set_index(['index'], inplace=True)
                    divname = 'cost_per_' + udiv
                    unit_cost = ohead.cost['cost'] / divdat[udiv]
                    setattr(ohead, divname, unit_cost)
                    setattr(ohead, 'tot_cost', ohead.cost['cost'])


        """"build activity"""

        for index, row in self.input_data.eq_activity.iterrows():
            curr_equip = eval(index)
            try:
                curr_equip.act_tree.append(
                    Eq_Act(self, index, curr_equip.o_roster.operator_ratio, curr_equip.period_op_hours, curr_equip.capacity, row['activity'], row['act_tree'], row['hour_basis'], row['unit_number'],
                           row['utilisation_if_unit'], row['prodrate_if_rate'], row['sched_row_proportion'],row['sched_row'], row['sched_dt_wt'],
                           row['material'], row['l_curve'], row['bucket_fill_factor'], row['carryback_perc'], row['single_side_loading_perc'], row['truck_loaded'], row['spot_time'], row['first_pass'], row['other_passes'], row['truck_presentation']))
            except:
                print("Eq_Id Missing data for: ",index)
                root = tkinter.Tk()
                root.withdraw()
                messagebox.showwarning("Warning", "Missing Activity Data for " + index + " Check the Eq_Activity in Spreadhsheet - spot times, truck loaded etc")


        if self.init_data == 2:
            runner = runcostmodel.Build_Tree_Dat.update_treedat(self)

    def __del__(self):
        class_name = self.__class__.__name__
        print (class_name, "destroyed")

    def recalc_fleet(self):
        for row in CostModel.all_cost_objects:
            row.fleet_data
            del row.fleet_data
            row.fleet_data

    def calc_vcost(self):
        for row in CostModel.all_cost_objects:
            row.variable_op_cost
            del row.variable_op_cost
            row.variable_op_cost

    def mtc_factor(self,cls,used_hrs):
        etype = cls.etype
        for coldata in self.input_data.mcurve_cols:
            hrs_targ = float(coldata[1:])
            if used_hrs < hrs_targ:
               try:
                   self.mcurve_data = self.input_data.maintenance_curve.loc[etype, coldata].max()
               except:
                   self.mcurve_data = 1
               return self.mcurve_data

    def ma_factor(self,cls,used_hrs):
        etype = cls.etype
        for coldata in self.input_data.ma_curve_cols:
            hrs_targ = float(coldata[1:])
            if used_hrs < hrs_targ:
               try:
                   self.ma_curve_data = self.input_data.ma_curve.loc[etype, coldata].max()
               except:
                   self.ma_curve_data = 1
               return self.ma_curve_data


    def labour_cost(self,labour_cat):
        month_basic = self.input_data.labour_cost_input.loc[labour_cat, 'basic_salary_month']
        on_costs = self.input_data.labour_cost_input.loc[labour_cat, 'on_costs']
        # TODO fix this up so it links to labour period input
        return (month_basic + on_costs) * 12

    def fleet_module(self,cls):
        self.curr_equip = cls
        etype = cls.etype
        fpass = 0
        fleet_hrs = sum(fleet.req_hrs for fleet in self.curr_equip.act_tree)
        unit_req = np.rint(sum(fleet.unit_number for fleet in self.curr_equip.act_tree if (fleet.calc_method == 'count') or (fleet.calc_method == 'count_variable')))
        eq_count_based_unit = 0
        eq_count_hrs_units = 0
        for fleet in self.curr_equip.act_tree:
            if (fleet.calc_method == 'count') or (fleet.calc_method == 'count_variable'):
                eq_count_based_unit += fleet.unit_number
                eq_count_hrs_units += fleet.req_hrs

        data = {'Eq_Id': etype, 'equip_num': fpass, 'rem_hrs': self.curr_equip.life, 'used_hrs': 0,
                'start': 1, 'finish': 9999, 'period_hours': self.curr_equip.period_op_hours , 'active': True,
                'allocated_hrs': 0, 'sched_period': 0}
        total_life = self.curr_equip.life

        fleet_data = pd.DataFrame([data])
        try:
            for row in cls.exist_fleet:
                data = {'Eq_Id': etype, 'equip_num': fpass, 'rem_hrs': row.rem_hrs, 'used_hrs': row.used_hrs,
                        'start': row.start, 'finish': row.finish, 'period_hours': self.curr_equip.period_op_hours ,
                        'active': row.active, 'allocated_hrs': 0, 'sched_period': row.year}
                if fpass == 0:
                    fleet_data = pd.DataFrame([data])
                    total_life = row.life
                    fpass += 1
                else:
                    fpass += 1
                    fleet_temp = pd.DataFrame([data])
                    fleet_data = fleet_data.append(fleet_temp)
        except:
            pass

        fleet_data.reset_index(inplace=True)
        fleet_data.drop('index', axis=1, inplace=True)

        try:
            fleet_min_nums = self.curr_equip.min_fleet
        except:
            ydata = {'1': 0}

            fleet_min_nums = pd.DataFrame([ydata])

        fleetme = fleetcalcs.Fleet_calcs(self)
        fleetme.fleetcalcs(fleet_data, self.keyinputs.period_name,fleet_min_nums, fleet_hrs, total_life, etype, unit_req,cls.act_tree,eq_count_based_unit,eq_count_hrs_units)
        fleet_data_run_all = fleetme.fleet_data_run
        fleet_data_run_all.reset_index(inplace=True)
        fleet_data_run_all.drop('index', axis=1, inplace=True)
        fleet_data_run = fleet_data_run_all[fleet_data_run_all.sched_period != 0].copy()
        fleet_data_run['operator_category'] = self.curr_equip.operator_category

        self.curr_equip.total_req_hrs = sum(fleet.req_hrs for fleet in self.curr_equip.act_tree)

        self.curr_equip.operator_number_count_based = fleet_data_run.groupby(['sched_period'])['operator_number'].sum() * 0 + (self.curr_equip.count_unit_number * self.curr_equip.o_roster.operator_ratio * self.curr_equip.operator_allocation)

        self.curr_equip.operator_number_total = fleet_data_run.groupby(['sched_period'])['operator_number'].sum()

        self.curr_equip.operator_number_hours_based = self.curr_equip.operator_number_total - self.curr_equip.operator_number_count_based

        self.curr_equip.req_hours_hr_based = sum(fleet.req_hrs for fleet in self.curr_equip.act_tree if fleet.calc_method != 'count')

        for index, rowdat in fleet_data_run.iterrows():
            maint_factor = self.mtc_factor(self.curr_equip, rowdat['used_hrs'])
            fleet_data_run.loc[index,'variable_cost'] = fleet_data_run.loc[index,'allocated_hrs'] * maint_factor * self.curr_equip.variable_costs
            fleet_data_run.loc[index,'fixed_cost'] = self.curr_equip.fixed_costs * fleet_data_run.loc[index,'allocated_hrs']
            fleet_data_run.loc[index,'mtc_fact'] = maint_factor
            fleet_data_run.loc[index, 'ownership_cost'] = self.curr_equip.ownership_cost * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'fuel_litres'] = self.curr_equip.fuel_cons * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'kw_hrs'] = self.curr_equip.kw * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'water'] = self.curr_equip.water * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'reagents'] = self.curr_equip.reagents * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'contract_cost'] = self.curr_equip.contract_cost * fleet_data_run.loc[index, 'allocated_hrs']
            fleet_data_run.loc[index, 'cost'] = fleet_data_run.loc[index,'variable_cost'] + fleet_data_run.loc[index,'fixed_cost'] +  fleet_data_run.loc[index,'operator_cost'] +  fleet_data_run.loc[index,'tradesmen_cost']
            fleet_data_run.loc[index, 'cost_no_op'] = fleet_data_run.loc[index,'variable_cost'] + fleet_data_run.loc[index,'fixed_cost'] + fleet_data_run.loc[index,'tradesmen_cost']

        for udiv, rowdiv in self.scheddiv.iterrows():
            divdat = self.scheddiv.loc[udiv]
            divdat = divdat.reset_index()
            divdat.set_index(['index'],inplace=True)
            divname = 'cost_per_' + udiv
            fleet_data_run[divname] = fleet_data_run['cost'] / divdat[udiv]
            fleet_data_run['tot_cost'] = fleet_data_run['cost']

        self.curr_equip.operator_cost_total = fleet_data_run.groupby(['sched_period'])['operator_cost'].sum()

        self.curr_equip.total_cost_no_op = fleet_data_run.groupby(['sched_period'])['cost_no_op'].sum()
        self.curr_equip.total_cost = self.curr_equip.operator_cost_total + self.curr_equip.total_cost_no_op

        for fleet in self.curr_equip.act_tree:
            cmethod = fleet.calc_method[:5]
            if cmethod != 'count':
                try:
                    fleet.operator_number = fleet.req_hrs / self.curr_equip.req_hours_hr_based * self.curr_equip.operator_number_hours_based
                except:
                    fleet.operator_number = 0

            if cmethod == 'count':
                try:
                    fleet.operator_number = fleet.req_hrs * 0 + (fleet.unit_number * fleet.operator_ratio * self.curr_equip.operator_allocation)

                except:
                    messagebox.showwarning("Warning", "You have allocated " + etype + " as Count but dont have the fleet data - fix input spreadsheet in Eq_Activity sheet")

            """
            if self.curr_equip.operator_number_total.loc[1] > 0:
                fleet.operator_cost = fleet.operator_number / self.curr_equip.operator_number_total * self.curr_equip.operator_cost_total
            else:
                fleet.operator_cost = self.curr_equip.operator_cost_total * 0
            """
            operator_number_total_temp = self.curr_equip.operator_number_total
            operator_cost_total_temp = self.curr_equip.operator_cost_total
            mask = operator_number_total_temp > 0
            fleet.operator_cost = self.curr_equip.operator_cost_total * 0
            fleet.operator_cost[mask] = fleet.operator_number[mask] / operator_number_total_temp[mask] * operator_cost_total_temp[mask]

            """
            try:
                fleet.total_cost_no_op = fleet.req_hrs / self.curr_equip.total_req_hrs * self.curr_equip.total_cost_no_op
            except:
                fleet.total_cost_no_op =  * 0
            """
            total_req_hrs_temp = self.curr_equip.total_req_hrs
            total_cost_no_op_temp = self.curr_equip.total_cost_no_op
            mask = total_req_hrs_temp > 0
            fleet.total_cost_no_op = fleet.req_hrs * 0
            fleet.total_cost_no_op[mask] = fleet.req_hrs[mask] / total_req_hrs_temp[mask] * total_cost_no_op_temp[mask]

            try:
                fleet.total_cost = fleet.operator_cost + fleet.total_cost_no_op
            except:
                fleet.total_cost = fleet.req_hrs * 0

            for udiv, rowdiv in self.scheddiv.iterrows():
                divdat = self.scheddiv.loc[udiv]
                divdat = divdat.reset_index()
                divdat.set_index(['index'], inplace=True)
                divname = 'cost_per_' + udiv
                unit_cost = fleet.total_cost / divdat[udiv]
                setattr(fleet, divname, unit_cost)
                setattr(fleet, 'tot_cost', fleet.total_cost)

        fleet_operating = fleetme.equip_op_period
        fleet_purchase = fleetme.equip_purchase_period
        fleet_required = fleetme.equip_req_period
        fleet_salvage = fleetme.equip_slvg_period
        fleet_salvage_capital = fleetme.equip_slvg_cap_period
        fleet_capital = fleetme.equip_capital_period

        self.fleet_data_all = pd.DataFrame([{'fleet_data': fleet_data_run, 'fleet_operating': fleet_operating, 'fleet_purchased': fleet_purchase, 'fleet_capital': fleet_capital, 'fleet_required': fleet_required, 'fleet_salvaged': fleet_salvage, 'fleet_salvaged_capital': fleet_salvage_capital}])

        return self.fleet_data_all

class Input_Data():

    def __init__(self,CostModel):
        self.eqdata = CostModel.eqdata
        self.exist_fleet_input = CostModel.exist_fleet_input
        self.roster_input = CostModel.roster_input
        self.labour_cost_input = CostModel.labour_cost_input
        self.sched_data = CostModel.sched_data
        self.maintenance_curve = CostModel.maintenance_curve
        self.ma_curve = CostModel.ma_curve
        self.eq_activity = CostModel.eq_activity
        self.key_input_data = CostModel.key_input_data
        self.admin_input_data = CostModel.admin_input_data
        self.ohead_input_data = CostModel.ohead_input_data
        self.material_data = CostModel.material_data
        self.mcurve_cols = CostModel.mcurve_cols
        self.admin_years = CostModel.admin_years
        self.ohead_years = CostModel.ohead_years
        self.calendar = CostModel.calendar

class Equip():

    def __init__(self,CostModel):
        self.exist_fleet = []
        self.act_tree = []
        CostModel.all_cost_objects.append(self)
        CostModel.all_equipment.append(self)
        self.labour_cost = CostModel.labour_cost
        self.sched_periods = CostModel.sched_periods
        self.keyinputs = CostModel.keyinputs
        self.fleet_module = CostModel.fleet_module
        self.mtc_factor = CostModel.mtc_factor
        self.period_factor = self.keyinputs.period_factor

    @property
    def annual_op_hours(self):
        return self.o_roster.annual_op_hours

    @property
    def period_op_hours(self):
        return self.o_roster.period_ophours

    @property
    def life_yrs(self):
        return self.life / self.o_roster.annual_op_hours

    @lazyproperty
    def fuel_cost(self):

        fuel_cost = self.fuel_cons * self.keyinputs.fuel_charge
        if self.cost_basis == 'Contractor_Wet':
           fuel_cost = 0
        return fuel_cost

    @lazyproperty
    def electricity_cost(self):

        electicity_cost = self.kw * self.keyinputs.electricity_cost
        if self.cost_basis == 'Contractor_Wet':
            electicity_cost = 0
        return electicity_cost

    @lazyproperty
    def reagent_cost(self):

        reagent_cost = self.reagents * self.keyinputs.reagent_cost
        if self.cost_basis == 'Contractor_Wet':
           reagent_cost = 0
        return reagent_cost

    @lazyproperty
    def oil_grease_cost(self):
        og_flag = 'V'

        try:
            og_flag = self.oil_grease_factor[0]
        except:
            pass
        if og_flag == 'F':
            oil_grease_cost = float(self.oil_grease_factor[1:])
        else:
            oil_grease_cost = self.oil_grease_factor * self.fuel_cost

        if self.cost_basis == 'Contractor_Wet':
           oil_grease_cost = 0

        return oil_grease_cost

    @lazyproperty
    def component_cost(self):
        comp_flag = 'V'

        try:
            comp_flag = self.component_ratio[0]
        except:
            pass

        if comp_flag == 'F':
            component_cost = float(self.component_ratio[1:])
        else:
            component_cost = self.component_ratio * self.capital_cost * 1000000 * self.keyinputs.partsfactor

        if self.cost_basis == 'Contractor_Wet':
           component_cost = 0

        return component_cost

    @lazyproperty
    def tyre_cost(self):
        tyre_flag = 'V'

        try:
            tyre_flag = self.tyre_factor[0]
        except:
            pass

        if tyre_flag == 'F':
            tyre_cost = float(self.tyre_factor[1:])
        else:
            tyre_cost = self.tyre_factor * self.component_cost

        if self.cost_basis == 'Contractor_Wet':
           tyre_cost = 0

        return tyre_cost

    @lazyproperty
    def wear_get_cost(self):
        wg_flag = 'V'

        try:
            wg_flag = self.wear_get_ratio[0]
        except:
            pass

        if wg_flag == 'F':
            wear_get_cost = float(self.wear_get_ratio[1:])
        else:
            wear_get_cost = self.component_cost * self.wear_get_ratio

        if self.cost_basis == 'Contractor_Wet':
            wear_get_cost = 0

        return wear_get_cost

    @lazyproperty
    def minor_repair_costs(self):
        mr_flag = 'V'

        try:
            mr_flag = self.minor_repairs_ratio[0]
        except:
            pass

        if mr_flag == 'F':
            minor_repair_costs = float(self.minor_repairs_ratio[1:])
        else:
            minor_repair_costs = self.minor_repairs_ratio * (self.component_cost + self.wear_get_cost)

        if self.cost_basis == 'Contractor_Wet':
            minor_repair_costs = 0

        return minor_repair_costs

    @lazyproperty
    def ownership_cost(self):
        ownership_cost = (np.pmt(self.keyinputs.irate, self.life_yrs,
                                                 self.capital_cost * 1000000,
                                                 -1 * self.capital_cost * 1000000 * self.keyinputs.salvage_rate) / self.annual_op_hours) * -1

        if self.cost_basis == 'Contractor_Wet':
            ownership_cost = 0

        if self.cost_basis == 'Contractor_Dry':
            ownership_cost = 0

        if self.capital_cost_basis != 'ammortise':
            ownership_cost = 0

        return ownership_cost

    @lazyproperty
    def contract_cost(self):
        contract_cost = 0
        if self.cost_basis == 'Contractor_Wet':
            contract_cost = self.contractor_wet

        if self.cost_basis == 'Contractor_Dry':
            contract_cost = self.contractor_dry

        return contract_cost

    @lazyproperty
    def contract_cost_f(self):
        contract_cost_f = 0

        if self.cost_basis == 'Contractor_Cplus':
            if self.keyinputs.contractor_capfactor <= 0:
                root = tkinter.Tk()
                root.withdraw()
                messagebox.showwarning("Warning",
                                   "You have used allocated " + self.eqtype + " as Cost Plus Contractor but have no value in Capital Factor - fix up in Key Inputs of spreadhseet eg 10%")
            if self.keyinputs.contractor_opfactor <= 0:
                root = tkinter.Tk()
                root.withdraw()
                messagebox.showwarning("Warning",
                                   "You have used allocated " + self.eqtype + " as Cost Plus Contractor but have no value in Operating Factor - fix up in Key Inputs of spreadhseet eg 10%")

            contract_cost_f += self.ownership_cost * self.keyinputs.contractor_capfactor
            contract_cost_f += self.fuel_cost * self.keyinputs.contractor_opfactor
            contract_cost_f += self.oil_grease_cost * self.keyinputs.contractor_opfactor
            contract_cost_f += self.tyre_cost * self.keyinputs.contractor_opfactor
            contract_cost_f += self.wear_get_cost * self.keyinputs.contractor_opfactor


        return contract_cost_f

    @lazyproperty
    def contract_cost_v(self):
        contract_cost_v = 0

        if self.cost_basis == 'Contractor_Cplus':
            if self.keyinputs.contractor_capfactor <= 0:
                root = tkinter.Tk()
                root.withdraw()
                messagebox.showwarning("Warning",
                                   "You have used allocated " + self.eqtype + " as Cost Plus Contractor but have no value in Capital Factor - fix up in Key Inputs of spreadhseet eg 10%")
            if self.keyinputs.contractor_opfactor <= 0:
                root = tkinter.Tk()
                root.withdraw()
                messagebox.showwarning("Warning",
                                   "You have used allocated " + self.eqtype + " as Cost Plus Contractor but have no value in Operating Factor - fix up in Key Inputs of spreadhseet eg 10%")

            contract_cost_v += self.component_cost * self.keyinputs.contractor_opfactor
            contract_cost_v += self.minor_repair_costs * self.keyinputs.contractor_opfactor

        return contract_cost_v


    @lazyproperty
    def contract_cost_fixed(self):
        contract_cost_f = 0
        if self.cost_basis == 'Contractor_Wet':
            contract_cost_f = self.contractor_wet

        if self.cost_basis == 'Contractor_Dry':
            contract_cost_f = self.contractor_dry

        if self.cost_basis == 'Contractor_Cplus':
            contract_cost_f = self.contract_cost_f

        return contract_cost_f

    @lazyproperty
    def contract_cost_variable(self):
        contract_cost_variable = 0
        if self.cost_basis == 'Contractor_Cplus':
            contract_cost_variable = self.contract_cost_v
        return contract_cost_variable

    @lazyproperty
    def fixed_costs(self):
        fixed_costs = self.ownership_cost + self.fuel_cost + self.electricity_cost + self.reagent_cost + self.oil_grease_cost + self.tyre_cost + self.wear_get_cost + self.contract_cost_fixed
        return fixed_costs

    @lazyproperty
    def variable_costs(self):
        variable_costs = self.component_cost + self.minor_repair_costs + self.contract_cost_variable
        return variable_costs

    @lazyproperty
    def fleet_data_all(self):
        return self.fleet_module(self)

    @lazyproperty
    def fleet_data(self):
        return self.fleet_data_all.loc[0, 'fleet_data']

    @lazyproperty
    def fleet_operating(self):
        equip_operating_sums = self.fleet_data_all.loc[0, 'fleet_operating'].groupby(['sched_period','period_name', 'Eq_Id'])['opnum'].sum()
        equip_operating_sums = equip_operating_sums.reset_index()
        return equip_operating_sums

    @lazyproperty
    def fleet_purchased(self):
        equip_purchased_sums = self.fleet_data_all.loc[0, 'fleet_purchased'].groupby(['sched_period','period_name', 'Eq_Id'])['purchase'].sum()
        equip_purchased_sums = equip_purchased_sums.reset_index()
        return equip_purchased_sums

    @lazyproperty
    def fleet_capital(self):
        equip_capital_sums = self.fleet_data_all.loc[0, 'fleet_capital'].groupby(['sched_period','period_name', 'Eq_Id'])['capital'].sum()
        equip_capital_sums = equip_capital_sums.reset_index()
        return equip_capital_sums

    @lazyproperty
    def fleet_required(self):
        equip_required_sums = self.fleet_data_all.loc[0, 'fleet_required'].groupby(['sched_period','period_name', 'Eq_Id'])['opnum'].sum()
        equip_required_sums = equip_required_sums.reset_index()
        return equip_required_sums

    @lazyproperty
    def fleet_salvaged(self):
        equip_salvaged_sums = self.fleet_data_all.loc[0, 'fleet_salvaged'].groupby(['sched_period','period_name', 'Eq_Id'])['salvage'].sum()
        equip_salvaged_sums = equip_salvaged_sums.reset_index()
        return equip_salvaged_sums

    @lazyproperty
    def fleet_salvaged_capital(self):
        equip_salvaged_cap_sums = self.fleet_data_all.loc[0, 'fleet_salvaged_capital'].groupby(['sched_period','period_name', 'Eq_Id'])['salvage_value'].sum()
        equip_salvaged_cap_sums = equip_salvaged_cap_sums.reset_index()
        return equip_salvaged_cap_sums

    @lazyproperty
    def count_unit_number(self):
        return sum(fleet.unit_number for fleet in self.act_tree if fleet.calc_method == 'count')

    @property
    def operator_cost(self):
        return self.labour_cost(self.operator_category) * self.o_roster.operator_ratio * self.operator_allocation / self.o_roster.annual_op_hours

    @property
    def operator_annual_cost(self):
        return self.labour_cost(self.operator_category)

    @property
    def trades_cost(self):
        trades_cost = self.labour_cost('trades') / self.t_roster.manhour_worked
        if self.cost_basis == 'Contractor_Wet':
            trades_cost = 0

        if self.cost_basis == 'Contractor_Cplus':
            trades_cost = trades_cost*(1+self.keyinputs.contractor_opfactor)


        return trades_cost

class Admin():

    def __init__(self,CostModel):
        self.act_tree = []
        CostModel.all_cost_objects.append(self)
        CostModel.all_admin.append(self)
        self.keyinputs = CostModel.keyinputs

    @property
    def admin_cost(self):
        return self.unit_cost * self.keyinputs.admin_units * self.staff_number

    @property
    def cost_period(self):
        cal_temp = self.keyinputs.calendar.T
        self.admin_period = pd.DataFrame(columns=['admin_name', 'period_name', 'sched_period','staff_number','cost'])
        for index, row in cal_temp.iterrows():
            admin_year = row['Year']
            admin_staff_number = self.staff_number[admin_year]
            admin_cost = self.unit_cost * row['Months'] / 12 * admin_staff_number * self.keyinputs.admin_units
            data_temp = {'admin_name': self.aname, 'period_name': row['Name'], 'sched_period': index, 'staff_number' : self.staff_number[admin_year],'cost':admin_cost}
            self.admin_period = self.admin_period.append([data_temp])
        return self.admin_period

class Ohead():

    def __init__(self,CostModel):
        self.act_tree = []
        CostModel.all_cost_objects.append(self)
        CostModel.all_ohead.append(self)
        self.keyinputs = CostModel.keyinputs

    @property
    def ohead_cost(self):
        return self.an_cost * self.keyinputs.oh_units

    @property
    def cost_period(self):
        cal_temp = self.keyinputs.calendar.T
        self.ohead_period = pd.DataFrame(columns=['ohead_name', 'period_name', 'sched_period','cost'])
        for index, row in cal_temp.iterrows():
            ohead_year = row['Year']
            ohead_cost = self.an_cost[ohead_year] * row['Months'] / 12 * self.keyinputs.oh_units
            data_temp = {'ohead_name': self.ohead_name, 'period_name': row['Name'], 'sched_period': index, 'cost':ohead_cost}
            self.ohead_period = self.ohead_period.append([data_temp])

        return self.ohead_period

class Fleet():
    def __init__(self, CostModel, used_hrs, life, start, finish, active, allocated_hrs, year):
        self.used_hrs = used_hrs
        self.life = life
        self.start = start
        self.finish = finish
        self.active = active
        self.allocated_hrs = allocated_hrs
        self.year = year

    @property
    def rem_hrs(self):
        return self.life - self.used_hrs

class Eq_Act():
    def __init__(self, CostModel, eqtype, operator_ratio, period_op_hours, capacity, act_tree,act_tree_names,hour_basis,unit_number,utilisation_if_unit,prodrate_if_rate, sched_row_proportion, sched_row, dry_wet, material,l_curve, bucket_fill_factor, carryback_perc, single_side_loading_perc, truck_loaded,spot_time,first_pass,other_passes,truck_presentation):
        CostModel.all_activities.append(self)
        self.costmod = CostModel
        self.eqtype = eqtype
        self.operator_ratio = operator_ratio
        self.act_tree = act_tree
        self.act_tree_names = act_tree_names
        self.calc_method = hour_basis
        self.utilisation_if_unit = utilisation_if_unit
        self.prodrate_if_rate = prodrate_if_rate
        if hour_basis != 'count':
            self.sched_calc = 'sched.loc[' + str(int(sched_row)) + ']'
            self.sched_row = sched_row
        else:
            self.sched_calc = 'sched.loc[0]'
            self.sched_row = 0
        self.input_op_hours = period_op_hours
        self.material = material
        self.bucket_fill_factor = bucket_fill_factor
        self.carryback = carryback_perc
        self.single_side_loading = single_side_loading_perc
        self.truck_loaded = truck_loaded
        self.spot_time = spot_time
        self.first_pass = first_pass
        self.other_passes = other_passes
        self.truck_presentation = truck_presentation
        self.dry_wet = dry_wet
        self.raw_unit_number = unit_number
        self.keyinputs = CostModel.keyinputs
        self.sched_periods = CostModel.sched_periods
        self.input_data = CostModel.input_data
        self.sched = CostModel.sched
        self.learning_curve = CostModel.learning_curve
        self.l_curve = l_curve
        self.capacity = capacity

        hourdata = 'self.' + self.sched_calc
        if self.calc_method != 'count': sdata = eval(hourdata) * self.keyinputs.tonnage_units_sched * sched_row_proportion

        if self.calc_method == 'hours':
            hourdata = 'self.' + self.sched_calc
            lcurve = pd.Series(range(0, self.sched_periods + 1))
            lcurve = lcurve * 0 + 1
            lcurve = lcurve.T
            try:
                lcurve_select = self.learning_curve.loc[self.l_curve]
                lcurve = lcurve * lcurve_select
                lcurve.fillna(value=1,inplace=True)
                lcurve=lcurve.T
                lcurve = lcurve.drop(lcurve.index[0]).T
                self.req_hrs = eval(hourdata) / lcurve
                self.req_hrs.fillna(value=0, inplace=True)
            except:
                self.hdata = eval(hourdata)

        elif self.calc_method == 'count':
            hdata = pd.Series(range(0, self.sched_periods + 1))
            hdata.drop(0, inplace=True)
            hdata = (hdata * 0) + 1
            self.req_hrs = hdata * self.period_op_hours * self.unit_number


        elif self.calc_method == 'count_variable':
            self.req_hrs = sdata * self.period_op_hours * self.raw_unit_number


        elif self.calc_method == 'rate_instant':
            self.curr_digger = next(obj for obj in self.costmod.all_cost_objects if obj.etype == self.eqtype)
            digprod = self.prodrate_if_rate * self.curr_digger.o_roster.effectiveness
            self.req_hrs = sdata / digprod

        elif self.calc_method == 'rate_effective':
            digprod = self.prodrate_if_rate
            self.req_hrs = sdata / digprod

        elif self.calc_method == 'rate_calc':
            digprod = self.prodrate_if_rate
            # TODO put in error catch if digprod not properly populated
            if digprod == 'digprod':
                try:
                    self.bucket_load_pass = self.bucket_fill_factor * self.capacity
                    self.swell_into_bucket = self.input_data.material_data.loc[self.material,'swell_in_bucket']
                    self.bucket_load_pass_bcm = self.bucket_load_pass / (1 + self.swell_into_bucket)
                    self.insitu_wet_density = self.input_data.material_data.loc[self.material,'in_situ_wet_density']
                    self.bucket_load_per_pass = self.bucket_load_pass_bcm * self.insitu_wet_density
                    self.truck_activity = self.truck_loaded.split(":")[1]
                    self.truck_name = self.truck_loaded.split(":")[0]
                    self.curr_digger = next(obj for obj in self.costmod.all_cost_objects if obj.etype == self.eqtype)
                    self.effectiveness = self.curr_digger.o_roster.effectiveness
                    self.curr_truck = next(obj for obj in self.costmod.all_cost_objects if obj.etype == self.truck_name)
                    self.truck_capacity = self.curr_truck.capacity            #this needs to be linked to activity
                    self.number_passes = self.truck_capacity / self.bucket_load_per_pass
                    self.number_passes_int = np.rint(self.number_passes)
                    self.truck_load_ton = self.number_passes_int * self.bucket_load_per_pass * (1 - self.carryback)
                    self.truck_load_bcm = self.number_passes_int * self.bucket_load_pass_bcm * (1 - self.carryback)
                    self.dbl_side_loading = 1 - self.single_side_loading
                    self.total_load_time = (self.single_side_loading * ((self.number_passes_int - 1) * self.other_passes + self.first_pass + self.spot_time)/60) + (self.dbl_side_loading * ((self.number_passes_int * self.other_passes) + self.first_pass) /60)
                    self.loads_hour = 60 / self.total_load_time
                    self.wet_tonnes_hour = self.loads_hour * self.truck_load_ton
                    self.bcm_hour = self.loads_hour * self.truck_load_bcm
                    self.work_efficiency = self.curr_digger.o_roster.work_efficiency / 60
                    self.truck_presentation = self.truck_presentation
                    self.wet_tonnes_productivity = self.truck_presentation * self.work_efficiency * self.wet_tonnes_hour * self.effectiveness
                    self.dry_tonnes_productivity = self.wet_tonnes_productivity / (self.input_data.material_data.loc[self.material,'in_situ_wet_density']/self.input_data.material_data.loc[self.material,'dry_density'])

                    self.bcm_productivity = self.truck_presentation * self.work_efficiency * self.bcm_hour * self.effectiveness

                    if self.dry_wet == 'dry':
                        digprod = self.dry_tonnes_productivity
                    else:
                        digprod = self.wet_tonnes_productivity

                    self.productivity = digprod

                except:
                    print("problem with digprod for", etype)
                    messagebox.showwarning("Warning",
                                           "Problem with dig production calc for " + etype + " check details in spreadsheet in Eq_Activity - bucket fill, truck loaded, check truck has spot times etc")

                self.req_hrs = sdata / digprod

    @property
    def unit_number(self):
        if self.calc_method == 'count':
            tdata = pd.Series(range(0, self.sched_periods + 1))
            tdata.drop(0, inplace=True)
            tdata = (tdata * 0) + 1
            unit_number = self.raw_unit_number * tdata
        elif self.calc_method == 'count_variable':
            hourdata = 'self.' + self.sched_calc
            sdata = eval(hourdata) * self.keyinputs.tonnage_units_sched

            unit_number = self.raw_unit_number * sdata
        else:
            unit_number = 0

        return unit_number

    @property
    def period_op_hours(self):
        if self.calc_method == 'count':
            return self.input_op_hours * self.utilisation_if_unit
        else:
            return self.input_op_hours

class Admin_Act():
    def __init__(self, CostModel, admin_type, staff_number, act_tree,act_tree_names,unit_costs,cost_period):
        CostModel.all_activities.append(self)
        self.atype = admin_type
        self.act_tree = act_tree
        self.act_tree_names = act_tree_names
        self.staff_number = staff_number
        self.unit_costs = unit_costs
        self.cost_period = cost_period

class Ohead_Act():
    def __init__(self, CostModel,ohead_type, act_tree,act_tree_names,cost_period):
        CostModel.all_activities.append(self)
        self.otype = ohead_type
        self.act_tree = act_tree
        self.act_tree_names = act_tree_names
        self.cost_period = cost_period

class Roster():
    def __init__(self, CostModel, name, hrs_shift, shifts_day, panels, days_roster, days_worked,  unpaid_days_shut,public_holidays, equip_non_work, absent, annual_leave,
                 sick_leave, crib, smoko, work_efficiency, de_rate_days, wet_weather, planned_onshift_mtc,
                 planned_offshift_mtc, rostered_onshift_mtc, standby, bdown, service_lube,
                 awaiting_trucks, awaiting_other_plant, re_locating, clean_up, shift_change, blasting_other):

        CostModel.all_rosters.append(self)
        self.roster_name = name
        self.hrs_shift = hrs_shift
        self.shifts_day = shifts_day
        self.panels = panels
        self.days_roster = days_roster
        self.days_worked = days_worked
        self.public_holidays = public_holidays
        self.equip_non_work = equip_non_work
        self.absent = absent

        self.annual_leave = annual_leave
        self.sick_leave = sick_leave
        self.crib = crib
        self.smoko = smoko
        self.work_efficiency = work_efficiency
        self.de_rate_days = de_rate_days
        self.wet_weather = wet_weather
        self.planned_onshift_mtc = planned_onshift_mtc
        self.planned_offshift_mtc = planned_offshift_mtc
        self.unpaid_days_lost = unpaid_days_shut
        self.rostered_onshift_mtc = rostered_onshift_mtc
        self.standby = standby
        self.bdown = bdown
        self.service_lube = service_lube
        self.awaiting_trucks = awaiting_trucks
        self.awaiting_other_plant = awaiting_other_plant
        self.re_locating = re_locating
        self.clean_up = clean_up
        self.shift_change = shift_change
        self.blasting_other = blasting_other

        # TODO not sure what to do with this
        self.calendar_days = 365
        self.annual_leave_yr = annual_leave
        self.annual_leave_period = (self.annual_leave_yr / self.calendar_days) * self.calendar_days
        self.sick_leave = sick_leave
        self.total_paid_days = self.calendar_days - self.rostered_days_off - self.unpaid_days_lost
        self.actual_work_days = self.total_paid_days - self.annual_leave_yr - self.sick_leave - self.public_holidays
        self.manhour_worked = self.actual_work_days * self.hrs_shift
        self.manhours_paid = self.total_paid_days * self.hrs_shift
        self.de_rate = de_rate_days
        self.bdown = bdown
        self.awaiting_trucks = awaiting_trucks
        self.awaiting_other_plant = awaiting_other_plant
        self.re_locating = re_locating
        self.clean_up = clean_up
        self.standby = standby
        self.blasting_other = blasting_other
        self.keyinputs = CostModel.keyinputs

        self.on_shift_mtc = self.planned_onshift_mtc
        self.planned_mtc = self.planned_offshift_mtc
        self.scheduled_days = self.calendar_days - self.public_holidays - self.equip_non_work
        self.sched_hours = self.scheduled_days * self.shifts_day * self.hrs_shift
        self.available_hours = self.sched_hours - (self.on_shift_mtc * self.hrs_shift * self.shifts_day + self.rostered_onshift_mtc * self.hrs_shift + self.bdown * self.scheduled_days * self.shifts_day)
        self.available_days = self.available_hours / (self.shifts_day * self.hrs_shift)
        sum_days_lost = self.wet_weather + self.de_rate_days
        sum_minutes_lost = self.crib + self.smoko + self.shift_change + self.service_lube
        self.lost_hrs = ((sum_minutes_lost * self.shifts_day * self.available_days) / 60) + ((sum_days_lost * self.shifts_day * self.hrs_shift))
        self.annual_op_hours = self.available_hours - self.lost_hrs
        self.op_days = self.available_days - sum_days_lost
        sum_shift_mins_lost = self.awaiting_trucks + self.awaiting_other_plant + self.re_locating + self.clean_up + self.standby + self.blasting_other
        self.effective_work_hours = (self.annual_op_hours - (((sum_shift_mins_lost) / 60) * self.shifts_day * self.op_days)) * (self.work_efficiency / 60)
        self.maintenance_availability = self.available_hours / self.sched_hours
        self.operating_utilisation = self.annual_op_hours / self.available_hours
        self.effectiveness = self.effective_work_hours / self.annual_op_hours

    @property
    def period_ophours(self):

        return self.annual_op_hours * self.keyinputs.period_factor

    @property
    def rostered_days_off(self):
        return (self.calendar_days / self.days_roster) * (self.days_roster - self.days_worked)

    @property
    def paid_days(self):
        return (self.calendar_days - self.rostered_days_off - self.unpaid_days_lost)

    @property
    def operator_ratio(self):
        leave_coverage = (self.sick_leave + self.annual_leave_yr)/self.actual_work_days
        return (self.panels * (1 / (1 - (self.absent + leave_coverage))))

class Key_Inputs():
    def __init__(self,CostModel):
        CostModel.all_key_inputs.append(self)
        self.irate = CostModel.input_data.key_input_data.loc['interest_equip','value']
        self.salvage_rate = CostModel.input_data.key_input_data.loc['salvage_ratio','value']
        self.fuel_charge = CostModel.input_data.key_input_data.loc['fuel_charge','value']
        self.electricity_cost = CostModel.input_data.key_input_data.loc['electricity_cost', 'value']
        self.reagent_cost = CostModel.input_data.key_input_data.loc['reagent_cost', 'value']
        self.partsfactor = CostModel.input_data.key_input_data.loc['partsfactor','value']
        self.period_factor = CostModel.calendar.loc['Months'] / 12
        self.period_name = CostModel.calendar.loc['Name']
        self.calendar = CostModel.calendar
        self.tonnage_units_sched = CostModel.input_data.key_input_data.loc['tonnage_units_sched', 'value']
        self.tonnage_units_digprod = CostModel.input_data.key_input_data.loc['tonnage_units_digprod', 'value']
        self.admin_units = CostModel.input_data.key_input_data.loc['admin_units', 'value']
        self.oh_units = CostModel.input_data.key_input_data.loc['oh_units', 'value']
        self.tradesmen_factor = CostModel.input_data.key_input_data.loc['tradesmen_factor', 'value']
        self.contractor_capfactor = CostModel.input_data.key_input_data.loc['contractor_capfactor', 'value']
        self.contractor_opfactor = CostModel.input_data.key_input_data.loc['contractor_opfactor', 'value']
        self.discount_rate = CostModel.input_data.key_input_data.loc['discount_rate', 'value']

    @property
    def c_days(self):
        # TODO this is a bit weird
        self.calendar_days = 365
        return self.calendar_days


if __name__ == '__main__':

    costrun = CostModel(1)
    fred = costrun.Kom_930E_AA.fleet_data




