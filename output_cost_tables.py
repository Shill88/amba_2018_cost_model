import openpyxl
from pandas import HDFStore
from pandas import read_hdf

from openpyxl.utils.dataframe import dataframe_to_rows

class Output_tabs():
    def runme():

        wb = openpyxl.load_workbook('output.xlsx')

        #listdb =
        #self.listdb = hdf.keys()
        #self.comboBox_selectTable.addItems(self.listdb)

        ### Discounted Costs ###
        hdfinput = HDFStore('plot_data.h5')
        listme = hdfinput.keys()
        for sheet in listme:
            df = read_hdf(hdfinput, sheet)
            sheetname = sheet[1:]
            if sheetname not in wb.sheetnames:
                wb.create_sheet(sheetname)
            ws = wb[sheetname]
            rows = dataframe_to_rows(df)
            for r_idx, row in enumerate(rows, 1):
                for c_idx, value in enumerate(row, 1):
                    ws.cell(row=r_idx, column=c_idx, value=value)

        ### Schedule ###
        hdfinput = HDFStore('input_data.h5')
        df = read_hdf(hdfinput, 'sched_data_raw')
        ws = wb['sched_data_raw']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                 ws.cell(row=r_idx, column=c_idx, value=value)


        ### Schedule Report ###
        hdfinput = HDFStore('input_data.h5')
        df = read_hdf(hdfinput, 'sched_report_raw')
        ws = wb['sched_report_raw']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Capital ###
        hdfinput = HDFStore('input_data.h5')
        df = read_hdf(hdfinput, 'capital_data')
        ws = wb['capital']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Total Operating Fleet ###
        hdftree = HDFStore('tree_data.h5')
        df = read_hdf(hdftree, 'Total_Operating_Fleet')
        ws = wb['total_operating_fleet']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                 ws.cell(row=r_idx, column=c_idx, value=value)


        ### Total Purchased Fleet ###
        df = read_hdf(hdftree, 'Total_Purchased_Fleet')
        ws = wb['total_purchased_fleet']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Total Required Fleet ###
        hdftree = HDFStore('tree_data.h5')
        df = read_hdf(hdftree, 'Total_Required_Fleet')
        ws = wb['total_required_fleet']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                 ws.cell(row=r_idx, column=c_idx, value=value)



        ### Total Fleet Manning ###
        df = read_hdf(hdftree, 'opnum_tree')
        ws = wb['opnum_tree']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Admin Manning ###
        df = read_hdf(hdftree, 'admin_tree_number')
        ws = wb['admin_tree_number']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                 ws.cell(row=r_idx, column=c_idx, value=value)


        ### Admin Manning Cost ###
        df = read_hdf(hdftree, 'admin_tree_cost')
        ws = wb['admin_tree_cost']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Overhead Cost ###
        df = read_hdf(hdftree, 'ohead_tree_cost')
        ws = wb['ohead_tree_cost']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Fuel ###
        df = read_hdf(hdftree, 'fuel_data')
        ws = wb['fuel_tree_data']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Kw ###
        df = read_hdf(hdftree, 'kw_data')
        ws = wb['kw_tree_data']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### water ###
        df = read_hdf(hdftree, 'water_data')
        ws = wb['water_tree_data']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### reagents ###
        df = read_hdf(hdftree, 'reagents_data')
        ws = wb['reagents_tree_data']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Operator Numbers ###
        df = read_hdf(hdftree, 'op_equip_data')
        ws = wb['op_equip_data']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Costs and Manning ###
        df = read_hdf(hdftree, 'total_costs_and_manning')
        ws = wb['total_costs_and_manning']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Operating Cost Total ###
        df = read_hdf(hdftree, 'tot_cost_tree')
        ws = wb['tot_cost_tree']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        ### Individual Fleet Costs ###
        df = read_hdf(hdftree, 'fleet_cost_details')
        ws = wb['fleet_cost_details']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        ### Undiscoutnted Costs ###
        df = read_hdf(hdftree, 'cost_per_ore_dt')
        ws = wb['undisc_cost_ore_dt']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        df = read_hdf(hdftree, 'cost_per_ore_wt')
        ws = wb['undisc_cost_ore_wt']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        df = read_hdf(hdftree, 'cost_per_tot_dt')
        ws = wb['undisc_cost_tot_dt']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)


        df = read_hdf(hdftree, 'cost_per_tot_wt')
        ws = wb['undisc_cost_tot_wt']
        rows = dataframe_to_rows(df)
        for r_idx, row in enumerate(rows, 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        wb.save('output.xlsx')

        print('output.xlsx updated')

        return

if __name__ == '__main__':
    fred = Output_tabs.runme()

