# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 18:00:38 2017

@author: slewis
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication

class viewme():
    def __init__(self, parent=None):
        self.parent = parent
        self.viewdbase()

    def viewdbase(self):
        import update_costmod
        self.Dialog = QtWidgets.QDialog()
        self.ui = update_costmod.Ui_Dialog()
        self.ui.setupUi(self.Dialog)
        self.ui.pushButton.clicked.connect(self.selectionchosen)
        self.Dialog.show()

    def selectionchosen(self):
        import runcostmodel_DP
        self.comp_me = runcostmodel_DP


def main():
    app = QApplication(sys.argv)
    form = viewme()
    app.exec_()


if __name__ == '__main__':
    import sys

    main()