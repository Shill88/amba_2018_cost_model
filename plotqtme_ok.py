
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
from pandas import HDFStore
from pandas import read_hdf

plt.style.use('ggplot')

class Plot_Window(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("IMC Plotting Widget")

        self.ax = self.figure.add_subplot(211)
        self.ax1 = self.figure.add_subplot(212)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")


        #label=QLabel(self)
        #label.setText("Window Title")

        #self.setStyleSheet("QWidget {background-color:   #23297A}")

        self.resize(1280,860)

        # set the layout
        layout = QVBoxLayout()
        #layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        #layout.addWidget(self.button)
        self.setLayout(layout)
        self.plotfuel()

    def plotfuel(self):
        ''' plot some random stuff '''

        self.figure.tight_layout(pad=3)
        self.setWindowTitle('Consumables')
        self.figure.figsize=(18, 14)

        hdfplot = HDFStore('plot_data.h5')

        graph_data = read_hdf(hdfplot, 'fuel_data')
        temp = list(graph_data)
        for x in range(len(temp)):
            firsty = temp[x][0]
            if firsty == '_':
                temp[x] = temp[x][1:]

        graph_data.columns = temp

        gsums = graph_data.sum()
        gtop5 = gsums.nlargest(n=5)
        gtop5 = gtop5.to_frame()
        graph_data = graph_data.T
        graph_data = gtop5.join(graph_data)
        graph_data = graph_data.drop(0, 1).T
        self.ax.set_ylabel('Fuel Litres')
        self.figure; graph_data.plot(ax=self.ax, kind='area', title='Fuel - Top 5 Users').legend(loc=9, bbox_to_anchor=(0.5, -0.1), ncol=5, fontsize=7, borderaxespad=0)

        graph_data2 = read_hdf(hdfplot, 'kw_data')
        temp = list(graph_data2)
        for x in range(len(temp)):
            firsty = temp[x][0]
            if firsty == '_':
                temp[x] = temp[x][1:]

        graph_data2.columns = temp

        gsums2 = graph_data2.sum()
        gtop52 = gsums2.nlargest(n=5)
        gtop52 = gtop52.to_frame()
        masktemp = gtop52[0] > 0
        graph_data2 = graph_data2.T
        graph_data2 = gtop52[masktemp].join(graph_data2)
        graph_data2 = graph_data2.drop(0, 1).T
        self.ax1.set_ylabel('KwHrs')
        self.figure; graph_data2.plot(ax=self.ax1, kind='area', title='KwHrs - Top 5 Users').legend(loc=9, bbox_to_anchor=(0.5, -0.1), ncol=5, fontsize=7, borderaxespad=0)


class Plot_Window_Sched(QDialog):
    def __init__(self, parent=None):
        super(Plot_Window_Sched, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()
        self.setWindowTitle("IMC Plotting Widget")

        self.ax = self.figure.add_subplot(221)
        self.ax1 = self.figure.add_subplot(222)
        self.ax2 = self.figure.add_subplot(223)
        self.ax3 = self.figure.add_subplot(224)
        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMinimumWidth(300)
        self.toolbar.setStyleSheet("QToolBar { border: 0px }")

        # label=QLabel(self)
        # label.setText("Window Title")

        # self.setStyleSheet("QWidget {background-color:   #23297A}")

        #self.resize(1280, 860)

        # set the layout
        layout = QVBoxLayout()
        # layout.addWidget(label)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        # layout.addWidget(self.button)
        self.setLayout(layout)
        self.plotsched()

    def plotsched(self):
        ''' plot Schedule Data '''

        self.figure.tight_layout(pad=1)
        self.setWindowTitle('Schedule Data')
        self.figure.figsize = (18, 14)

        self.hdfinputs = HDFStore('input_data.h5')
        self.figure; read_hdf(self.hdfinputs, 'sched_graph_stack1').plot(ax=self.ax, kind='bar',figsize=(18, 9),
                                                                 stacked=True, fontsize=6, title='Total Mined Material')
        self.ax.set(xlabel="Period", ylabel="Million Tonnes")
        self.figure; read_hdf(self.hdfinputs, 'sched_graph_stack2').plot(ax=self.ax1, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Ore Mined')
        self.ax1.set(xlabel="Period", ylabel="Million Tonnes")
        self.figure; read_hdf(self.hdfinputs, 'sched_graph_stack3').plot(ax=self.ax2, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Ore Processed')
        self.ax2.set(xlabel="Period", ylabel="Million Tonnes")
        self.figure; read_hdf(self.hdfinputs, 'sched_graph_stack4').plot(ax=self.ax3, kind='bar',figsize=(18, 9),
                                                                  stacked=True, fontsize=6, title='Total Material Moved')
        self.ax3.set(xlabel="Period", ylabel="Million Tonnes")



if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Plot_Window()
    main.show()

    sys.exit(app.exec_())