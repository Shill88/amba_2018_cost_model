# -*- coding: utf-8 -*-
"""
Created on Mon May  1 09:34:41 2017

@author: slewis
"""


import pandas as pd
from PyQt5.QtWidgets import (QAction, QActionGroup, QApplication, QFrame, QLabel, QMainWindow, QMenu, QMessageBox, QSizePolicy, QVBoxLayout, QWidget,QPushButton,QCheckBox,QProgressBar,QCalendarWidget)
from pandas import HDFStore
from pandas import read_hdf
import matplotlib.pyplot as plt
import matplotlib



class Fuel_view:
    
    def __init__(self):
        super(Fuel_view, self).__init__()

        hdfplot = HDFStore('plot_data.h5')

        graph_data = read_hdf(hdfplot, 'fuel_data')

        (fig, axes) = plt.subplots(figsize=(18, 10))
        fig.canvas.set_window_title('IMC - Operating Data')
        axes.set_ylabel('Fuel Litres')

        fig.tight_layout(pad=7)

        graph_data.plot(ax=axes, kind='area', title='fuel usage').legend(loc=9, bbox_to_anchor=(0.5, -0.05),
                                                                              ncol=7, fontsize=8, borderaxespad=0.)




#app = Fuel_view()
