import tkinter
from tkinter import messagebox
import pandas as pd


class period_names:
    def period_me(self, callist, treedata,namecols):
        self.callist = callist
        self.treedata = treedata
        if namecols == 4:
            curdat_periods = self.treedata.drop(self.treedata.columns[[0, 1, 2, 3]], axis=1)
        elif namecols == 3:
            curdat_periods = self.treedata.drop(self.treedata.columns[[0, 1, 2]], axis=1)
        elif namecols == 2:
            curdat_periods = self.treedata.drop(self.treedata.columns[[0, 1]], axis=1)
        elif namecols == 1:
            curdat_periods = self.treedata.drop(self.treedata.columns[[0]], axis=1)
        else:
            curdat_periods = self.treedata.drop(self.treedata.columns[[0, 1, 2]], axis=1)

        try:
            curdat_periods.columns = [self.callist]
        except:
            root = tkinter.Tk()
            root.withdraw()
            messagebox.showwarning("Warning",
                                   "Missaligned Calendar - Check the Spreadhsheet calendar periods matches the periods in the schedule worksheet")

        lencurdat = self.treedata.shape[1]
        if namecols == 4:
            curdat_front = self.treedata.drop(self.treedata.columns[4:lencurdat], axis=1)
        elif namecols == 3:
            curdat_front = self.treedata.drop(self.treedata.columns[3:lencurdat], axis=1)
        elif namecols == 2:
            curdat_front = self.treedata.drop(self.treedata.columns[2:lencurdat], axis=1)
        elif namecols == 1:
            curdat_front = self.treedata.drop(self.treedata.columns[1:lencurdat], axis=1)
        else:
            curdat_front = self.treedata.drop(self.treedata.columns[3:lencurdat], axis=1)

        curdat_join = curdat_front.join(curdat_periods)
        return curdat_join

    def sum_act(self, treedata,namecols):

        self.treedata = treedata
        temp_tree_cost = treedata
        temp_tree_tots = self.treedata.copy()
        collist = list(temp_tree_tots)
        temp_tree_tots.drop(collist[1:namecols], axis=1, inplace=True)
        temp_tree_tots.reset_index(inplace=True)
        temp_tree_tots.drop(['index'], axis=1, inplace=True)
        temp_tree_tots.set_index(['act_level'], inplace=True)
        temp_tree_tots.fillna(value=0,inplace=True)
        tree_cost_iter = temp_tree_cost.fillna(value=0)
        temp_tree_tots = temp_tree_tots.iloc[1:]
        cost_tree_sums = pd.DataFrame()

        for idx_tree, treedat in tree_cost_iter.iterrows():
            act_lev_start = treedat['act_level']
            tot_dat = treedat
            tot_dat = tot_dat.iloc[3:]
            tot_dat = tot_dat.reset_index()
            tot_dat.set_index(['index'],inplace=True)
            tot_dat = tot_dat.T
            tot_dat.reset_index(inplace=True)
            tot_dat.drop(['index'], axis=1, inplace=True)

            for children, childat in temp_tree_tots.iterrows():
                act_lev_current = children
                if act_lev_current <= act_lev_start:
                    break
                childdf = childat.reset_index()
                childdf.set_index(['index'],inplace=True)
                childdf = childdf.T
                childdf.reset_index(inplace=True)
                childdf.drop(['index'], axis=1, inplace=True)
                tot_dat += childdf

            tot_dat['act_level'] = treedat['act_level']
            tot_dat['Act_Id'] = treedat['Act_Id']
            tot_dat['Name'] = treedat['Name']
            temp_tree_tots = temp_tree_tots.iloc[1:]
            cost_tree_sums = cost_tree_sums.append(tot_dat)

        cost_tree_sums.set_index(['act_level','Act_Id','Name'],inplace=True)
        cost_tree_sums.reset_index(inplace=True)

        return cost_tree_sums




